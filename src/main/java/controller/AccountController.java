/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import model.Account;
import model.AccountPlan;
import model.Client;
import model.Enterprise;
import model.EnterpriseAccount;
import model.Item;
import model.Session;

/**
 *
 * @author GDF
 */
public class AccountController {

    //VERIFICAR
    /**
     *
     * @param ap
     * @param it
     * @param name
     * @return
     */
    public static boolean insert(AccountPlan ap, Item it, String name) {
        if (findForName(ap.getId(), name) == null) {
            Account newAccount = new Account(ap, it, name);
            boolean resultado;
            try {
                JPA.beginTransaction();
                resultado = JPA.create(newAccount);
                JPA.commitTransaction();
            } catch (Exception e) {
                resultado = false;
            } finally {
                JPA.rollbackTransaction();
            }
            return resultado;
        } else {
            return false;
        }

    }

    /**
     * Insertar una cuenta dentro de otra.
     *
     * @param a
     * @param ap
     * @param it
     * @param name
     * @return
     */
    public static boolean insert(Account a, AccountPlan ap, Item it, String name) {
        if (findForName(ap.getId(), name) == null) {
            Account newAccount = new Account(a, ap, it, name);
            boolean resultado;
            try {
                JPA.beginTransaction();
                resultado = JPA.create(newAccount);
                JPA.commitTransaction();
            } catch (Exception e) {
                resultado = false;
            } finally {
                JPA.rollbackTransaction();
            }
            return resultado;
        } else {
            return false;
        }

    }

    /**
     *
     * @param idAP Id del plan de cuenta
     * @param name Nombre de la cuenta a buscar
     * @return Retorna cuenta que coincide con el nombre y el plan de cuentas
     */
    public static Account findForName(int idAP, String name) {
        try {
            return (Account) JPA.runQuery("SELECT a FROM " + Account.class.getName() + " a WHERE a.accountPlan.id=" + idAP + " AND a.name='" + name + "'").get(0);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param a
     * @return
     */
    public static boolean isClient(Account a) {
        return a.getCategory().getName().compareTo("ACTIVO") == 0;
    }

    /**
     *
     * @param a
     * @return
     */
    public static boolean isProvider(Account a) {
        return a.getCategory().getName().compareTo("PASIVO") == 0;
    }

    /**
     *
     * @param ap
     * @return
     */
    public static List<Account> getAll(AccountPlan ap) {
        return ap.getAccounts();
    }

    /**
     *
     * @param i
     * @param ap
     * @return
     */
    public static List<Account> getAll(Item i, AccountPlan ap) {
        ArrayList<Account> ret = new ArrayList<Account>();
        for (Object a : JPA.getAll(Account.class)) {
            Account account = (Account) a;
            if (account.getItem() == i && account.getAccountPlan() == ap) {
                ret.add(account);
            }
        }
        return ret;
    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            Account foundAccount = (Account) JPA.findByID(Account.class, id);
            JPA.beginTransaction();
            resultado = JPA.delete(foundAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Account.class);
    }

    /**
     *
     * @param id
     * @param name
     * @return
     */
    public static boolean edit(int id, String name) {
        boolean result;
        try {
            Account foundAccount = (Account) JPA.findByID(Account.class, id);
            if (name != null) {
                foundAccount.setName(name);
            }
            JPA.beginTransaction();
            result = JPA.update(foundAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     *
     * @param value
     * @return
     */
    public static List getLike(String value) {
        return JPA.runQuery("SELECT a FROM " + Account.class.getName() + " a WHERE a.accountPlan.id=" + Session.client.getAccountPlan().getId() + " AND a.name LIKE '" + value.toUpperCase() + "%'");
    }

    /**
     *
     * @param enterprise
     * @return Retorna las cuentas (del plan de cuentas de la sesion) que
     * pertenecen a una empresa.
     */
    public static List<Account> getAccount(Enterprise enterprise) {
        List ret = new LinkedList();
        List eal = enterprise.getEnterpriseAccount();

        for (Object aux : eal) {
            EnterpriseAccount ea = (EnterpriseAccount) aux;
            if (ea.getAccount().getAccountPlan() == Session.client.getAccountPlan()) {
                ret.add(ea.getAccount());
            }
        }
        return ret;
    }

    /**
     *
     * @param a Cuenta de la que se desea saber el cliente
     * @return Cliente en cuyo plan de cuentas esta la cuenta a
     */
    public static Client getClient(Account a) {
        return a.getAccountPlan().getClient();
    }

    /**
     *
     * @param ap
     * @return
     */
    public static List<Account> getClientAccounts(AccountPlan ap) {
        LinkedList<Account> ret = new LinkedList<Account>();
        List all = getAll(ap);
        for (Object o : all) {
            Account a = (Account) o;
            if (isClient(a)) {
                ret.add(a);
            }
        }
        return ret;
    }

    /**
     *
     * @param ap
     * @return
     */
    public static List<Account> getProviderAccounts(AccountPlan ap) {
        LinkedList<Account> ret = new LinkedList<Account>();
        List all = getAll(ap);
        for (Object o : all) {
            Account a = (Account) o;
            if (isProvider(a)) {
                ret.add(a);
            }
        }
        return ret;
    }
}