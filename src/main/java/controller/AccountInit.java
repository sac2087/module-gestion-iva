/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.AccountPlan;

/**
 *
 * @author GDF
 */
public class AccountInit {

    /**
     *
     * @param ap
     */
    public static void init(AccountPlan ap) {
        activoCorriente(ap);
        activoNoCorriente(ap);
        pasivoCorriente(ap);
        patrimonioNetoCapReseResu(ap);
        resultados(ap);
    }

    /**
     *
     * @param ap
     */
    public static void activoCorriente(AccountPlan ap) {
        AccountController.insert(ap, ItemController.findForName("CAJA Y BANCOS"), "CAJA");
        AccountController.insert(ap, ItemController.findForName("CAJA Y BANCOS"), "BANCO");
        AccountController.insert(ap, ItemController.findForName("CAJA Y BANCOS"), "DEPOSITOS DE CAUCION");
        AccountController.insert(ap, ItemController.findForName("CAJA Y BANCOS"), "CAJA DE AHORRO");

        AccountController.insert(ap, ItemController.findForName("CREDITOS POR VENTAS"), "CLIENTES");
        AccountController.insert(ap, ItemController.findForName("CREDITOS POR VENTAS"), "TARJETAS A COBRAR");

        AccountController.insert(ap, ItemController.findForName("OTROS CREDITOS"), "IVA CREDITO FISCAL");
        AccountController.insert(ap, ItemController.findForName("OTROS CREDITOS"), "RETENCIONES INGRESOS BRUTOS");
        AccountController.insert(ap, ItemController.findForName("OTROS CREDITOS"), "RETENCIONES IVA");
        AccountController.insert(ap, ItemController.findForName("OTROS CREDITOS"), "RETENCIONES GANANCIAS");
        AccountController.insert(ap, ItemController.findForName("OTROS CREDITOS"), "OTRAS CUENTAS POR COBRAR");

        AccountController.insert(ap, ItemController.findForName("BIENES DE CAMBIO"), "MATERIAS PRIMAS");
        AccountController.insert(ap, ItemController.findForName("BIENES DE CAMBIO"), "PRODUCTOS EN PROCESO");
        AccountController.insert(ap, ItemController.findForName("BIENES DE CAMBIO"), "PRODUCTOS TERMINADOS");
        AccountController.insert(ap, ItemController.findForName("BIENES DE CAMBIO"), "MERCADERIAS DE REVENTA");

    }

    /**
     *
     * @param ap
     */
    public static void activoNoCorriente(AccountPlan ap) {
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "MUEBLES Y UTILES");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "AMORT. ACUM. MUEBLES Y UTILES");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "RODADOS");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "AMORT. ACOM. RODADOS");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "INSTALACIONES");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "AMORT. ACOM. INSTALACIONES");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "EDIFICIOS");
        AccountController.insert(ap, ItemController.findForName("BIENES DE USO"), "AMORT. ACOM. EDIFICIOS");
    }

    /**
     *
     * @param ap
     */
    public static void pasivoCorriente(AccountPlan ap) {
        AccountController.insert(ap, ItemController.findForName("DEUDAS COMERCIALES"), "PROVEEDORES");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "SUELDOS A PAGAR");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "SUSS A PAGAR");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "A.E.C. A PAGAR");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "FAECYS A PAGAR");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "LA ESTRELLA A PAGAR");
        AccountController.insert(ap, ItemController.findForName("DEUDAS SOCIALES"), "OTRAS LEYES SOC. A PAGAR");

        AccountController.insert(ap, ItemController.findForName("DEUDAS BANCARIAS"), "PRESTAMOS Y ACUERDOS BANCARIOS");

        AccountController.insert(ap, ItemController.findForName("OTRAS DEUDAS"), "I.V.A DEBITO FISCAL");
        AccountController.insert(ap, ItemController.findForName("OTRAS DEUDAS"), "IVA RESP. NO INSCRIPTO");
        AccountController.insert(ap, ItemController.findForName("OTRAS DEUDAS"), "ALQUILERES A PAGAR");
        AccountController.insert(ap, ItemController.findForName("OTRAS DEUDAS"), "SEGUROS A PAGAR");
    }

    /**
     *
     * @param ap
     */
    public static void patrimonioNetoCapReseResu(AccountPlan ap) {
        AccountController.insert(ap, ItemController.findForName("CAPITAL"), "CAPITAL SOCIAL");
        AccountController.insert(ap, ItemController.findForName("CAPITAL"), "AJUSTE AL CAPITAL");
        AccountController.insert(ap, ItemController.findForName("CAPITAL"), "APORTE IRREVOCABLE A CTA. FUT. SUSC.");

        AccountController.insert(ap, ItemController.findForName("RESERVAS"), "RESERVA LEGAL");

        AccountController.insert(ap, ItemController.findForName("RESULTADOS"), "RESULTADO DEL EJERCICIO");
        AccountController.insert(ap, ItemController.findForName("RESULTADOS"), "RESULTADOS NO ASIGNADOS");
        AccountController.insert(ap, ItemController.findForName("RESULTADOS"), "AJUSTE EJERCICIOS ANTERIORES");
    }

    /**
     *
     * @param ap
     */
    public static void resultados(AccountPlan ap) {
        AccountController.insert(ap, ItemController.findForName("INGRESOS POR VENTAS Y SERVICIOS"), "VENTAS POR MAYOR");
        AccountController.insert(ap, ItemController.findForName("INGRESOS POR VENTAS Y SERVICIOS"), "VENTAS POR MENOR");
        AccountController.insert(ap, ItemController.findForName("INGRESOS POR VENTAS Y SERVICIOS"), "VENTAS AL EXTERIOR");

        AccountController.insert(ap, ItemController.findForName("OTROS INGRESOS"), "DESCUENTOS OBTENIDOS");
        AccountController.insert(ap, ItemController.findForName("OTROS INGRESOS"), "DONIFICACIONES OBTENIDAS");
        AccountController.insert(ap, ItemController.findForName("OTROS INGRESOS"), "INTERESES GANADOS");
        AccountController.insert(ap, ItemController.findForName("OTROS INGRESOS"), "GASTOS RECUPERADOS");
        AccountController.insert(ap, ItemController.findForName("OTROS INGRESOS"), "OTROS INGRESOS");

        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "COSTOS DE MERCADERIAS VENDIDAS");
        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "INSUMOS MATERIAS PRIMAS");
        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "AMORTIZACIONES");
        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "SUELDOS FABRICA");
        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "CARGAS SOCIALES FABRICA");
        AccountController.insert(ap, ItemController.findForName("COSTO DE VENTAS"), "GASTOS VARIOS FABRICA");

        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "SUELDOS COMERCIALIZACION");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "CARGAS SOCIALES COMERCIALIZACION");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "GASTOS DE EMBALAJE Y FLETES");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "MOVILIDAD");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "IMPUESTOS Y TASAS");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "ALQUILERES");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "PUBLICIDAD Y IMPRENTA");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "BONIFICACIONES CONCEDIDAS");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "INGRESOS BRUTOS");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE COMERCIALIZACION"), "GASTOS VARIOS DE COMERCIALIZACION");

        AccountController.insert(ap, ItemController.findForName("GASTOS DE ADMINISTRACION"), "HONORARIOS");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE ADMINISTRACION"), "SUELDOS ADMINISTRACION");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE ADMINISTRACION"), "CARGAS SOCIALES ADMINISTRACION");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE ADMINISTRACION"), "PAPELERIA Y UTILES DE OFICINA");
        AccountController.insert(ap, ItemController.findForName("GASTOS DE ADMINISTRACION"), "GASTOS VARIOS DE ADMINISTRACION");

        AccountController.insert(ap, ItemController.findForName("OTROS EGRESOS"), "SEGUROS");

    }
}