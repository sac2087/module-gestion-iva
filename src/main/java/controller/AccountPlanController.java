/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.Account;
import model.AccountPlan;
import model.Client;

/**
 *
 * @author GDF
 */
public class AccountPlanController {

    /**
     *
     * @param idClient
     * @return
     */
    public static boolean insert(int idClient) {
        Client foundClient = (Client) JPA.findByID(Client.class, idClient);
        AccountPlan newAccountPlan = new AccountPlan(foundClient);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newAccountPlan);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param idAccount
     * @return
     */
    public static boolean addAccount(int id, int idAccount) {
        boolean resultado;
        try {
            AccountPlan foundAccountPlan = (AccountPlan) JPA.findByID(AccountPlan.class, id);
            Account foundAccount = (Account) JPA.findByID(Account.class, idAccount);
            foundAccountPlan.addAccount(foundAccount);
            JPA.beginTransaction();
            resultado = JPA.update(foundAccountPlan);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param idAccount
     * @return
     */
    public static boolean deleteAccount(int id, int idAccount) {
        boolean resultado;
        try {
            AccountPlan foundAccountPlan = (AccountPlan) JPA.findByID(AccountPlan.class, id);
            Account foundAccount = (Account) JPA.findByID(Account.class, idAccount);
            foundAccountPlan.removeAccount(foundAccount);
            JPA.beginTransaction();
            resultado = JPA.update(foundAccountPlan);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            AccountPlan foundAccountPlan = (AccountPlan) JPA.findByID(AccountPlan.class, id);
            JPA.beginTransaction();
            resultado = JPA.delete(foundAccountPlan);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(AccountPlan.class);
    }

    /**
     *
     * @param id
     * @return
     */
    public static AccountPlan findByID(int id) {
        return (AccountPlan) JPA.findByID(AccountPlan.class, id);
    }
}
