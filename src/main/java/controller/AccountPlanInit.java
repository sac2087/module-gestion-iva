/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author lprone
 */
public class AccountPlanInit {

    /**
     *
     */
    public static void initAccountPlanStructure() {
        createCategories();
        createSubCategories();
        createItems();
    }

    private static void createCategories() {
        CategoryController.insert("ACTIVO");
        CategoryController.insert("PASIVO");
        CategoryController.insert("PATRIMONIO NETO");
        CategoryController.insert("RESULTADOS");
    }

    private static void createSubCategories() {

//      ACTIVO
        SubCategoryController.insert("ACTIVO CORRIENTE", "ACTIVO");
        SubCategoryController.insert("ACTIVO NO CORRIENTE", "ACTIVO");
        SubCategoryController.insert("CUENTAS TRANSITORIAS", "ACTIVO");

//      PASIVO
        SubCategoryController.insert("PASIVO CORRIENTE", "PASIVO");
        SubCategoryController.insert("PASIVO NO CORRIENTE", "PASIVO");

//      PATRIMONIO NETO
        SubCategoryController.insert("CAPITAL, RESERVAS Y RESULTADOS", "PATRIMONIO NETO");

//      RESULTADOS
        SubCategoryController.insert("INGRESOS", "RESULTADOS");
        SubCategoryController.insert("GASTOS", "RESULTADOS");
    }

    private static void createItems() {

//      ACTIVO - ACTIVO CORRIENTE
        ItemController.insert("CAJA Y BANCOS", "ACTIVO CORRIENTE");
        ItemController.insert("CREDITOS POR VENTAS", "ACTIVO CORRIENTE");
        ItemController.insert("OTROS CREDITOS", "ACTIVO CORRIENTE");
        ItemController.insert("BIENES DE CAMBIO", "ACTIVO CORRIENTE");

//      ACTIVO - ACTIVO NO CORRIENTE        
        ItemController.insert("BIENES DE USO", "ACTIVO NO CORRIENTE");

//      ACTIVO - CUENTAS TRANSITORIAS
        ItemController.insert("PARTIDAS EN SUSPENSO", "CUENTAS TRANSITORIAS");

//      PASIVO - PASIVO CORRIENTE        
        ItemController.insert("DEUDAS COMERCIALES", "PASIVO CORRIENTE");
        ItemController.insert("DEUDAS SOCIALES", "PASIVO CORRIENTE");
        ItemController.insert("DEUDAS FISCALES", "PASIVO CORRIENTE");
        ItemController.insert("DEUDAS BANCARIAS", "PASIVO CORRIENTE");
        ItemController.insert("CUENTAS PARTICULARES SOCIOS", "PASIVO CORRIENTE");
        ItemController.insert("OTRAS DEUDAS", "PASIVO CORRIENTE");

//      PATRIMONIO NETO - CAPITAL, RESERVAS Y RESULTADOS
        ItemController.insert("CAPITAL", "CAPITAL, RESERVAS Y RESULTADOS");
        ItemController.insert("RESERVAS", "CAPITAL, RESERVAS Y RESULTADOS");
        ItemController.insert("RESULTADOS", "CAPITAL, RESERVAS Y RESULTADOS");

//      RESULTADOS - INGRESOS        
        ItemController.insert("INGRESOS POR VENTAS Y SERVICIOS", "INGRESOS");
        ItemController.insert("OTROS INGRESOS", "INGRESOS");

//      RESULTADOS - GASTOS
        ItemController.insert("COSTO DE VENTAS", "GASTOS");

        ItemController.insert("GASTOS DE COMERCIALIZACION", "GASTOS");
        ItemController.insert("GASTOS DE ADMINISTRACION", "GASTOS");
        ItemController.insert("GASTOS FINANCIEROS", "GASTOS");
        ItemController.insert("OTROS EGRESOS", "GASTOS");
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        initAccountPlanStructure();
    }
}
