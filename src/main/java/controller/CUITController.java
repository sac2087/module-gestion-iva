/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author lprone
 */
public class CUITController {

    /**
     *
     * @param cuit
     * @return
     */
    public static boolean verify(String cuit) {
        if (cuit.length() == 11) {

            int digito = 0;
            int[] mult = {5, 4, 3, 2, 7, 6, 5, 4, 3, 2};
            for (int i = 0; i < mult.length; i++) {
                digito += mult[i] * Integer.parseInt(String.valueOf(cuit.charAt(i)));
            }

            return ((11 - digito % 11) % 11) == Integer.parseInt(cuit.substring(10, 11));
        } else {
            return false;
        }
    }
}
