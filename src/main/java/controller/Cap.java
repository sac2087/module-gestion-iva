/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author lprone
 */
public class Cap {

    boolean cap = false;

    /**
     *
     * @param filename
     */
    public Cap(String filename) {
        if (cap) {
            try {
                doIt(filename);
            } catch (Exception ex) {
            }
        }
    }

    /**
     *
     * @param filename
     * @throws AWTException
     * @throws IOException
     */
    private void doIt(String filename) throws AWTException, IOException {
        BufferedImage pantalla = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        File file = new File("./doc/caps/" + filename + ".jpg");
        ImageIO.write(pantalla, "jpg", file);
    }
}
