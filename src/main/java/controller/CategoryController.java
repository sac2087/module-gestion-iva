/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.Category;

/**
 *
 * @author gdf
 */
public class CategoryController {

    /**
     *
     * @param name
     * @return
     */
    public static boolean insert(String name) {
        Category newCategory = new Category(name.toUpperCase());
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newCategory);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static boolean delete(String name) {
        boolean resultado;
        try {
            Category findCat = (Category) JPA.findByName(Category.class, name.toUpperCase()).get(0);
            JPA.beginTransaction();
            resultado = JPA.delete(findCat);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @param newName
     * @return
     */
    public static boolean editName(String name, String newName) {
        boolean resultado;
        try {
            Category findCat = (Category) JPA.findByName(Category.class, name.toUpperCase()).get(0);
            findCat.setName(newName.toUpperCase());
            JPA.beginTransaction();
            resultado = JPA.update(findCat);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static Category findForName(String name) {
        Category result = null;
        try {
            result = (Category) JPA.findByName(Category.class, name.toUpperCase()).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Category.class);
    }
}
