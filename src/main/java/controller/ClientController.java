/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.AccountPlan;
import model.Client;
import model.FiscCondition;
import model.IdType;

/**
 *
 * @author GDF
 */
public class ClientController {

    /**
     *
     * @param name
     * @param ap
     * @return
     */
    public static boolean insert(String name, AccountPlan ap) {
        Client newClient = new Client(name, ap);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param tipoDoc
     * @param name
     * @param zipCode
     * @param cuit
     * @param address
     * @param iva
     * @param phone
     * @param condFiscal
     * @param email
     * @return
     */
    public static boolean insert(String name, IdType tipoDoc, String cuit, String zipCode, String address, String phone, float iva, FiscCondition condFiscal, String email) {
        Client newClient = new Client(name, tipoDoc, cuit, zipCode, address, phone, iva, condFiscal, email);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param c 
     * @return
     */
    public static boolean delete(Client c) {
        boolean resultado;
        try {
            Client foundClient = (Client) JPA.findByID(Client.class, c.getId());
            JPA.beginTransaction();
            resultado = JPA.delete(foundClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param newName
     * @return
     */
    public static boolean editName(int id, String newName) {
        boolean resultado;
        try {
            Client foundClient = (Client) JPA.findByID(Client.class, id);
            foundClient.setName(newName);
            JPA.beginTransaction();
            resultado = JPA.update(foundClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Client.class);
    }

    /**
     *
     * @param id
     * @param name
     * @param cuit
     * @param address
     * @param phone
     * @param condFiscal
     * @param email
     * @return
     */
    public static boolean editClient(int id, String name, String cuit, String address, String phone, FiscCondition condFiscal, String email) {
        boolean result;
        try {
            Client foundClient = (Client) JPA.findByID(Client.class, id);
            if (name != null) {
                foundClient.setName(name);
            }
            if (cuit != null) {
                foundClient.setCuit(cuit);
            }
            if (address != null) {
                foundClient.setAddress(address);
            }
            if (phone != null) {
                foundClient.setPhone(phone);
            }
            if (condFiscal != null) {
                foundClient.setCondFiscal(condFiscal);
            }
            if (email != null) {
                foundClient.setEmail(email);
            }
            JPA.beginTransaction();
            result = JPA.update(foundClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     *
     * @param id
     * @param iva
     * @return
     */
    public static boolean editIVA(int id, float iva) {
        boolean result;
        try {
            Client foundClient = (Client) JPA.findByID(Client.class, id);
            foundClient.setIva(iva);
            JPA.beginTransaction();
            result = JPA.update(foundClient);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     *
     * @param field
     * @param value
     * @return
     */
    public static List getLike(String field, String value) {
        return JPA.runQuery("SELECT c FROM " + Client.class.getName() + " c WHERE c." + field + " LIKE '" + value.toUpperCase() + "%'");
    }

    /**
     *
     * @param name
     * @return
     */
    public static Client findForName(String name) {
        Client result = null;
        try {
            result = (Client) JPA.findByName(Client.class, name.toUpperCase()).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }
 
    
    
    /**
     *
     * @param doc
     * @return
     */
    public static Client findForDoc(String doc) {
        try {
            return (Client) JPA.runQuery("SELECT c FROM " + Client.class.getName() + " c WHERE cuit= '" + doc + "'").get(0);
        } catch (Exception e) {
            return null;
        }

    }
    
    
    
    /**
     *
     * @param tipo
     * @param doc
     * @return
     */
    public static Client findFor(IdType tipo,String doc) {
        try {
            return (Client) JPA.runQuery("SELECT c FROM " + Client.class.getName() + " c WHERE cuit= '" + doc + "' AND tipoDoc='"+tipo.toString()+"'").get(0);
        } catch (Exception e) {
            return null;
        }

    }    
    
    
    
    
}
