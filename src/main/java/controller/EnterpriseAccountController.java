/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.Account;
import model.Enterprise;
import model.EnterpriseAccount;
import model.TOperacion;
import static view.CargaComprobante.Empresa;

/**
 *
 * @author lprone
 */
public class EnterpriseAccountController {

    /**
     *
     * @param a
     * @param e
     * @return
     */
    public static boolean insert(Account a, Enterprise e) {
        EnterpriseAccount newLink = new EnterpriseAccount(e, a);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newLink);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param ea
     * @return
     */
    public static boolean update(EnterpriseAccount ea) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.update(ea);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param ea
     * @return
     */
    public static boolean delete(EnterpriseAccount ea) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.delete(ea);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param enterprise
     * @param operation
     * @return Dada una empresa y una operacion(Compra,Venta), retorna la cuenta
     * asignada a dicha operacion
     */
    public static Account getAccountOperation(Enterprise enterprise, TOperacion operation) {
        Account resultAcc = null;
        if (Empresa != null) {
            if (operation == TOperacion.VENTA) {
                List<EnterpriseAccount> enterpriseAccount = enterprise.getEnterpriseAccount();
                for (EnterpriseAccount enterpriseAccount1 : enterpriseAccount) {
                    Account aux = enterpriseAccount1.getAccount();
                    if (AccountController.isClient(aux)) {
                        resultAcc = aux;
                    }
                }
            } else if (operation == TOperacion.COMPRA) {
                List<EnterpriseAccount> enterpriseAccount = enterprise.getEnterpriseAccount();
                for (EnterpriseAccount enterpriseAccount1 : enterpriseAccount) {
                    Account aux = enterpriseAccount1.getAccount();
                    if (AccountController.isProvider(aux)) {
                        resultAcc = aux;
                    }
                }
            }
        }
        return resultAcc;
    }

    /**
     * @param e
     * @param a
     * @return
     */
    public static EnterpriseAccount find(Enterprise e, Account a) {
        try {
            return (EnterpriseAccount) JPA.runQuery("SELECT ea FROM " + EnterpriseAccount.class.getName() + " ea WHERE ea.account.id=" + a.getId() + " AND ea.enterprise.id =" + e.getId()).get(0);
        } catch (Exception ex) {
        }
        return null;

    }
}
