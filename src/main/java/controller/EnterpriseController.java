/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import model.Account;
import model.AccountPlan;
import model.Enterprise;
import model.EnterpriseAccount;
import model.FiscCondition;
import model.IdType;
import model.Session;

/**
 *
 * @author lprone
 */
public class EnterpriseController {

    /**
     *
     * @param name
     * @param cuit
     * @param zipCode
     * @param address
     * @param phone
     * @param condFiscal
     * @param iva
     * @param email
     * @param tipoDoc
     * @param enterpriseAccount
     * @return
     */
    public static boolean insert(String name, String cuit, String zipCode, String address, String phone, FiscCondition condFiscal, float iva, String email, IdType tipoDoc, List<EnterpriseAccount> enterpriseAccount) {
        Enterprise newEnterprise = new Enterprise(name, cuit, zipCode, address, phone, condFiscal, iva, email, tipoDoc, enterpriseAccount);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newEnterprise);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param enterprise 
     * @return
     */
    public static boolean delete(Enterprise enterprise) {
        boolean resultado;
        try {
            Enterprise foundEnterprise = (Enterprise) JPA.findByID(Enterprise.class, enterprise.getId());
            JPA.beginTransaction();
            resultado = JPA.delete(foundEnterprise);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @param name
     * @param cuit
     * @param address
     * @param phone
     * @param condFiscal
     * @param email
     * @return
     */
    public static boolean editEnterprise(int id, String name, String cuit, String address, String phone, FiscCondition condFiscal, String email) {
        boolean result;
        try {
            Enterprise foundEnterprise = (Enterprise) JPA.findByID(Enterprise.class, id);
            if (name != null) {
                foundEnterprise.setName(name);
            }
            if (cuit != null) {
                foundEnterprise.setCuit(cuit);
            }
            if (address != null) {
                foundEnterprise.setAddress(address);
            }
            if (phone != null) {
                foundEnterprise.setPhone(phone);
            }
            if (condFiscal != null) {
                foundEnterprise.setCondFiscal(condFiscal);
            }
            if (email != null) {
                foundEnterprise.setEmail(email);
            }
            JPA.beginTransaction();
            result = JPA.update(foundEnterprise);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     *
     * @param id
     * @param iva
     * @return
     */
    public static boolean editIVA(int id, float iva) {
        boolean result;
        try {
            Enterprise foundEnterprise = (Enterprise) JPA.findByID(Enterprise.class, id);
            foundEnterprise.setIva(iva);
            JPA.beginTransaction();
            result = JPA.update(foundEnterprise);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     * @param value Comiezo del nombre
     * @return Retorna todas las empresas que comiencen con el nombre tomado por
     * parametro
     */
    public static List getLike(String value) {
        return JPA.runQuery("SELECT c FROM Enterprise c WHERE c.name LIKE '" + value.toUpperCase() + "%'");

    }

    /**
     * @param value Comiezo del nombre
     * @param type Cliente= 1 , Proveedor= 2
     * @return Retorna las empresas (Clientes o Proveedores) que comiencen con
     * el nombre tomado por parametro y pertenescan al plan de cuentas.
     */
    public static ArrayList<Enterprise> getLikeType(String value, int type) {
        List empresas = JPA.runQuery("SELECT c FROM Enterprise c WHERE c.name LIKE '" + value.toUpperCase() + "%'");
        ArrayList<Enterprise> result = new ArrayList<Enterprise>();
        for (Object object : empresas) {
            List<Account> accountList = AccountController.getAccount((Enterprise) object);
            for (Account account : accountList) {
                if (type == 1) { //clientes
                    if (AccountController.isClient(account)) {
                        result.add((Enterprise) object);
                    }
                }
                if (type == 2) { //proveedores
                    if (AccountController.isProvider(account)) {
                        result.add((Enterprise) object);
                    }
                }
            }

        }

        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Enterprise.class);
    }

    /**
     *
     * @param name
     * @return
     */
    public static Enterprise findForName(String name) {
        Enterprise result = null;
        try {
            result = (Enterprise) JPA.findByName(Enterprise.class, name.toUpperCase()).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    /**
     *
     * @param doc
     * @return
     */
    public static Enterprise findForCuit(String doc) {
        return (Enterprise) JPA.runQuery("SELECT c FROM " + Enterprise.class.getName() + " c WHERE cuit='" + doc + "'").get(0);
    }
    
    
        /**
     *
     * @param tipo 
     * @param doc
     * @return
     */
    public static Enterprise findFor(IdType tipo, String doc) {
        return (Enterprise) JPA.runQuery("SELECT c FROM " + Enterprise.class.getName() + " c WHERE cuit='" + doc + "' AND tipoDoc='"+tipo.toString()+"'").get(0);
    }

    /*NUEVOS METODOS*/
    /**
     *
     * @return Retorna los Clientes pertenecientes al plan de cuentas de la
     * sesion.
     */
    public static List<Enterprise> getClientes() {
        AccountPlan ap = Session.client.getAccountPlan();
        List<Account> accounts = ap.getAccounts();
        List<Enterprise> enterprises = new ArrayList<Enterprise>();
        for (Account account : accounts) {
            if (account.getCategory().getName().compareTo("ACTIVO") == 0) {
                if (account.getEnterpriseAccounts().size() > 0) {
                    for (EnterpriseAccount ea : account.getEnterpriseAccounts()) {
                        enterprises.add(ea.getEnterprise());
                    }
                }
            }
        }
        return enterprises;
    }

    /**
     *
     * @return Retorna los Proveedores pertenecientes al plan de cuenta de la
     * sesion.
     */
    public static List<Enterprise> getProvedores() {
        AccountPlan ap = Session.client.getAccountPlan();
        List<Account> accounts = ap.getAccounts();
        List<Enterprise> enterprises = new ArrayList<Enterprise>();
        for (Account account : accounts) {
            if (account.getCategory().getName().compareTo("PASIVO") == 0) {
                if (account.getEnterpriseAccounts().size() > 0) {
                    for (EnterpriseAccount ea : account.getEnterpriseAccounts()) {
                        enterprises.add(ea.getEnterprise());
                    }
                }
            }
        }
        return enterprises;
    }

    /**
     * @return Retorna todas las empresas pertenecientes al plan de cuentas de
     * la sesion.
     */
    public static List<Enterprise> getEnterprices() {
        AccountPlan ap = Session.client.getAccountPlan();
        List<Account> accounts = ap.getAccounts();
        List<Enterprise> enterprises = new ArrayList<Enterprise>();
        for (Account account : accounts) {
            if (account.getEnterpriseAccounts().size() > 0) {
                for (EnterpriseAccount ea : account.getEnterpriseAccounts()) {
                    enterprises.add(ea.getEnterprise());
                }
            }
        }
        return enterprises;
    }

    /**
     *
     * @param account Toma una cuenta
     * @return retorna las empresas pertenecientes a una cuenta (del plan de
     * cuentas de la sesion).
     */
    public static List<Enterprise> getEnterprise(Account account) {
        List ret = new LinkedList();
        List eal = account.getEnterpriseAccounts();
        for (Object aux : eal) {
            EnterpriseAccount ea = (EnterpriseAccount) aux;
            ret.add(ea);
        }
        return ret;
    }
}
