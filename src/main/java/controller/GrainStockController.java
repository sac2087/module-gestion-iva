/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import model.Client;
import model.Enterprise;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.Pair;
import model.TConcepto;

/**
 *
 * @author lprone
 */
public class GrainStockController {

    /**
     *
     * @param c
     * @param e
     * @param gt
     * @return
     */
    public static int getStock(Client c, Enterprise e, GrainType gt) {
        int stock = 0;
        for (GrainTicket gtk : GrainTicketController.getByClient(c)) {
            for (GrainTicketAccount gta : gtk.getTicket_Accounts()) {
                if ((gta.getConcepto() == TConcepto.NETO || gta.getConcepto() == TConcepto.NETOV) && gta.getGrainType() == gt && gtk.getEnterprise() == e) {
                    switch (gtk.getType().getId()) {
                        case 96:
                            stock += gta.getKg();
                            break;
                        case 97:
                            stock -= gta.getKg();
                            break;
                        case 98:
                            stock -= gta.getKg();
                            break;
                    }
                }
            }
        }
        return stock;
    }

    /**
     *
     * @param c
     * @param gt
     * @return
     */
    public static List<Pair<Enterprise, Integer>> getStock(Client c, GrainType gt) {
        HashSet<Enterprise> hsEnterprise = new HashSet();
        LinkedList<Pair<Enterprise, Integer>> ret = new LinkedList();
        for (GrainTicket gtk : GrainTicketController.getByClient(c)) {
            hsEnterprise.add(gtk.getEnterprise());
            for (Enterprise enterprise : hsEnterprise) {
                ret.add(new Pair<Enterprise, Integer>(enterprise, getStock(c, enterprise, gt)));
            }
        }
        return ret;
    }

    /**
     *
     * @param c
     * @return
     */
    public static List<Pair<GrainType, List<Pair<Enterprise, Integer>>>> getStock(Client c) {
        HashSet<GrainType> hsGrainType = new HashSet();
        for (GrainTicket gtk : GrainTicketController.getByClient(c)) {
            for (GrainTicketAccount gta : gtk.getTicket_Accounts()) {
                hsGrainType.add(gta.getGrainType());
            }
        }
        LinkedList<Pair<GrainType, List<Pair<Enterprise, Integer>>>> ret = new LinkedList();
        for (GrainType grainType : hsGrainType) {
            ret.add(new Pair<GrainType, List<Pair<Enterprise, Integer>>>(grainType, getStock(c, grainType)));
        }
        return ret;
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        System.out.println(getStock((Client) ClientController.getAll().get(0), (Enterprise) EnterpriseController.getAll().get(0), GrainType.MA�Z_PISINGALLO));
    }
}
