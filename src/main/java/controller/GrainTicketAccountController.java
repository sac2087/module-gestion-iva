/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.math.BigDecimal;
import model.Account;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.TConcepto;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class GrainTicketAccountController {

    /**
     *
     * @param price
     * @param ivaPervent
     * @param kg
     * @param account
     * @param ticket
     * @param grainType
     * @param concept
     * @return
     */
    public static boolean insert(BigDecimal price, float ivaPervent, int kg, GrainType grainType, Account account, GrainTicket ticket, TConcepto concept) {
        GrainTicketAccount gta = new GrainTicketAccount(price, ivaPervent, kg, grainType, account, ticket, concept);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(gta);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param newGrainTicketAccount
     * @return
     */
    public static boolean insert(GrainTicketAccount newGrainTicketAccount) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newGrainTicketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param t
     * @param tc
     * @return
     */
    public static GrainTicketAccount get(GrainTicket t, TConcepto tc) {
        for (GrainTicketAccount ta : t.getTicket_Accounts()) {
            if (ta.getConcept() == tc) {
                return ta;
            }
        }
        return null;
    }

    /**
     *
     * @param ticketA
     * @return
     */
    public static boolean delete(GrainTicketAccount ticketA) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.delete(ticketA);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param ticketAccount
     * @return
     */
    public static boolean update(GrainTicketAccount ticketAccount) {
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.update(ticketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }
}
