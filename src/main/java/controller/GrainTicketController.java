/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.TicketController.getClient;
import db.JPA;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import model.Client;
import model.Enterprise;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.TOperacion;
import model.TTicket;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class GrainTicketController {

    /**
     *
     * @param client
     * @param nro
     * @param enterprise
     * @param emissionDate 
     * @param contDate 
     * @param impFDate 
     * @param type
     * @return
     */
    public static boolean insert(Client client, Enterprise enterprise, String nro, Date emissionDate, Date contDate, Date impFDate, TTicket type) {
        GrainTicket grainTicket = new GrainTicket(client, enterprise, nro, emissionDate, contDate, impFDate, type);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(grainTicket);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param client
     * @param enterprise
     * @param nro
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param type
     * @param netoV
     * @param noGrabV
     * @param retencV
     * @param bruto
     * @return
     */
    public static boolean insert(Client client, Enterprise enterprise, String nro, Date emissionDate, Date contDate, Date impFDate, TTicket type, ArrayList<GrainTicketAccount> netoV, ArrayList<GrainTicketAccount> noGrabV, ArrayList<GrainTicketAccount> retencV, GrainTicketAccount bruto) {
        GrainTicket grainTicket = new GrainTicket(client, enterprise, nro, emissionDate, contDate, impFDate, type);
        boolean resultado;
        try {
            JPA.beginTransaction();            
            resultado = JPA.create(grainTicket);
            //GrainTicketAccount a Libro IVA Venta
            for (GrainTicketAccount grainTicketAccount : netoV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            for (GrainTicketAccount grainTicketAccount : noGrabV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            for (GrainTicketAccount grainTicketAccount : retencV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            bruto.setTicket(grainTicket);
            JPA.create(bruto);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }      
    
    /**
     *
     * @param client
     * @param enterprise
     * @param nro
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param type
     * @param netoV
     * @param noGrabV
     * @param retencV
     * @param netoC
     * @param noGrabC
     * @param retencC
     * @param brutoC
     * @param brutoV
     * @return
     */
    public static boolean insert(Client client, Enterprise enterprise, String nro, Date emissionDate, Date contDate, Date impFDate, TTicket type, ArrayList<GrainTicketAccount> netoV, ArrayList<GrainTicketAccount> noGrabV, ArrayList<GrainTicketAccount> retencV, ArrayList<GrainTicketAccount> netoC, ArrayList<GrainTicketAccount> noGrabC, ArrayList<GrainTicketAccount> retencC, GrainTicketAccount brutoC, GrainTicketAccount brutoV) {
        GrainTicket grainTicket = new GrainTicket(client, enterprise, nro, emissionDate, contDate, impFDate, type);
        boolean resultado;
        try {
            JPA.beginTransaction();            
            resultado = JPA.create(grainTicket);
            //GrainTicketAccount a Libro IVA Venta
            for (GrainTicketAccount grainTicketAccount : netoV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            for (GrainTicketAccount grainTicketAccount : noGrabV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            for (GrainTicketAccount grainTicketAccount : retencV) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            //GrainTicketAccount a Libro IVA Compra
            for (GrainTicketAccount grainTicketAccount : netoC) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }  
            for (GrainTicketAccount grainTicketAccount : noGrabC) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }
            for (GrainTicketAccount grainTicketAccount : retencC) {
                grainTicketAccount.setTicket(grainTicket);
                JPA.create(grainTicketAccount);
            }  
            brutoC.setTicket(grainTicket);
            JPA.create(brutoC);
            brutoV.setTicket(grainTicket);
            JPA.create(brutoV);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }    
    
    
    /**
     *
     * @param gs
     * @return
     */
    public static boolean update(GrainTicket gs) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.update(gs);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id 
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            GrainTicket foundGrainTicket = (GrainTicket) JPA.findByID(GrainTicket.class, id);
            JPA.beginTransaction();
            for (GrainTicketAccount grainTicketAccount : foundGrainTicket.getTicket_Accounts()) {
                JPA.delete(grainTicketAccount);
            }
            resultado = JPA.delete(foundGrainTicket);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param num
     * @return
     */
    public static GrainTicket find(String num) {
        try {
            return (GrainTicket) JPA.runQuery("SELECT FROM " + GrainTicket.class.getName() + " gt WHERE gt.nro='" + num + "'").get(0);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @param empresa 
     * @param num
     * @return
     */
    public static GrainTicket find(Enterprise empresa, String num) {
        try {
            return (GrainTicket) JPA.runQuery("SELECT FROM " + GrainTicket.class.getName() + " gt WHERE enterprise.id=" + empresa.getId() + " AND gt.nro='" + num + "'").get(0);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        try {
            return JPA.getAll(GrainTicket.class);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @param c
     * @return
     */
    public static List<GrainTicket> getByClient(Client c) {
        return c.getGrainTickets();
    }

    /**
     *
     * @param c
     * @param e
     * @return
     */
    public static List getByClientAndEnterprise(Client c, Enterprise e) {
        List all = c.getGrainTickets();
        LinkedList<GrainTicket> ret = new LinkedList();
        for (Object gt : all) {
            Enterprise aux = ((GrainTicket) gt).getEnterprise();
            if (aux == e) {
                ret.add((GrainTicket) gt);
            }
        }
        return ret;
    }

    /**
     *
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param tcomp
     * @return
     */
    public static ArrayList<GrainTicket> findTicket(Client cliente, Enterprise enterprise, String compNumber, Integer tcomp) {
        String consulta = "SELECT a FROM " + GrainTicket.class.getName() + " a WHERE ";
        String where = "";
        if (enterprise != null) {
            where += " a.enterprise.id=" + enterprise.getId();
        }
        if (compNumber != null) {
            if (compNumber.length() == 16) {
                where += " AND a.nro='" + compNumber + "'";
            }
        }
        if (cliente != null) {
            where += " AND a.client.id=" + cliente.getId() + "";
        }
        if (tcomp != null) {
            where += " AND a.type.id=" + tcomp;
        }
        consulta += where.startsWith(" AND") ? where.substring(4) : where;
        ArrayList<GrainTicket> resultado = new ArrayList();
        List resultadoAux = JPA.runQuery(consulta);
        if (resultadoAux != null) {
            for (int i = 0; i < resultadoAux.size(); i++) {
                GrainTicket object = (GrainTicket) resultadoAux.get(i);
                resultado.add(object);
            }
        }
        return resultado;
    }
    
    /**
     *
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param tcomp
     * @return
     */
    public static boolean exists(Client cliente, Enterprise enterprise, String compNumber, Integer tcomp) {
        if (findTicket(cliente, enterprise, compNumber, tcomp).size()<=0){
            return false;
        }else{
            return true;
        }
    }

    /**
     *
     * @param b
     */
    public static void main(String[] b) {
//        insert((Client) ClientController.getAll().get(0), (Enterprise) EnterpriseController.getAll().get(0), 97746, 53684, GrainType.MA�Z, BigDecimal.valueOf(380),TTicketController.findForName("1116 A"));
//        for (Object object : getByClient((Client) ClientController.getAll().get(0))) {
//            System.out.println(object);
//        }
//        System.out.println("----------------------");
//
//        for (Object object : getByClientAndEnterprise((Client) ClientController.getAll().get(0), (Enterprise) EnterpriseController.getAll().get(0))) {
//            System.out.println(object);
//        }
//
//        System.out.println("----------------------");
//
//        for (Object object : getByClientAndGrain((Client) ClientController.getAll().get(0), GrainType.CENTENO)) {
//            System.out.println(object);
//        }
//
//        System.out.println("----------------------");
//
//        for (Object object : getByClientAndGrain((Client) ClientController.getAll().get(0), GrainType.SOJA)) {
//            System.out.println(object);
//        }
//
//        System.out.println("----------------------");
    }
}
