/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.LinkedList;
import java.util.List;
import model.Item;
import model.SubCategory;

/**
 *
 * @author gdf
 */
public class ItemController {

    /**
     *
     * @param name
     * @param subCat
     * @return
     */
    public static boolean insert(String name, String subCat) {
        Item newItem = new Item(name.toUpperCase());
        boolean resultado;
        try {
            newItem.setSubCat(SubCategoryController.findForName(subCat.toUpperCase()));
            JPA.beginTransaction();
            resultado = JPA.create(newItem);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static boolean delete(String name) {
        boolean resultado;
        try {
            Item findItem = (Item) JPA.findByName(Item.class, name.toUpperCase()).get(0);
            JPA.beginTransaction();
            resultado = JPA.delete(findItem);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @param newName
     * @return
     */
    public static boolean editName(String name, String newName) {
        boolean resultado;
        try {
            Item findItem = (Item) JPA.findByName(Item.class, name.toUpperCase()).get(0);
            findItem.setName(newName.toUpperCase());
            JPA.beginTransaction();
            resultado = JPA.update(findItem);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static Item findForName(String name) {
        Item result = null;
        try {
            result = (Item) JPA.findByName(Item.class, name.toUpperCase()).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Item.class);
    }

    /**
     *
     * @param sc
     * @return
     */
    public static List getBySubCategory(SubCategory sc) {
        LinkedList ret = new LinkedList();
        for (Object e : JPA.runQuery("select from " + Item.class.getName() + " i")) {
            Item i = (Item) e;
            if (i.getSubCat() == sc) {
                ret.add(i);
            }
        }
//        System.out.println("select i from " + Item.class.getName() + " i where i.subcat.id=" + sc.getId());
        return ret;
    }

    /**
     *
     * @param name
     * @return
     */
    public static List getLike(String name) {
        return JPA.runQuery("SELECT i FROM " + Item.class.getName() + " i WHERE i.name LIKE '" + name + "%'");

    }
}
