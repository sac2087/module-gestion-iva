/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.Category;
import model.SubCategory;

/**
 *
 * @author gdf
 */
public class SubCategoryController {

    /**
     *
     * @param name
     * @param cat
     * @return
     */
    public static boolean insert(String name, String cat) {
        SubCategory newSubCategory = new SubCategory(name.toUpperCase());
        boolean resultado;
        try {
            newSubCategory.setCat(CategoryController.findForName(cat.toUpperCase()));
            JPA.beginTransaction();
            resultado = JPA.create(newSubCategory);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static boolean delete(String name) {
        boolean resultado;
        try {
            SubCategory findSubCat = (SubCategory) JPA.findByName(SubCategory.class, name).get(0);
            JPA.beginTransaction();
            resultado = JPA.delete(findSubCat);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @param newName
     * @return
     */
    public static boolean editName(String name, String newName) {
        boolean resultado;
        try {
            SubCategory findCat = (SubCategory) JPA.findByName(SubCategory.class, name).get(0);
            findCat.setName(newName.toUpperCase());
            JPA.beginTransaction();
            resultado = JPA.update(findCat);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static SubCategory findForName(String name) {
        SubCategory result = null;
        try {
            result = (SubCategory) JPA.findByName(SubCategory.class, name.toUpperCase()).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(SubCategory.class);
    }

    /**
     *
     * @param c
     * @return
     */
    public static List getByCategory(Category c) {
        return JPA.runQuery("select s from " + SubCategory.class.getName() + " s where s.cat.id=" + c.getId());
    }
}
