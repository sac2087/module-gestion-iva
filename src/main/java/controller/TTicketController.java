/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.TOperacion;
import model.TTicket;

/**
 *
 * @author lprone
 */
public class TTicketController {

    /**
     *
     * @param cod
     * @param name
     * @param top
     * @return
     */
    public static boolean insert(int cod, String name, TOperacion top) {
        TTicket newTTicket = new TTicket(cod, name, top);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newTTicket);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            TTicket findTTicket = (TTicket) JPA.findByID(TTicket.class, id);
            JPA.beginTransaction();
            resultado = JPA.delete(findTTicket);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param oldId
     * @param newId
     * @param name
     * @param op
     * @return
     */
    public static boolean edit(int oldId, int newId, String name, TOperacion op) {
        boolean resultado;
        try {
            TTicket findTTicket = (TTicket) JPA.findByID(TTicket.class, oldId);
            findTTicket.setTop(op);
            findTTicket.setId(newId);
            findTTicket.setName(name);
            JPA.beginTransaction();
            resultado = JPA.update(findTTicket);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static TTicket findForName(String name) {
        TTicket result = null;
        try {
            result = (TTicket) JPA.findByName(TTicket.class, name).get(0);
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(TTicket.class);
    }

    /**
     *
     * @return
     */
    public static List getCompra() {
        return JPA.runQuery("SELECT i FROM " + TTicket.class.getName() + " i WHERE top='" + TOperacion.COMPRAVENTA + "' OR top='" + TOperacion.COMPRA + "'");
    }

    /**
     *
     * @return
     */
    public static List getVenta() {
        return JPA.runQuery("SELECT i FROM " + TTicket.class.getName() + " i WHERE top='" + TOperacion.COMPRAVENTA + "' OR top='" + TOperacion.VENTA + "'");
    }

    /**
     *
     * @return
     */
    public static List getCompraVenta() {
        return JPA.runQuery("SELECT i FROM " + TTicket.class.getName() + " i WHERE top='" + TOperacion.COMPRAVENTA + "' OR top='" + TOperacion.VENTA + "'");
    }

    /**
     *
     * @return
     */
    public static List getAgropecuario() {
        return JPA.runQuery("SELECT i FROM " + TTicket.class.getName() + " i WHERE top='" + TOperacion.OPERACION_AGROPECUARIA + "'");
    }

    /**
     *
     * @param top
     * @return
     */
    public static List getByToperation(TOperacion top) {
        if (top == TOperacion.OPERACION_AGROPECUARIA) {
            return getAgropecuario();
        } else {
            return getCompraVenta();
        }
    }

    /**
     *
     * @param name
     * @return
     */
    public static List getLike(String name) {
        return JPA.runQuery("SELECT i FROM " + TTicket.class.getName() + " i WHERE i.name LIKE '" + name.toUpperCase() + "%'");

    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        for (Object o : getByToperation(TOperacion.VENTA)) {
            System.out.println((TTicket) o);
        }
    }
}
