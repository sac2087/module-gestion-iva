/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.TOperacion;

/**
 *
 * @author lprone
 */
public class TTicketInit {

    /**
     *
     */
    public static void init() {
        TTicketController.insert(80, "CIERRE Z", TOperacion.COMPRAVENTA);
        TTicketController.insert(96, "1116 A", TOperacion.OPERACION_AGROPECUARIA);
        TTicketController.insert(97, "1116 B", TOperacion.OPERACION_AGROPECUARIA);
        TTicketController.insert(98, "1116 C", TOperacion.OPERACION_AGROPECUARIA);
        TTicketController.insert(01, "FACTURA A", TOperacion.COMPRAVENTA);
        TTicketController.insert(06, "FACTURA B", TOperacion.COMPRAVENTA);
        TTicketController.insert(11, "FACTURA C", TOperacion.COMPRAVENTA);
        TTicketController.insert(04, "RECIBO A", TOperacion.COMPRAVENTA);
        TTicketController.insert(15, "RECIBO C", TOperacion.COMPRAVENTA);
        TTicketController.insert(81, "TICKET A", TOperacion.COMPRAVENTA);
        TTicketController.insert(82, "TICKET B", TOperacion.COMPRAVENTA);
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        init();
    }
}
