/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.math.BigDecimal;
import java.util.ArrayList;
import model.Account;
import model.TConcepto;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class TicketAccountController {

    /**
     *
     * @param a
     * @param t
     * @param price
     * @param ivaPercent
     * @param concept
     * @return
     */
    public static boolean insert(Account a, Ticket t, BigDecimal price, float ivaPercent, TConcepto concept) {
        TicketAccount ta = new TicketAccount(a, t, price, ivaPercent, concept);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(ta);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param newTicketAccount
     * @return
     */
    public static boolean insert(TicketAccount newTicketAccount) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newTicketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            //JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param ticketAccount
     * @return
     */
    public static boolean update(TicketAccount ticketAccount) {
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.update(ticketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }

    /**
     *
     * @param editTicket
     * @param netoTotalList
     * @param noGravOpExeList
     * @param retenPercepList
     * @return
     */
    public static boolean updateAll(Ticket editTicket, ArrayList<TicketAccount> netoTotalList, ArrayList<TicketAccount> noGravOpExeList, ArrayList<TicketAccount> retenPercepList) {
        //Edito o elimino los ticket account que ya existian en el ticket
        try {
            boolean editar; // Variable utilizada para decidir, si se debe editar o no un ticket 
            for (TicketAccount ticketAccount : editTicket.getTicket_Accounts()) {
                editar = false;
                int i = 0;
                // Este Switch, controla si un ticket 'i' existe en el update, y ese caso lo actualiza
                switch (ticketAccount.getConcept()) {
                    case NETO:
                        i = 0;
                        while (i < netoTotalList.size()) {
                            if (netoTotalList.get(i).getId() == ticketAccount.getId()) {
                                ticketAccount.setAccount(netoTotalList.get(i).getAccount());
                                ticketAccount.setPrice(netoTotalList.get(i).getPrice());
                                editar = true;
                            }
                            i++;
                        }
                        break;
                    case IVA:
                        i = 0;
                        while (i < netoTotalList.size()) {
                            if (netoTotalList.get(i).getId() == ticketAccount.getId()) {
                                ticketAccount.setAccount(netoTotalList.get(i).getAccount());
                                ticketAccount.setPrice(netoTotalList.get(i).getPrice());
                                ticketAccount.setIvaPercent(netoTotalList.get(i).getIvaPercent());
                                editar = true;
                            }
                            i++;
                        }
                        break;
                    case NO_GRAVADO:
                        i = 0;
                        while (i < noGravOpExeList.size()) {
                            if (noGravOpExeList.get(i).getId() == ticketAccount.getId()) {
                                ticketAccount.setAccount(noGravOpExeList.get(i).getAccount());
                                ticketAccount.setPrice(noGravOpExeList.get(i).getPrice());
                                editar = true;
                            }
                            i++;
                        }
                        break;
                    case RET_PERCEPCIONES:
                        i = 0;
                        while (i < retenPercepList.size()) {
                            if (retenPercepList.get(i).getId() == ticketAccount.getId()) {
                                ticketAccount.setAccount(retenPercepList.get(i).getAccount());
                                ticketAccount.setPrice(retenPercepList.get(i).getPrice());
                                editar = true;
                            }
                            i++;
                        }
                        break;
                    case BRUTO:
                        BigDecimal ctotal = BigDecimal.ZERO;
                        for (TicketAccount ticketAccount2 : netoTotalList) {
                            ctotal = ctotal.add(ticketAccount2.getPrice());
                        }
                        for (TicketAccount ticketAccount2 : noGravOpExeList) {
                            ctotal = ctotal.add(ticketAccount2.getPrice());
                        }
                        for (TicketAccount ticketAccount2 : retenPercepList) {
                            ctotal = ctotal.add(ticketAccount2.getPrice());
                        }
                        ticketAccount.setPrice(ctotal);
                        editar = true;
                        break;
                }
                // Si el ticket fue editado realiza el update, en caso contrario lo borra
                if (editar) {
                    TicketAccountController.update(ticketAccount);
                } else {
                    TicketAccountController.delete(ticketAccount);
                }
            }
            //agrego nuevos ticket account, en el caso de que se hallan creado
            for (TicketAccount ticketAccount : netoTotalList) {
                if (ticketAccount.getId() == 0) {
                    ticketAccount.setTicket(editTicket);
                    TicketAccountController.insert(ticketAccount);
                }
            }
            for (TicketAccount ticketAccount : noGravOpExeList) {
                if (ticketAccount.getId() == 0) {
                    ticketAccount.setTicket(editTicket);
                    TicketAccountController.insert(ticketAccount);
                }
            }
            for (TicketAccount ticketAccount : retenPercepList) {
                if (ticketAccount.getId() == 0) {
                    ticketAccount.setTicket(editTicket);
                    TicketAccountController.insert(ticketAccount);
                }
            }
            //En caso de que un Ticket no tenga no tenga ningun ticketAccount, es borrado.
            boolean resultado;
            if (netoTotalList.size() == 0 && noGravOpExeList.size() == 0 && retenPercepList.size() == 0) {
                resultado = TicketController.delete(editTicket.getId());
            } else {
                resultado = TicketController.update(editTicket);
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            TicketAccount foundTicketAccount = (TicketAccount) JPA.findByID(TicketAccount.class, id);
            JPA.beginTransaction();
            resultado = JPA.delete(foundTicketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param ticketA
     * @return
     */
    public static boolean delete(TicketAccount ticketA) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.delete(ticketA);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }
}
