/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import model.Account;
import model.Client;
import model.Enterprise;
import model.Session;
import model.TOperacion;
import model.TTicket;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author GDF
 */
public class TicketController {

    /**
     *
     * @param number
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param tOp
     * @param type
     * @param enterprise
     * @return
     */
    public static boolean insert(String number, Date emissionDate, Date contDate, Date impFDate, TTicket type, Enterprise enterprise, TOperacion tOp) {
        Ticket newTicket = new Ticket(number, emissionDate, contDate, impFDate, type, enterprise, tOp);
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.create(newTicket);
            JPA.commitTransaction();
        } catch (Exception e) {
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }

    /**
     *
     * @param number
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param type
     * @param enterprise
     * @param tOp
     * @param ticketAccount1
     * @param ticketAccount2
     * @param ticketAccount3
     * @param bruto
     * @return
     */
    
    public static boolean insert(String number, Date emissionDate, Date contDate, Date impFDate, TTicket type, Enterprise enterprise, TOperacion tOp, ArrayList<TicketAccount> ticketAccount1, ArrayList<TicketAccount> ticketAccount2, ArrayList<TicketAccount> ticketAccount3, TicketAccount bruto) {
        Ticket newTicket = new Ticket(number, emissionDate, contDate, impFDate, type, enterprise, tOp);
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.create(newTicket);
            for (TicketAccount ticketAccount : ticketAccount1) {
                ticketAccount.setTicket(newTicket);
                JPA.create(ticketAccount);
            }
            for (TicketAccount ticketAccount : ticketAccount2) {
                ticketAccount.setTicket(newTicket);
                JPA.create(ticketAccount);
            }
            for (TicketAccount ticketAccount : ticketAccount3) {
                ticketAccount.setTicket(newTicket);
                JPA.create(ticketAccount);
            }
            bruto.setTicket(newTicket);
            JPA.create(bruto);
            JPA.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }

    /**
     *
     * @param t
     * @return
     */
    public static Client getClient(Ticket t) {
        return t.getTicket_Accounts().get(0).getAccount().getAccountPlan().getClient();
    }

    /**
     *
     * @param id
     * @return
     */
    public static Ticket findByID(int id) {
        return (Ticket) JPA.findByID(Ticket.class, id);
    }

    /**
     *
     * @param enterprise
     * @param compNumber
     * @return
     */
    public static List findTicket(Enterprise enterprise, String compNumber) {
        return JPA.runQuery("SELECT a FROM Ticket a WHERE a.enterprise.id=" + enterprise.getId() + " AND a.nro='" + compNumber + "'");
    }

    /**
     *
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param operacion
     * @param tcomp
     * @return
     */
    public static ArrayList<Ticket> findTicket(Client cliente, Enterprise enterprise, String compNumber, TOperacion operacion, Integer tcomp) {
        String consulta = "SELECT a FROM " + Ticket.class.getName() + " a WHERE ";
        String where = "";
        if (enterprise != null) {
            where += " a.enterprise.id=" + enterprise.getId();
        }
        if (compNumber != null) {
            if (compNumber.length() == 16) {
                where += " AND a.nro='" + compNumber + "'";
            }
        }

        if (operacion != null) {
            where += " AND a.tOp='" + operacion.toString() + "'";
        }
        if (tcomp != null) {
            where += " AND a.type.id=" + tcomp;
        }
        consulta += where.startsWith(" AND") ? where.substring(4) : where;
        ArrayList<Ticket> resultado = new ArrayList();
        List resultadoAux = JPA.runQuery(consulta);
        if (resultadoAux != null) {
            for (int i = 0; i < resultadoAux.size(); i++) {
                Ticket object = (Ticket) resultadoAux.get(i);
                if (getClient(object) == cliente) {
                    resultado.add(object);
                }

            }
        }
        return resultado;

    }

    /**
     *
     * @param ticket
     * @return
     */
    public static boolean update(Ticket ticket) {
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.update(ticket);
            JPA.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        return JPA.getAll(Ticket.class);
    }

    /**
     *
     * @param client
     * @return
     */
    public static List getAll(Client client) {
        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        for (Account a : client.getAccountPlan().getAccounts()) {
            for (TicketAccount ta : a.getTicket_Accounts()) {
                Ticket t = ta.getTicket();
                tickets.add(t);
            }
        }
        System.out.println(tickets.size());
        HashSet<Ticket> hsTickets = new HashSet<Ticket>();
        hsTickets.addAll(tickets);
        return new LinkedList(hsTickets);
    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean delete(int id) {
        boolean resultado;
        try {
            Ticket foundTicket = (Ticket) JPA.findByID(Ticket.class, id);
            JPA.beginTransaction();
            for (TicketAccount ticketAccount : foundTicket.getTicket_Accounts()) {
                JPA.delete(ticketAccount);
            }
            resultado = JPA.delete(foundTicket);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }
    /**
     * 
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param operacion
     * @param tcomp
     * @return False si no existe en la base de datos, true en caso contrario.
     */
    public static boolean exists(Client cliente, Enterprise enterprise, String compNumber, TOperacion operacion, Integer tcomp){
        if (findTicket(cliente, enterprise, compNumber, operacion, tcomp).size()<=0){
            return false;
        }else{
            return true;
        }
    }
    
    
}
