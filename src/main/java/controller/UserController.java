/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.User;

/**
 *
 * @author gdf
 */
public class UserController {

    /**
     *
     * @param name
     * @param clave
     * @param role
     * @return
     */
    public static boolean insert(String name, String clave, String role) {
        crypt cr = new crypt();
        String encriptedPass = "";
        try {
            encriptedPass = cr.encrypt(clave);
        } catch (Exception e) {
        }

        User newUser = new User(name, encriptedPass, role);
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.create(newUser);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @return
     */
    public static boolean delete(String name) {
        boolean resultado;
        try {
            User newUser = (User) JPA.findByID(User.class, name);
            JPA.beginTransaction();
            resultado = JPA.delete(newUser);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @param newPass
     * @return
     */
    public static boolean changePass(String name, String newPass) {
        boolean resultado;
        try {
            User newUser = (User) JPA.findByID(User.class, name);

            crypt cr = new crypt();
            String encriptedPass = "";
            try {
                encriptedPass = cr.encrypt(newPass);
            } catch (Exception e) {
            }

            newUser.setPass(encriptedPass);
            JPA.beginTransaction();
            resultado = JPA.update(newUser);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param name
     * @param role
     * @return
     */
    public static boolean changeRole(String name, String role) {
        boolean resultado;
        try {
            User newUser = (User) JPA.findByID(User.class, name);
            newUser.setRole(role);
            JPA.beginTransaction();
            resultado = JPA.update(newUser);
            JPA.commitTransaction();
        } catch (Exception e) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @return
     */
    public static List getAllUsers() {
        return JPA.getAll(User.class);
    }
}