package db;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author gdf
 */
public class DbConnection {

    private static DbConnection instance;
    private static EntityManagerFactory emf;
    private static EntityManager em;

    /**
     *
     * @return
     */
    public synchronized static EntityManager getInstance() {
        if (instance == null) {
            instance = new DbConnection();
        }
        return em;
    }

    /**
     *
     */
    public DbConnection() {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("./config.properties"));
        } catch (Exception ex) {
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        emf = Persistence.createEntityManagerFactory("moduloiva", props);
        em = emf.createEntityManager();
    }

    /**
     *
     */
    public void closeConnection() {
        em.close();
        emf.close();
    }
}
