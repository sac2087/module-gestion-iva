package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Account")
public class Account implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    String name;
//    Relations
    @ManyToOne
    private AccountPlan accountPlan;
    @OneToMany
    private List<Account> child;
    @ManyToOne
    Item item;
    @OneToMany(mappedBy = "account")
    private List<TicketAccount> ticket_Accounts;
    @OneToMany(mappedBy = "account")
    private List<EnterpriseAccount> enterpriseAccounts;

//    Methods 
    /**
     *
     * @param ap
     * @param it
     * @param name
     */
    public Account(AccountPlan ap, Item it, String name) {
        this.accountPlan = ap;
        this.item = it;
        this.name = name.toUpperCase();
    }

    /**
     *
     * @param a
     * @param ap
     * @param it
     * @param name
     */
    public Account(Account a, AccountPlan ap, Item it, String name) {
        this.accountPlan = ap;
        this.item = it;
        this.name = name.toUpperCase();
        this.child.add(a);
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @param child
     */
    public void setChild(List<Account> child) {
        this.child = child;
    }

    /**
     *
     * @return
     */
    public List<Account> getChild() {
        return child;
    }

    /**
     *
     * @param a
     */
    public void addChild(Account a) {
        child.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeChild(Account a) {
        child.remove(a);
    }

    /**
     *
     * @param a
     */
    public void addEnterprice(EnterpriseAccount a) {
        enterpriseAccounts.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeEnterprise(EnterpriseAccount a) {
        enterpriseAccounts.remove(a);
    }

    /**
     *
     * @return
     */
    public Item getItem() {
        return item;
    }

    /**
     *
     * @param item
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     *
     * @return
     */
    public AccountPlan getAccountPlan() {
        return accountPlan;
    }

    /**
     *
     * @param accountPlan
     */
    public void setAccountPlan(AccountPlan accountPlan) {
        this.accountPlan = accountPlan;
    }

    /**
     *
     * @return
     */
    public List<TicketAccount> getTicket_Accounts() {
        return ticket_Accounts;
    }

    /**
     *
     * @param ticket_Accounts
     */
    public void setTicket_Accounts(List<TicketAccount> ticket_Accounts) {
        this.ticket_Accounts = ticket_Accounts;
    }

    /**
     *
     * @return
     */
    public String getCompleteId() {
        String id = getItem().getSubCat().getCat().getId() + "." + item.getSubCat().getId() + "." + item.getId() + "." + getId();
        return id;
    }

    /**
     *
     * @return
     */
    public List<EnterpriseAccount> getEnterpriseAccounts() {
        return enterpriseAccounts;
    }

    /**
     *
     * @return
     */
    public Category getCategory() {
        return item.getSubCat().getCat();
    }

    @Override
    public String toString() {
        return name;
    }
}
