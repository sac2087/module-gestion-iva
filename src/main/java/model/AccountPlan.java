package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "AccountPlan")
public class AccountPlan implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
//  Relations
    @OneToMany(mappedBy = "accountPlan")
    List<Account> accounts;
    @OneToOne
    private Client client;

//    Methods 
    /**
     *
     * @param client
     */
    public AccountPlan(Client client) {
        this.client = client;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public List<Account> getAccounts() {
        return accounts;
    }

    /**
     *
     * @param accounts
     */
    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     *
     * @param a
     */
    public void addAccount(Account a) {
        accounts.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeAccount(Account a) {
        accounts.remove(a);
    }

    /**
     *
     * @param client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     *
     * @return
     */
    public Client getClient() {
        return client;
    }
}
