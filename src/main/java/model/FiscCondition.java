/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lprone
 */
public enum FiscCondition {
    
    /**
     *
     */
    IVA_RESPONSABLE_INSCRIPTO,
    /**
     *
     */
    IVA_RESPONSABLE_NO_INSCRIPTO,
    /**
     *
     */
    IVA_NO_RESPONSABLE,
    /**
     *
     */
    IVA_SUJETO_EXENTO,
    /**
     *
     */
    CONSUMIDOR_FINAL,
    /**
     *
     */
    RESPONSABLE_MONOTRIBUTO,
    /**
     *
     */
    SUJETO_NO_CATEGORIZADO,
    /**
     *
     */
    PROVEEDOR_DEL_EXTERIOR,
    /**
     *
     */
    CLIENTE_DEL_EXTERIOR,
    /**
     *
     */
    IVA_LIBERADO_LEY_N�_19640,
    /**
     *
     */
    IVA_RESPONSABLE_INSCRIPTO_AGENTE_DE_PERCEPCI�N,
    /**
     *
     */
    PEQUE�O_CONTRIBUYENTE_EVENTUAL,
    /**
     *
     */
    MONOTRIBUTISTA_SOCIAL,
    /**
     *
     */
    PEQUE�O_CONTRIBUYENTE_EVENTUAL_SOCIAL;

    /**
     *
     * @param id
     * @return
     */
    public static String valueOf(FiscCondition id) {
        switch (id) {
            case IVA_RESPONSABLE_INSCRIPTO:
                return "01";
            case IVA_RESPONSABLE_NO_INSCRIPTO:
                return "02";
            case IVA_NO_RESPONSABLE:
                return "03";
            case IVA_SUJETO_EXENTO:
                return "04";
            case CONSUMIDOR_FINAL:
                return "05";
            case RESPONSABLE_MONOTRIBUTO:
                return "06";
            case SUJETO_NO_CATEGORIZADO:
                return "07";
            case PROVEEDOR_DEL_EXTERIOR:
                return "08";
            case CLIENTE_DEL_EXTERIOR:
                return "09";
            case IVA_LIBERADO_LEY_N�_19640:
                return "10";
            case IVA_RESPONSABLE_INSCRIPTO_AGENTE_DE_PERCEPCI�N:
                return "11";
            case PEQUE�O_CONTRIBUYENTE_EVENTUAL:
                return "12";
            case MONOTRIBUTISTA_SOCIAL:
                return "13";
            case PEQUE�O_CONTRIBUYENTE_EVENTUAL_SOCIAL:
                return "14";
            default:
                return "";
        }
    }
    
}
