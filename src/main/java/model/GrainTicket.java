/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "GrainTicket")
public class GrainTicket implements Serializable, Comparable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    Client client;
    @ManyToOne
    Enterprise enterprise;
    @Basic
    private String nro;
    @Basic
    private Date emissionDate;
    @Basic
    private Date contDate;
    @Basic
    private Date impFDate;
    @ManyToOne
    private TTicket type;
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL)
    private List<GrainTicketAccount> ticket_Accounts;

    /**
     *
     * @param client
     * @param enterprise
     * @param nro
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param type
     */
    public GrainTicket(Client client, Enterprise enterprise, String nro, Date emissionDate, Date contDate, Date impFDate, TTicket type) {
        this.client = client;
        this.enterprise = enterprise;
        this.nro = nro;
        this.emissionDate = emissionDate;
        this.contDate = contDate;
        this.impFDate = impFDate;
        this.type = type;
    }

    /**
     *
     */
    public GrainTicket() {
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Client getClient() {
        return client;
    }

    /**
     *
     * @param client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     *
     * @return
     */
    public Enterprise getEnterprise() {
        return enterprise;
    }

    /**
     *
     * @param enterprise
     */
    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    /**
     *
     * @return
     */
    public TTicket getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(TTicket type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public Date getEmissionDate() {
        return emissionDate;
    }

    /**
     *
     * @param emissionDate
     */
    public void setEmissionDate(Date emissionDate) {
        this.emissionDate = emissionDate;
    }

    /**
     *
     * @return
     */
    public Date getContDate() {
        return contDate;
    }

    /**
     *
     * @param contDate
     */
    public void setContDate(Date contDate) {
        this.contDate = contDate;
    }

    /**
     *
     * @return
     */
    public Date getImpFDate() {
        return impFDate;
    }

    /**
     *
     * @param impFDate
     */
    public void setImpFDate(Date impFDate) {
        this.impFDate = impFDate;
    }

    /**
     *
     * @return
     */
    public List<GrainTicketAccount> getTicket_Accounts() {
        return ticket_Accounts;
    }

    /**
     *
     * @param ticket_Accounts
     */
    public void setTicket_Accounts(List<GrainTicketAccount> ticket_Accounts) {
        this.ticket_Accounts = ticket_Accounts;
    }

    /**
     *
     * @return
     */
    public String getNro() {
        return nro;
    }

    /**
     *
     * @param nro
     */
    public void setNro(String nro) {
        this.nro = nro;
    }

    @Override
    public String toString() {
        return id + " " + type;
    }

    public int compareTo(Object o) {
        return this.emissionDate.compareTo(((GrainTicket) o).emissionDate);
    }
}
