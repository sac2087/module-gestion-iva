package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Item")
public class Item implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique = true)
    private String name;
//    Relations    
    @Basic
    @ManyToOne
    private SubCategory subCat;
    @OneToMany(mappedBy = "item", cascade = CascadeType.REMOVE)
    private List<Account> accounts;

    //    Methods    
    /**
     *
     * @param name
     */
    public Item(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public SubCategory getSubCat() {
        return subCat;
    }

    /**
     *
     * @param subCat
     */
    public void setSubCat(SubCategory subCat) {
        this.subCat = subCat;
    }

    /**
     *
     * @param a
     */
    public void addAccount(Account a) {
        accounts.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeAccount(Account a) {
        accounts.remove(a);
    }

    /**
     *
     * @return
     */
    public List<Account> getAccounts() {
        return accounts;
    }

    @Override
    public String toString() {
        return name;
    }
}
