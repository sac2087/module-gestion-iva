package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "TTicket")
public class TTicket implements Serializable {

//  Atributes
    @Id
    private int id;
    @Column(unique = true)
    private String name;
    private TOperacion top;
//    Relations    
    @OneToMany(mappedBy = "type", cascade = CascadeType.REMOVE)
    private List<Ticket> tickets;

//    Methods
    /**
     *
     */
    public TTicket() {
    }

    /**
     *
     * @param id
     * @param name
     * @param top
     */
    public TTicket(int id, String name, TOperacion top) {
        this.id = id;
        this.name = name.toUpperCase();
        this.top = top;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public List<Ticket> getTickets() {
        return tickets;
    }

    /**
     *
     * @param tickets
     */
    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    /**
     *
     * @param ticket
     */
    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    /**
     *
     * @param ticket
     */
    public void removeTicket(Ticket ticket) {
        tickets.remove(ticket);
    }

    /**
     *
     * @return
     */
    public TOperacion getTop() {
        return top;
    }

    /**
     *
     * @param top
     */
    public void setTop(TOperacion top) {
        this.top = top;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }
}
