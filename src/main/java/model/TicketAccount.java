/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Ticket_Account")
public class TicketAccount implements Serializable {

//    Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(scale=2)
    BigDecimal price;
    float ivaPercent;
//    Relations
    @ManyToOne
    Account account;
    @ManyToOne
    Ticket ticket ;
    @Basic
    TConcepto concept;

    //    Methods
    /**
     *
     * @param a
     * @param t
     * @param price
     * @param ivaPercent
     * @param concept
     */
    public TicketAccount(Account a, Ticket t, BigDecimal price, float ivaPercent, TConcepto concept) {
        this.price = price;
        this.ivaPercent = ivaPercent;
        this.account = a;
        this.ticket = t;
        this.concept = concept;
    }

    /**
     *
     * @return
     */
    public Account getAccount() {
        return account;
    }

    /**
     *
     * @param account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     *
     * @return
     */
    public Ticket getTicket() {
        return ticket;
    }

    /**
     *
     * @param ticket
     */
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    /**
     *
     * @return
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public TConcepto getConcepto() {
        return concept;
    }

    /**
     *
     * @param concept
     */
    public void setConcepto(TConcepto concept) {
        this.concept = concept;
    }

    /**
     *
     * @return
     */
    public float getIvaPercent() {
        return ivaPercent;
    }

    /**
     *
     * @param ivaPercent
     */
    public void setIvaPercent(float ivaPercent) {
        this.ivaPercent = ivaPercent;
    }

    /**
     *
     * @return
     */
    public TConcepto getConcept() {
        return concept;
    }

    /**
     *
     * @param concept
     */
    public void setConcept(TConcepto concept) {
        this.concept = concept;
    }
}
