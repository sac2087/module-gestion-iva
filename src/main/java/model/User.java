package model;

import java.io.Serializable;
import javax.jdo.annotations.PrimaryKey;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author lprone
 */
@Entity
public class User implements Serializable {

//  Atributes
    @Id
    @PrimaryKey
    String userName = null;
    @Basic
    String pass = null;
    @Basic
    String role = null;

//    Methods    
    /**
     *
     */
    protected User() {
    }

    /**
     *
     * @param userName
     * @param pass
     * @param role
     */
    public User(String userName, String pass, String role) {
        this.userName = userName;
        this.pass = pass;
        this.role = role;
    }

    @Override
    public String toString() {
        return userName + " - " + role;
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @return
     */
    public String getPass() {
        return pass;
    }

    /**
     *
     * @return
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @param pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     *
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return this;
    }
}
