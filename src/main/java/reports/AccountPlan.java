/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import model.Account;
import model.Category;
import model.Item;
import model.Session;
import model.SubCategory;

/**
 *
 * @author lprone
 */
public class AccountPlan {

    /**
     *
     * @param file
     */
    public void createPdf(String file) {
        /*Declaramos documento como un objeto Document
         Asignamos el tama�o de hoja y los margenes */
        Document documento = new Document(PageSize.A4);

        //writer es declarado como el m�todo utilizado para escribir en el archivo
        PdfWriter writer = null;

        try {
            //Obtenemos la instancia del archivo a utilizar
            writer = PdfWriter.getInstance(documento, new FileOutputStream(file));
        } catch (Exception ex) {
            ex.getMessage();
        }

        //Abrimos el documento para edici�n
        documento.open();

        try {
            //Obtenemos la instancia de la imagen
//            System.out.println(System.getProperty("user.dir") + "\\src\\main\\resources\\img\\logo_pdf.png");
            Image imagen = Image.getInstance(this.getClass().getResource("/img/logo_pdf.png"));
            //Alineamos la imagen al centro
            imagen.setAlignment(Image.ALIGN_LEFT);
            //Escalamos la imagen al 50%
            imagen.scalePercent(50);
            //Agregamos la imagen al documento
            documento.add(imagen);
        } catch (Exception e) {
            e.getMessage();
        }

        Paragraph title = new Paragraph();
        title.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(FontFactory.getFont("Sans", 20, Font.BOLD, BaseColor.BLUE));
        title.add("Plan de Cuentas");

        Paragraph client = new Paragraph();
        client.setAlignment(Paragraph.ALIGN_CENTER);
        client.setFont(FontFactory.getFont("Sans", 15, Font.ITALIC, BaseColor.BLACK));
        client.add(Session.client.getName());

        try {

            documento.add(title);
            documento.add(client);
            documento.add(new Paragraph(" "));

            documento.add(tabla());
        } catch (DocumentException ex) {
            ex.getMessage();
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }

    //M�todo para crear la tabla
    private static PdfPTable tabla() {
        PdfPTable tabla = new PdfPTable(5);

        tabla.setWidthPercentage(100);

        PdfPCell c1 = new PdfPCell(new Phrase(" "));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        c1 = new PdfPCell(new Phrase("CATEGORIA"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        c1 = new PdfPCell(new Phrase("SUBCATEGORIA"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        c1 = new PdfPCell(new Phrase("ITEM"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        c1 = new PdfPCell(new Phrase("CUENTA"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        tabla.setHeaderRows(1);

        Item i_old = null;
        SubCategory sc_old = null;
        Category c_old = null;

        for (Object o : Session.client.getAccountPlan().getAccounts()) {
            Account a = (Account) o;
            Item i = a.getItem();
            SubCategory sc = i.getSubCat();
            Category c = sc.getCat();
            tabla.addCell(a.getCompleteId());
            tabla.addCell(c == c_old ? "" : c.toString());
            tabla.addCell(sc == sc_old ? "" : sc.toString());
            tabla.addCell(i == i_old ? "" : i.toString());
            tabla.addCell(a.getName());
            i_old = i;
            sc_old = sc;
            c_old = c;
        }
        return tabla;
    }
}
