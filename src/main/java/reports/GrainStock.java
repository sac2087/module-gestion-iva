/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import controller.ClientController;
import controller.GrainTicketAccountController;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import model.Client;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.Session;
import model.TConcepto;

/**
 *
 * @author lprone
 */
public class GrainStock {

    /**
     *
     * @param file
     */
    public void createPdf(String file) {
        /*Declaramos documento como un objeto Document
         Asignamos el tama�o de hoja y los margenes */
        Document documento = new Document(PageSize.A4, 15, 15, 15, 0);

        //writer es declarado como el m�todo utilizado para escribir en el archivo
        PdfWriter writer = null;

        try {
            //Obtenemos la instancia del archivo a utilizar
            writer = PdfWriter.getInstance(documento, new FileOutputStream(file));
            Rectangle rct = new Rectangle(PageSize.A4.getRight() - 5, PageSize.A4.getTop() - 15);
            //Definimos un nombre y un tama�o para el PageBox los nombres posibles son: �crop�, �trim�, �art� and �bleed�.
            writer.setBoxSize("art", rct);
            PdfPageEventHelper event = new GrainStock.PageNumber();
            writer.setPageEvent(event);
        } catch (Exception ex) {
            ex.getMessage();
        }

        //Abrimos el documento para edici�n
        documento.open();

        try {
            //Obtenemos la instancia de la imagen
            Image imagen = Image.getInstance(this.getClass().getResource("/img/logo_pdf.png"));
            //Alineamos la imagen 
            imagen.setAlignment(Image.ALIGN_LEFT);
            //Escalamos la imagen 
            imagen.scalePercent(50);
            //Agregamos la imagen al documento
            documento.add(imagen);
        } catch (Exception e) {
            e.getMessage();
        }

        Paragraph title = new Paragraph();
        title.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 16));
        title.add("STOCK DE GRANOS");

        Paragraph client = new Paragraph();
        client.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 14));
        client.add(Session.client.getName() + "\n");
        client.add(Session.client.getCuit());

        try {
            documento.add(title);
            documento.add(client);
            documento.add(new Paragraph(" "));
            documento.add(tabla());
        } catch (DocumentException ex) {
            ex.getMessage();
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }

    /**
     *
     * @param value
     * @return
     */
    private static PdfPCell cellText(String value) {
        Paragraph cellTitle = new Paragraph();
        cellTitle.setAlignment(Paragraph.ALIGN_CENTER);
        cellTitle.setFont(new Font(Font.FontFamily.HELVETICA, 6));
        cellTitle.add(value);
        PdfPCell cell = new PdfPCell(cellTitle);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private static PdfPCell cellNumber(String value) {
        Paragraph cellTitle = new Paragraph();
        cellTitle.setAlignment(Paragraph.ALIGN_CENTER);
        cellTitle.setFont(new Font(Font.FontFamily.HELVETICA, 6));
        cellTitle.add(value);
        PdfPCell cell = new PdfPCell(cellTitle);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    /**
     *
     * @param value
     * @return
     */
    private static PdfPCell cellTitle(String value) {
        Paragraph title = new Paragraph();
        title.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 10));
        title.add(value);
        PdfPCell cellTitle = new PdfPCell(title);
        cellTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellTitle.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellTitle.setBorder(Rectangle.BOTTOM);
        return cellTitle;
    }

    //M�todo para crear la tabla
    private static PdfPTable tabla() {
        PdfPTable tabla = new PdfPTable(8);

        tabla.setWidthPercentage(100);

        tabla.addCell(cellTitle("FECHA"));
        tabla.addCell(cellTitle("T. CPTE"));
        tabla.addCell(cellTitle("N� CPTE"));
        tabla.addCell(cellTitle("GRANO"));
        tabla.addCell(cellTitle("ALTA/BAJA STOCK"));
        tabla.addCell(cellTitle("PRECIO"));
        tabla.addCell(cellTitle("EMPRESA"));
        tabla.addCell(cellTitle("TOTAL"));

        tabla.setHeaderRows(1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<GrainTicket> gt = Session.client.getGrainTickets();
        for (GrainTicket grainTicket : gt) {
            GrainTicketAccount gta = GrainTicketAccountController.get(grainTicket, grainTicket.getType().getId() == 96
                    ? TConcepto.NETOC
                    : TConcepto.NETOV);
            int kg = gta.getKg();
            BigDecimal price = gta.getPrice();

            tabla.addCell(cellText(sdf.format(grainTicket.getEmissionDate())));
            tabla.addCell(cellText(grainTicket.getType().toString()));
            tabla.addCell(cellText(grainTicket.getNro()));
            tabla.addCell(cellText(gta.getGrainType().toString()));
            tabla.addCell(cellNumber(Integer.toString(grainTicket.getType().getId() == 96 ? kg : kg * -1)));
            tabla.addCell(cellNumber(price.toString()));
            tabla.addCell(cellText(grainTicket.getEnterprise().getName()));
            tabla.addCell(cellNumber(price.multiply(new BigDecimal(kg)).toString()));
        }

        tabla.setComplete(true);
        return tabla;
    }

    /**
     *
     */
    static class PageNumber extends PdfPageEventHelper {

        /**
         *
         * @param writer
         * @param document
         */
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            Rectangle r = writer.getBoxSize("art");
            Paragraph page = new Paragraph("Pagina " + writer.getPageNumber());
            page.setFont(new Font(Font.FontFamily.HELVETICA, 10));
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, page, r.getRight(), r.getTop(), 0);
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        Session.client = (Client) ClientController.getAll().get(0);
        String fullPath = "C:/Users/Usuario/Desktop/grainStock.pdf";
        new reports.GrainStock().createPdf(fullPath);
        try {
            File path = new File(fullPath);
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
