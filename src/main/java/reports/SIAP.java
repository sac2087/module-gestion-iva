/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import controller.ClientController;
import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import model.Account;
import model.Client;
import model.FiscCondition;
import model.IdType;
import model.Session;
import static model.TConcepto.BRUTO;
import static model.TConcepto.IVA;
import static model.TConcepto.NETO;
import static model.TConcepto.NO_GRAVADO;
import model.TOperacion;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class SIAP {

    /**
     *
     * @param file
     * @param top
     */
    public SIAP(String file, TOperacion top) {

        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(file);
            pw = new PrintWriter(fichero);

            ArrayList<Ticket> tickets = new ArrayList<Ticket>();
            for (Account a : Session.client.getAccountPlan().getAccounts()) {

                for (TicketAccount ta : a.getTicket_Accounts()) {
                    Ticket t = ta.getTicket();
                    if (t.gettOp() == top) {
                        tickets.add(t);
                    }
                }
            }

            HashSet<Ticket> hsTickets = new HashSet<Ticket>();
            hsTickets.addAll(tickets);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            for (Ticket ticket : hsTickets) {
                String fila = "";

                fila += "1";
                fila += (sdf.format(ticket.getEmissionDate()));

                fila += (String.format("%02d", (ticket.getType().getId())));
                fila += " ";

//                Nro Comprobante
                fila += (String.valueOf(ticket.getSucursal()));
                fila += "000000000000";
                fila += (String.valueOf(ticket.getNumber()));

//                Numero hasta
                fila += "000000000000";
                fila += (String.valueOf(ticket.getNumber()));

                fila += IdType.valueOf(ticket.getEnterprise().getTipoDoc());

                fila += (ticket.getEnterprise().getCuit());

                fila += String.format("%1$-30s", ticket.getEnterprise().getName());

                BigDecimal bruto = BigDecimal.ZERO;
                BigDecimal neto = BigDecimal.ZERO;
                BigDecimal ungrav = BigDecimal.ZERO;
                BigDecimal iva = BigDecimal.ZERO;
                Float ivaPorcent = new Float(0);
                for (TicketAccount ta : ticket.getTicket_Accounts()) {
                    switch (ta.getConcepto()) {
                        case BRUTO:
                            bruto = bruto.add(ta.getPrice());
                            break;
                        case NETO:
                            neto = neto.add(ta.getPrice());
                            break;
                        case IVA:
                            iva = iva.add(ta.getPrice());
                            ivaPorcent = ta.getIvaPercent();
                            break;
                        case NO_GRAVADO:
                            ungrav = ungrav.add(ta.getPrice());
                            break;
                    }
                }

                fila += (String.format("%015d", Integer.parseInt(bruto.toString().replace(".", ""))));
                fila += (String.format("%015d", Integer.parseInt(ungrav.toString().replace(".", ""))));
                fila += (String.format("%015d", Integer.parseInt(neto.toString().replace(".", ""))));
                String auxIvaPercent = String.valueOf(ivaPorcent).replace(".", "");
                System.out.println(auxIvaPercent.length()+" "+auxIvaPercent);
                if (auxIvaPercent.length() == 3) {
                    fila += auxIvaPercent + "0";
                } else {
                    fila += auxIvaPercent;
                }
                fila += (String.format("%015d", Integer.parseInt(iva.toString().replace(".", ""))));
//                fila += "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000   00000000000 000000000000000000000000000000                                                                           00000000000000000000000";

                fila += "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
                fila +=FiscCondition.valueOf(ticket.getEnterprise().getCondFiscal()); //TIPO DE RESPONSABLE CAMBIAR
                fila +="PES";
                fila +="00010000001 000000000000000000000000000000                                                                           00000000000000000000000";
                pw.println(fila);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        Session.client = (Client) ClientController.getAll().get(0);
        String fullPath = "C:\\Users\\Usuario\\Desktop\\siap.txt";
        new reports.SIAP(fullPath, TOperacion.VENTA);
        try {
            File path = new File(fullPath);
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
//        System.out.println(String.format("%1$-30s", "lalala")+"--");
//        NumberFormat formatter = new DecimalFormat("#,#0000000000");
//        System.out.println(formatter.format(new BigDecimal("10.5")));
//        System.out.println((new BigDecimal("10.5").toString().replace(".", "")));
//        System.out.println(String.format("%015d", new BigDecimal("10")));
        System.out.println(String.format("%015d", Integer.parseInt(new BigDecimal("10.50").toString().replace(".", ""))));
    }
}
