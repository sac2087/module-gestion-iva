/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import controller.UserController;
import java.io.FileOutputStream;
import model.User;

/**
 *
 * @author lprone
 */
public class Users {

    /**
     *
     * @param file
     */
    public void createPdf(String file) {
        /*Declaramos documento como un objeto Document
         Asignamos el tama�o de hoja y los margenes */
        Document documento = new Document(PageSize.A4);

        //writer es declarado como el m�todo utilizado para escribir en el archivo
        PdfWriter writer = null;

        try {
            //Obtenemos la instancia del archivo a utilizar
            writer = PdfWriter.getInstance(documento, new FileOutputStream(file));
        } catch (Exception ex) {
            ex.getMessage();
        }

        //Agregamos un titulo al archivo
//    documento.addTitle("Archivo pdf generado desde Java");

        //Agregamos el autor del archivo
//    documento.addAuthor("hug0");

        //Abrimos el documento para edici�n
        documento.open();

        try {
            //Obtenemos la instancia de la imagen            
            Image imagen = Image.getInstance(this.getClass().getResource("/img/logo_pdf.png"));
            //Alineamos la imagen al centro
            imagen.setAlignment(Image.ALIGN_LEFT);
            //Escalamos la imagen al 50%
            imagen.scalePercent(50);
            //Agregamos la imagen al documento
            documento.add(imagen);
        } catch (Exception e) {
            e.getMessage();
        }

        //Declaramos un texto como Paragraph
        //Le podemos dar formado como alineaci�n, tama�o y color a la fuente.
        Paragraph parrafo = new Paragraph();
        parrafo.setAlignment(Paragraph.ALIGN_CENTER);
        parrafo.setFont(FontFactory.getFont("Sans", 20, Font.BOLD, BaseColor.BLUE));
        parrafo.add("Usuarios");

        try {
            //Agregamos el texto al documento
            documento.add(parrafo);

            //Agregamos un salto de linea
            documento.add(new Paragraph(" "));

            //Agregamos la tabla al documento haciendo 
            //la llamada al m�todo tabla()
            documento.add(tabla());
        } catch (DocumentException ex) {
            ex.getMessage();
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }

    //M�todo para crear la tabla
    private static PdfPTable tabla() {
        //Instanciamos una tabla de 3 columnas
        PdfPTable tabla = new PdfPTable(2);

        tabla.setWidthPercentage(100);

        PdfPCell c1 = new PdfPCell(new Phrase("USUARIO"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        c1 = new PdfPCell(new Phrase("ROL"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(c1);

        tabla.setHeaderRows(1);

        for (Object u : UserController.getAllUsers()) {
            tabla.addCell(((User) u).getUserName());
            tabla.addCell(((User) u).getRole() == null ? "Administrador" : "Usuario");
        }

        return tabla;
    }
}
