/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import controller.ClientController;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Client;
import model.Session;
import static model.TConcepto.BRUTO;
import static model.TConcepto.IVA;
import static model.TConcepto.NETO;
import static model.TConcepto.NO_GRAVADO;
import static model.TConcepto.RET_PERCEPCIONES;
import model.TOperacion;
import model.Ticket;
import model.TicketAccount;
/*Librer�as para trabajar con archivos excel*/
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author lprone
 */
public class XLS {

    /**
     *
     * @param file
     * @param top
     */
    public XLS(String file, TOperacion top) {

        /*La ruta donde se crear� el archivo*/
        String rutaArchivo = file;
        /*Se crea el objeto de tipo File con la ruta del archivo*/
        File archivoXLS = new File(rutaArchivo);
        /*Si el archivo existe se elimina*/
        if (archivoXLS.exists()) {
            archivoXLS.delete();
        }
        try {
            /*Se crea el archivo*/
            archivoXLS.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(XLS.class.getName()).log(Level.SEVERE, null, ex);
        }

        /*Se crea el libro de excel usando el objeto de tipo Workbook*/
        Workbook libro = new HSSFWorkbook();
        /*Se inicializa el flujo de datos con el archivo xls*/
        FileOutputStream archivo = null;
        try {
            archivo = new FileOutputStream(archivoXLS);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XLS.class.getName()).log(Level.SEVERE, null, ex);
        }

        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
        Sheet hoja = libro.createSheet("Hoja 1");


        Row encabezado = hoja.createRow(0);
        Cell col1 = encabezado.createCell(0);
        col1.setCellValue("FECHA");

        Cell col2 = encabezado.createCell(1);
        col2.setCellValue("TIPO CPTE");

        Cell col3 = encabezado.createCell(2);
        col3.setCellValue("N� CPTE");

        Cell col4 = encabezado.createCell(3);
        col4.setCellValue(top == TOperacion.COMPRA ? "PROV" : "CLIENTE");

        Cell col5 = encabezado.createCell(4);
        col5.setCellValue("CUIT");

        Cell col6 = encabezado.createCell(5);
        col6.setCellValue("NETO");

        Cell col7 = encabezado.createCell(6);
        col7.setCellValue("NO GRAB\nEXCENT");

        Cell col8 = encabezado.createCell(7);
        col8.setCellValue("IVA");

        Cell col9 = encabezado.createCell(8);
        col9.setCellValue("RET\nPERCEP");

        Cell col10 = encabezado.createCell(9);
        col10.setCellValue("TOTAL");

        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        for (Account a : Session.client.getAccountPlan().getAccounts()) {

            for (TicketAccount ta : a.getTicket_Accounts()) {
                Ticket t = ta.getTicket();
                if (t.gettOp() == top) {
                    tickets.add(t);
                }
            }
        }

        HashSet<Ticket> hsTickets = new HashSet<Ticket>();
        hsTickets.addAll(tickets);
        tickets.clear();
        tickets.addAll(hsTickets);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        int fil = 1;

        for (Ticket ticket : tickets) {

            Row fila = hoja.createRow(fil);
            fil++;

            fila.createCell(0).setCellValue(sdf.format(ticket.getEmissionDate()));
            fila.createCell(1).setCellValue(ticket.getType().toString());
            fila.createCell(2).setCellValue(ticket.getNumber());
            fila.createCell(3).setCellValue(ticket.getEnterprise().getName());
            fila.createCell(4).setCellValue(ticket.getEnterprise().getCuit());


            BigDecimal neto = BigDecimal.ZERO;
            BigDecimal ungrav = BigDecimal.ZERO;
            BigDecimal iva = BigDecimal.ZERO;
            BigDecimal bruto = BigDecimal.ZERO;
            BigDecimal ret = BigDecimal.ZERO;

            for (TicketAccount ta : ticket.getTicket_Accounts()) {
                switch (ta.getConcepto()) {
                    case NETO:
                        neto = neto.add(ta.getPrice());
                        break;
                    case IVA:
                        iva = iva.add(ta.getPrice());
                        break;
                    case NO_GRAVADO:
                        ungrav = ungrav.add(ta.getPrice());
                        break;
                    case BRUTO:
                        bruto = bruto.add(ta.getPrice());
                        break;
                    case RET_PERCEPCIONES:
                        ret = ret.add(ta.getPrice());
                        break;
                }
            }

            fila.createCell(5).setCellValue(neto.toString());
            fila.createCell(6).setCellValue(ungrav.toString());
            fila.createCell(7).setCellValue(iva.toString());
            fila.createCell(8).setCellValue(ret.toString());
            fila.createCell(9).setCellValue(bruto.toString());

        }

        try {
            /*Escribimos en el libro*/
            libro.write(archivo);
        } catch (IOException ex) {
            Logger.getLogger(XLS.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            /*Cerramos el flujo de datos*/
            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(XLS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        Session.client = (Client) ClientController.getAll().get(0);
        String fullPath = "C:/Users/Usuario/Desktop/iva.xls";
        new reports.XLS(fullPath, TOperacion.VENTA);
        try {
            File path = new File(fullPath);
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
