package system;

import controller.ClientController;
import controller.EnterpriseController;
import controller.LoginController;
import controller.crypt;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.swing.JOptionPane;
import model.Client;
import model.IdType;
import model.Session;
import view.Principal;

/**
 *
 * @author lprone
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        if (args.length != 0) {
            System.out.println(args.length);

            crypt cr = new crypt();
            String decryptedPass = "";
            try {
                decryptedPass = cr.decrypt(args[1]);
            } catch (Exception e) {
            }

            if (LoginController.validate(args[0], decryptedPass)) {
                new Principal().setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Error login");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Error ");
        }
//          Session.client = (Client) ClientController.getAll().get(0);
//          new Principal().setVisible(true);
    }
}
