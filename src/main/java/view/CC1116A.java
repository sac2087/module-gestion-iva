/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.AccountController;
import controller.EnterpriseController;
import controller.GrainTicketAccountController;
import controller.GrainTicketController;
import controller.TTicketController;
import controller.TicketAccountController;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import model.Account;
import model.Enterprise;
import model.EnterpriseAccount;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.Session;
import model.TConcepto;
import model.TOperacion;
import model.TTicket;
import model.Ticket;
import model.TicketAccount;
import view.tabla.Tabla;
import view.tabla.TablaNetoConStock;

/**
 *
 * @author GDF
 */
public class CC1116A extends javax.swing.JDialog {

    /*TICKET A EDITAR*/
    private GrainTicket editGrainTicket;

    /* DATOS GRAIN TICKET*/
    Enterprise empresa;
    String numeroComprobante;
    java.sql.Date fechaEmision;
    java.sql.Date fechaContabilizacion;
    java.sql.Date fechaImpFiscal;
    TTicket tipoComprobante = TTicketController.findForName("1116 A");

    /*DATOS GRAIN TICKET ACCOUNT*/
    ArrayList<GrainTicketAccount> netoTotalList = new ArrayList();
    ArrayList<GrainTicketAccount> noGravOpExList = new ArrayList();
    ArrayList<GrainTicketAccount> retencPercepList = new ArrayList();
    GrainTicketAccount bruto = new GrainTicketAccount();


    /*OTRAS VARIABLES UTILIZADAS PARA LA CARGA/EDICION DE GRAIN TICKET*/
    private Color fieldFondoError = new Color(16099753);
    boolean editComprobante;
    private String error1 = "Primero debe seleccionar un Cliente";

    /**
     * Creates new form CargarComprobanteAgropecuario
     *
     * @param parent
     * @param modal
     */
    public CC1116A(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
        clienteNombre.setText(Session.client.getName());
        clienteCUIT.setText(Session.client.getCuit());
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     *
     * @param parent
     * @param modal
     * @param editTicket
     */
    public CC1116A(java.awt.Dialog parent, boolean modal, GrainTicket editTicket) {
        super(parent, modal);
        initComponents();

        clienteNombre.setText(Session.client.getName());
        clienteCUIT.setText(Session.client.getCuit());
        editInit(editTicket);
        editComprobante = true;

        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void editInit(GrainTicket gticket) {
        editGrainTicket = gticket;

        empresa = editGrainTicket.getEnterprise();
        empresaNombre.setText(empresa.getName());
        empresaNombre.setEditable(false);
        empresaCUIT.setText(empresa.getCuit());
        empresaCUIT.setEditable(false);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        fechaEmision = editGrainTicket.getEmissionDate();
        fechaContabilizacion = editGrainTicket.getContDate();
        fechaImpFiscal = editGrainTicket.getImpFDate();

        fieldFechaEmision.setText(sdf.format(editGrainTicket.getEmissionDate()));
        fieldFechaContabilizacion.setText(sdf.format(editGrainTicket.getContDate()));
        fieldFechaImpFiscal.setText(sdf.format(editGrainTicket.getImpFDate()));
        nroSuc.setText(editGrainTicket.getNro().substring(0, 4));
        nroNro.setText(editGrainTicket.getNro().substring(4, 12));
        nroLote.setText(editGrainTicket.getNro().substring(12, 16));

        editInitGrainTicketAccount(gticket);
    }

    private void editInitGrainTicketAccount(GrainTicket editTicket) {
        List<GrainTicketAccount> ticket_Accounts = editTicket.getTicket_Accounts();
        for (GrainTicketAccount ticketAccount : ticket_Accounts) {
            switch (ticketAccount.getConcept()) {
                case NETO:
                    netoTotalList.add(ticketAccount);
                    break;
                case IVA:
                    netoTotalList.add(ticketAccount);
                    break;
                case NO_GRAVADO:
                    noGravOpExList.add(ticketAccount);
                    break;
                case RET_PERCEPCIONES:
                    retencPercepList.add(ticketAccount);
                case BRUTO:
                    bruto = ticketAccount;
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        clienteNombre = new javax.swing.JTextField();
        clienteCUIT = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        fieldFechaEmision = new javax.swing.JFormattedTextField(df);
        fieldFechaContabilizacion = new javax.swing.JFormattedTextField(df);
        fieldFechaImpFiscal = new javax.swing.JFormattedTextField(df);
        jPanelTipoComprobante = new javax.swing.JPanel();
        nroSuc = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nroNro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        nroLote = new javax.swing.JTextField();
        empresaNombre = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        empresaCUIT = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnNetoTotalV = new javax.swing.JButton();
        btnNoGravOpExV = new javax.swing.JButton();
        btnRetencPercepV = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Empresa"));

        jLabel6.setText("C.U.I.T");

        jLabel7.setText("Nombre:");

        clienteNombre.setEditable(false);

        clienteCUIT.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(clienteCUIT)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clienteCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Tipo:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("1116A");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Fecha"));

        jLabel5.setText("Emision:");

        jLabel25.setText("Contabilizacion");

        jLabel8.setText("Imp. Fiscal:");

        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaEmision);
        } catch (ParseException ex) {

        }
        fieldFechaEmision.setColumns(8);
        fieldFechaEmision.setToolTipText("");
        fieldFechaEmision.setDragEnabled(true);
        fieldFechaEmision.setNextFocusableComponent(fieldFechaContabilizacion);
        fieldFechaEmision.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaEmisionFocusLost(evt);
            }
        });

        fieldFechaContabilizacion.setNextFocusableComponent(fieldFechaImpFiscal);
        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaContabilizacion);
        } catch (ParseException ex) {

        }
        fieldFechaContabilizacion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaContabilizacionFocusLost(evt);
            }
        });

        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaImpFiscal);
        } catch (ParseException ex) {

        }
        fieldFechaImpFiscal.setNextFocusableComponent(empresaNombre);
        fieldFechaImpFiscal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaImpFiscalFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel25)
                        .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelTipoComprobante.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        nroSuc.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') ||
                    (c == KeyEvent.VK_BACK_SPACE) ||
                    (c == KeyEvent.VK_DELETE))||(nroSuc.getText().length()>=4)) {
                getToolkit().beep();
                e.consume();
            }
        }
    });
    nroSuc.setNextFocusableComponent(nroNro);
    nroSuc.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroSucFocusLost(evt);
        }
    });

    jLabel2.setText("Sucursal:");

    jLabel3.setText("Nro:");

    nroNro.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroNro.getText().length()>=8)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroNro.setNextFocusableComponent(nroLote);
    nroNro.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroNroFocusLost(evt);
        }
    });

    jLabel4.setText("Lote:");

    nroLote.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroLote.getText().length()>=4)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroLote.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroLoteFocusLost(evt);
        }
    });

    empresaNombre.setToolTipText("Presione F2 para Buscar");
    empresaNombre.setName(""); // NOI18N
    empresaNombre.setNextFocusableComponent(empresaCUIT);
    empresaNombre.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaNombreKeyReleased(evt);
        }
    });

    jLabel13.setText("C.U.I.T");

    empresaCUIT.setNextFocusableComponent(nroSuc);
    empresaCUIT.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaCUITKeyReleased(evt);
        }
    });

    jLabel17.setText("Empresa:");

    javax.swing.GroupLayout jPanelTipoComprobanteLayout = new javax.swing.GroupLayout(jPanelTipoComprobante);
    jPanelTipoComprobante.setLayout(jPanelTipoComprobanteLayout);
    jPanelTipoComprobanteLayout.setHorizontalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel17)
                    .addGap(44, 44, 44)
                    .addComponent(empresaNombre)
                    .addGap(16, 16, 16)
                    .addComponent(jLabel13)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(19, 19, 19))
    );
    jPanelTipoComprobanteLayout.setVerticalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addGap(7, 7, 7)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(empresaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel13)
                .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel17))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(jLabel3)
                .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel4)
                .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(22, Short.MAX_VALUE))
    );

    btnCargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cargard.png"))); // NOI18N
    btnCargar.setBorderPainted(false);
    btnCargar.setContentAreaFilled(false);
    btnCargar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnCargarActionPerformed(evt);
        }
    });

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("+"));

    btnNetoTotalV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/neto-totald.png"))); // NOI18N
    btnNetoTotalV.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
    btnNetoTotalV.setBorderPainted(false);
    btnNetoTotalV.setContentAreaFilled(false);
    btnNetoTotalV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNetoTotalVActionPerformed(evt);
        }
    });

    btnNoGravOpExV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nograv-opexentad.png"))); // NOI18N
    btnNoGravOpExV.setBorderPainted(false);
    btnNoGravOpExV.setContentAreaFilled(false);
    btnNoGravOpExV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNoGravOpExVActionPerformed(evt);
        }
    });

    btnRetencPercepV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retenciones-persepcionesd.png"))); // NOI18N
    btnRetencPercepV.setBorderPainted(false);
    btnRetencPercepV.setContentAreaFilled(false);
    btnRetencPercepV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRetencPercepVActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(btnNetoTotalV)
            .addGap(30, 30, 30)
            .addComponent(btnNoGravOpExV, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(btnRetencPercepV, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(btnRetencPercepV, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnNoGravOpExV, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnNetoTotalV))
            .addGap(17, 17, 17))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(2, 2, 2))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel1)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel9)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(37, 37, 37))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1)
                .addComponent(jLabel9))
            .addGap(18, 18, 18)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(btnCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fieldFechaEmisionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaEmisionFocusLost
        try {
            fieldFechaEmision.commitEdit();
            fechaEmision = new java.sql.Date(((java.util.Date) fieldFechaEmision.getValue()).getTime());
            System.out.println(fechaEmision.toString());
            fieldFechaEmision.setBackground(Color.white);
            fieldFechaContabilizacion.setValue(fieldFechaEmision.getValue());
            fieldFechaImpFiscal.setValue(fieldFechaEmision.getValue());
        } catch (ParseException ex) {
            fieldFechaEmision.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaEmisionFocusLost

    private void fieldFechaContabilizacionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaContabilizacionFocusLost
        try {
            fieldFechaContabilizacion.commitEdit();
            fechaContabilizacion = new java.sql.Date(((java.util.Date) fieldFechaContabilizacion.getValue()).getTime());
            fieldFechaContabilizacion.setBackground(Color.white);
            fieldFechaImpFiscal.setValue(fieldFechaContabilizacion.getValue());
        } catch (ParseException ex) {
            fieldFechaContabilizacion.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaContabilizacionFocusLost

    private void nroSucFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroSucFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroSuc.getText()));
        } catch (Exception e) {
        }
        nroSuc.setText(fmt.toString());
    }//GEN-LAST:event_nroSucFocusLost

    private void nroNroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroNroFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%08d", Integer.parseInt(nroNro.getText()));
        } catch (Exception e) {
        }
        nroNro.setText(fmt.toString());
    }//GEN-LAST:event_nroNroFocusLost

    private void nroLoteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroLoteFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroLote.getText()));
        } catch (Exception e) {
        }
        nroLote.setText(fmt.toString());
    }//GEN-LAST:event_nroLoteFocusLost

    private void empresaNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaNombreKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            List lista;
            if ((empresaNombre.getText().length()) == 0) {
                lista = EnterpriseController.getClientes();
            } else {
                lista = EnterpriseController.getLikeType(empresaNombre.getText(), 1);
            }
            try {
                empresa = (Enterprise) sugerenciaList("Cliente", lista.toArray());
                empresaNombre.setText(empresa.getName());
                empresaCUIT.setText(empresa.getCuit());
                empresaCUIT.setEditable(false);
            } catch (Exception e) {
                System.out.println("No se selecciono ningun cliente o proveedor.");
            }

        }
    }//GEN-LAST:event_empresaNombreKeyReleased

    private void empresaCUITKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaCUITKeyReleased
    }//GEN-LAST:event_empresaCUITKeyReleased
    private Account obtenerCuentaBruto() {
        List<EnterpriseAccount> enterpriseAccount = empresa.getEnterpriseAccount();
        for (EnterpriseAccount enterpriseAccount1 : enterpriseAccount) {
            Account aux = enterpriseAccount1.getAccount();
            if (AccountController.isProvider(aux)) {
                return aux;
            }
        }
        return null;
    }

    private void cargarComprobante() {
        numeroComprobante = nroSuc.getText() + nroNro.getText() + nroLote.getText();

        bruto = new GrainTicketAccount(calculaTotal(), 0, 0 , null, obtenerCuentaBruto(), null, TConcepto.BRUTOC);
        boolean resultadoCarga = GrainTicketController.insert(Session.client, empresa, numeroComprobante, fechaEmision, fechaContabilizacion, fechaImpFiscal, tipoComprobante, netoTotalList, noGravOpExList, retencPercepList, bruto);
        if (resultadoCarga) {
            JOptionPane.showMessageDialog(null, "Comprobante cargado con exito");
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Error al intentar cargar comprobante");
        }
    }

    private void cargarCambios() {
        numeroComprobante = nroSuc.getText() + nroNro.getText() + nroLote.getText();
        editGrainTicket.setNro(numeroComprobante);
        editGrainTicket.setEmissionDate(fechaEmision);
        editGrainTicket.setContDate(fechaContabilizacion);
        editGrainTicket.setImpFDate(fechaImpFiscal);
        //Edito o elimino los ticket account que ya existian en el ticker
        boolean editar;
        for (GrainTicketAccount grainTicketAccount : editGrainTicket.getTicket_Accounts()) {
            editar = false;
            int i = 0;
            switch (grainTicketAccount.getConcept()) {
                case NETO:
                    i = 0;
                    while (i < netoTotalList.size()) {
                        if (netoTotalList.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalList.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalList.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case IVA:
                    i = 0;
                    while (i < netoTotalList.size()) {
                        if (netoTotalList.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalList.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalList.get(i).getPrice());
                            grainTicketAccount.setIvaPervent(netoTotalList.get(i).getIvaPervent());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case NO_GRAVADO:
                    i = 0;
                    while (i < noGravOpExList.size()) {
                        if (noGravOpExList.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(noGravOpExList.get(i).getAccount());
                            grainTicketAccount.setPrice(noGravOpExList.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case RET_PERCEPCIONES:
                    i = 0;
                    while (i < retencPercepList.size()) {
                        if (retencPercepList.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(retencPercepList.get(i).getAccount());
                            grainTicketAccount.setPrice(retencPercepList.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case BRUTO:
                    grainTicketAccount.setPrice(calculaTotal());
            }
            if (editar) {
                GrainTicketAccountController.update(grainTicketAccount);
            } else {
                GrainTicketAccountController.delete(grainTicketAccount);
            }
        }
//agrego nuevos ticker account, en el caso de que se hallan creado
        for (GrainTicketAccount ticketAccount : netoTotalList) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : noGravOpExList) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : retencPercepList) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }

        boolean resultadoEdit = GrainTicketController.update(editGrainTicket);
        if (resultadoEdit) {
            JOptionPane.showMessageDialog(null, "Comprobante editado con exito");
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Error al intentar editar el comprobante");
        }
    }

    /* private void cargarTicketAccount(GrainTicket ticket) {
     for (int i = 0; i < netoTotalList.size(); i++) {
     GrainTicketAccount grainTicketAccount = netoTotalList.get(i);
     grainTicketAccount.setTicket(ticket);
     GrainTicketAccountController.insert(grainTicketAccount);
     }
     for (int i = 0; i < noGravOpExList.size(); i++) {
     GrainTicketAccount grainTicketAccount = noGravOpExList.get(i);
     grainTicketAccount.setTicket(ticket);
     GrainTicketAccountController.insert(grainTicketAccount);
     }
     for (int i = 0; i < retencPercepList.size(); i++) {
     GrainTicketAccount grainTicketAccount = retencPercepList.get(i);
     grainTicketAccount.setTicket(ticket);
     GrainTicketAccountController.insert(grainTicketAccount);
     }

     }
     */
    private void fieldFechaImpFiscalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaImpFiscalFocusLost
        try {
            fieldFechaImpFiscal.commitEdit();
            fechaImpFiscal = new java.sql.Date(((java.util.Date) fieldFechaImpFiscal.getValue()).getTime());
            fieldFechaImpFiscal.setBackground(Color.white);
        } catch (ParseException ex) {
            fieldFechaImpFiscal.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaImpFiscalFocusLost

    /**
     * Retorna el bruno de la operacion
     *
     * @return
     */
    private BigDecimal calculaTotal() {
        BigDecimal result = BigDecimal.ZERO;
        for (GrainTicketAccount ticketAccount : netoTotalList) {
            if (ticketAccount.getConcept()==TConcepto.IVAC){
                result = result.add(ticketAccount.getPrice());
            }else{
                result = result.add(ticketAccount.getPrice().multiply(new BigDecimal(ticketAccount.getKg())));
            }
            
        }
        for (GrainTicketAccount ticketAccount : noGravOpExList) {
            result = result.add(ticketAccount.getPrice());
        }
        for (GrainTicketAccount ticketAccount : retencPercepList) {
            result = result.add(ticketAccount.getPrice());
        }
        return result;
    }

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        if (editComprobante) {
            cargarCambios();
        } else {
            if(GrainTicketController.exists(Session.client, empresa, numeroComprobante, tipoComprobante.getId())){
                JOptionPane.showMessageDialog(null, "El numero de comprobante ya existe en la base de datos.");
            }else{
                cargarComprobante();
            }
        }
    }//GEN-LAST:event_btnCargarActionPerformed

    private void btnRetencPercepVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetencPercepVActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.RET_PERCEPCIONESC);
            tablaAux.asignarTicket(null, retencPercepList);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnRetencPercepVActionPerformed

    private void btnNoGravOpExVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNoGravOpExVActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.NO_GRAVADOC);
            tablaAux.asignarTicket(null, noGravOpExList);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNoGravOpExVActionPerformed

    private void btnNetoTotalVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNetoTotalVActionPerformed

        if (empresa != null) {
            TablaNetoConStock tablaAux = new TablaNetoConStock(this, true, netoTotalList, empresa.getIva(), TConcepto.NETOC, TOperacion.COMPRA);
            tablaAux.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNetoTotalVActionPerformed

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista,
                null);
        return resultado;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnNetoTotalV;
    private javax.swing.JButton btnNoGravOpExV;
    private javax.swing.JButton btnRetencPercepV;
    private javax.swing.JTextField clienteCUIT;
    private javax.swing.JTextField clienteNombre;
    private javax.swing.JTextField empresaCUIT;
    private javax.swing.JTextField empresaNombre;
    private javax.swing.JFormattedTextField fieldFechaContabilizacion;
    private javax.swing.JFormattedTextField fieldFechaEmision;
    private javax.swing.JFormattedTextField fieldFechaImpFiscal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelTipoComprobante;
    private javax.swing.JTextField nroLote;
    private javax.swing.JTextField nroNro;
    private javax.swing.JTextField nroSuc;
    // End of variables declaration//GEN-END:variables
}
