/*
 * To change this license header, choose License Headers in Project Properties.
 * To change t
 @Override
 public Iterator<E> iterator() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }

 @Override
 public int size() {
 throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 }
 his template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.AccountController;
import controller.EnterpriseController;
import controller.GrainTicketAccountController;
import controller.GrainTicketController;
import controller.TicketController;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import model.Account;
import model.Enterprise;
import model.EnterpriseAccount;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.Session;
import model.TConcepto;
import model.TOperacion;
import model.TTicket;
import model.Ticket;
import model.TicketAccount;
import view.tabla.Tabla;
import view.tabla.TablaNetoConStock;

/**
 *
 * @author GDF
 */
public class CC1116BC extends javax.swing.JDialog {

    
    /*TICKET A EDITAR*/
    private GrainTicket editGrainTicket;
    
    /* DATOS GRAIN TICKET*/
    Enterprise empresa;
    String numeroComprobante;
    java.sql.Date fechaEmision;
    java.sql.Date fechaContabilizacion;
    java.sql.Date fechaImpFiscal;
    TTicket tipoComprobante;

    /*DATOS GRAIN TICKET ACCOUNT*/
    ArrayList<GrainTicketAccount> netoTotalListV = new ArrayList();
    ArrayList<GrainTicketAccount> noGravOpExListV = new ArrayList();
    ArrayList<GrainTicketAccount> retencPercepListV = new ArrayList();
    ArrayList<GrainTicketAccount> netoTotalListC = new ArrayList();
    ArrayList<GrainTicketAccount> noGravOpExListC = new ArrayList();
    ArrayList<GrainTicketAccount> retencPercepListC = new ArrayList();
    GrainTicketAccount brutoV =  new GrainTicketAccount();
    GrainTicketAccount brutoC = new GrainTicketAccount();

    /*OTRAS VARIABLES UTILIZADAS PARA LA CARGA/EDICION DE GRAIN TICKET*/
    private Color fieldFondoError = new Color(16099753);
    boolean editComprobante;
    private String error1 = "Primero debe seleccionar un Cliente";
    
    /**
     * Creates new form CC1116BC
     *
     * @param parent
     * @param tipcom
     * @param modal
     */
    public CC1116BC(java.awt.Dialog parent, boolean modal, TTicket tipcom) {
        super(parent, modal);
        initComponents();
        editComprobante=false;
        clienteNombre1.setText(Session.client.getName());
        clienteCUIT1.setText(Session.client.getCuit());
        tipoComptext.setText(tipcom.getName());
        tipoComprobante = tipcom;
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     *
     * @param parent
     * @param modal
     * @param editGrainTicket
     */
    public CC1116BC(java.awt.Dialog parent, boolean modal, GrainTicket editGrainTicket) {
        super(parent, modal);
        initComponents();
        
        clienteNombre1.setText(Session.client.getName());
        clienteCUIT1.setText(Session.client.getCuit()); 
        editInit(editGrainTicket);
        editComprobante=true;
       
        setLocationRelativeTo(null);
        setVisible(true);
    }    

    private Account obtenerCuentaBruto(TOperacion tOp){
        Account resultAcc = null;
        if (empresa != null) {
            if (tOp == TOperacion.VENTA) {
                List<EnterpriseAccount> enterpriseAccount = empresa.getEnterpriseAccount();
                for (EnterpriseAccount enterpriseAccount1 : enterpriseAccount) {
                    Account aux = enterpriseAccount1.getAccount();
                    if (AccountController.isClient(aux)) {
                        resultAcc = aux;
                    }
                }
            } else if (tOp == TOperacion.COMPRA) {
                List<EnterpriseAccount> enterpriseAccount = empresa.getEnterpriseAccount();
                for (EnterpriseAccount enterpriseAccount1 : enterpriseAccount) {
                    Account aux = enterpriseAccount1.getAccount();
                    if (AccountController.isProvider(aux)) {
                        resultAcc = aux;
                    }
                }
            }
        }
        return resultAcc;
    }
    
    private void editInit(GrainTicket gticket){
        editGrainTicket = gticket;
        
        tipoComprobante=editGrainTicket.getType();
        tipoComptext.setText(editGrainTicket.getType().getName());
        
        empresa=editGrainTicket.getEnterprise();
        empresaNombre.setText(empresa.getName());
        empresaNombre.setEditable(false);
        empresaCUIT.setText(empresa.getCuit());
        empresaCUIT.setEditable(false);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        fechaEmision = editGrainTicket.getEmissionDate();
        fechaContabilizacion = editGrainTicket.getContDate();
        fechaImpFiscal = editGrainTicket.getImpFDate();
        
        fieldFechaEmision.setText(sdf.format(editGrainTicket.getEmissionDate()));
        fieldFechaContabilizacion.setText(sdf.format(editGrainTicket.getContDate()));
        fieldFechaImpFiscal.setText(sdf.format(editGrainTicket.getImpFDate()));
        nroSuc.setText(editGrainTicket.getNro().substring(0, 4));
        nroNro.setText(editGrainTicket.getNro().substring(4, 12));
        nroLote.setText(editGrainTicket.getNro().substring(12, 16));      
        
        editInitTicketAccount(gticket);
    }
    
    private void editInitTicketAccount(GrainTicket editTicket) {
        List<GrainTicketAccount> ticket_Accounts = editTicket.getTicket_Accounts();
        for (GrainTicketAccount ticketAccount : ticket_Accounts) {
            switch (ticketAccount.getConcept()) {
                case NETOV:
                    netoTotalListV.add(ticketAccount);
                    break;
                case IVAV:
                    netoTotalListV.add(ticketAccount);
                    break;
                case NO_GRAVADOV:
                    noGravOpExListV.add(ticketAccount);
                    break;
                case RET_PERCEPCIONESV:
                    retencPercepListV.add(ticketAccount);
                case BRUTOV:
                    brutoV = ticketAccount;
                case NETOC:
                    netoTotalListC.add(ticketAccount);
                    break;
                case IVAC:
                    netoTotalListC.add(ticketAccount);
                    break;                    
                case NO_GRAVADOC:
                    noGravOpExListC.add(ticketAccount);
                    break;
                case RET_PERCEPCIONESC:
                    retencPercepListC.add(ticketAccount);
                case BRUTOC:
                    brutoC = ticketAccount;                    
            }
        }
    }    

    
    private BigDecimal calculaTotalC() {
        BigDecimal result = BigDecimal.ZERO;
        for (GrainTicketAccount ticketAccount : netoTotalListC) {
            if (ticketAccount.getConcept()==TConcepto.IVAC){
                result = result.add(ticketAccount.getPrice());
            }else{
                result = result.add(ticketAccount.getPrice().multiply(new BigDecimal(ticketAccount.getKg())));
            }
            
        }
        for (GrainTicketAccount ticketAccount : noGravOpExListC) {
            result = result.add(ticketAccount.getPrice());
        }
        for (GrainTicketAccount ticketAccount : retencPercepListC) {
            result = result.add(ticketAccount.getPrice());
        }
        return result;
    }   

     private BigDecimal calculaTotalV() {
        BigDecimal result = BigDecimal.ZERO;
        for (GrainTicketAccount ticketAccount : netoTotalListV) {
            if (ticketAccount.getConcept()==TConcepto.IVAV){
                result = result.add(ticketAccount.getPrice());
            }else{
                result = result.add(ticketAccount.getPrice().multiply(new BigDecimal(ticketAccount.getKg())));
            }
        }
        for (GrainTicketAccount ticketAccount : noGravOpExListV) {
            result = result.add(ticketAccount.getPrice());
        }
        for (GrainTicketAccount ticketAccount : retencPercepListV) {
            result = result.add(ticketAccount.getPrice());
        }
        return result;
    }      
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        clienteNombre = new javax.swing.JTextField();
        clienteCUIT = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        fieldFechaEmision = new javax.swing.JFormattedTextField(df);
        fieldFechaContabilizacion = new javax.swing.JFormattedTextField(df);
        fieldFechaImpFiscal = new javax.swing.JFormattedTextField(df);
        jPanelTipoComprobante = new javax.swing.JPanel();
        nroSuc = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nroNro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        nroLote = new javax.swing.JTextField();
        empresaNombre = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        empresaCUIT = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        clienteNombre1 = new javax.swing.JTextField();
        clienteCUIT1 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        tipoComptext = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnNetoTotalV = new javax.swing.JButton();
        btnNoGravOpExV = new javax.swing.JButton();
        btnRetencPercepV = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        btnNetoTotalC = new javax.swing.JButton();
        btnNoGravOpExC = new javax.swing.JButton();
        btnRetencPercepC = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Empresa"));

        jLabel6.setText("C.U.I.T");

        jLabel7.setText("Nombre:");

        clienteNombre.setEditable(false);

        clienteCUIT.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(clienteCUIT)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clienteCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Tipo:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("1116A");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Fecha"));

        jLabel5.setText("Emision:");

        jLabel25.setText("Contabilizacion");

        jLabel8.setText("Imp. Fiscal:");

        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaEmision);
        } catch (ParseException ex) {

        }
        fieldFechaEmision.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaEmisionFocusLost(evt);
            }
        });

        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaContabilizacion);
        } catch (ParseException ex) {

        }
        fieldFechaContabilizacion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaContabilizacionFocusLost(evt);
            }
        });

        try {
            MaskFormatter dateMask = new MaskFormatter("##/##/#####");
            dateMask.install(fieldFechaImpFiscal);
        } catch (ParseException ex) {

        }
        fieldFechaImpFiscal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fieldFechaImpFiscalFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel25)
                        .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanelTipoComprobante.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        nroSuc.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') ||
                    (c == KeyEvent.VK_BACK_SPACE) ||
                    (c == KeyEvent.VK_DELETE))||(nroSuc.getText().length()>=4)) {
                getToolkit().beep();
                e.consume();
            }
        }
    });
    nroSuc.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroSucFocusLost(evt);
        }
    });

    jLabel2.setText("Sucursal:");

    jLabel3.setText("Nro:");

    nroNro.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroNro.getText().length()>=8)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroNro.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroNroFocusLost(evt);
        }
    });

    jLabel4.setText("Lote:");

    nroLote.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroLote.getText().length()>=4)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroLote.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroLoteFocusLost(evt);
        }
    });

    empresaNombre.setToolTipText("Presione F2 para Buscar");
    empresaNombre.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaNombreKeyReleased(evt);
        }
    });

    jLabel13.setText("C.U.I.T");

    empresaCUIT.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaCUITKeyReleased(evt);
        }
    });

    jLabel15.setText("Empresa:");

    javax.swing.GroupLayout jPanelTipoComprobanteLayout = new javax.swing.GroupLayout(jPanelTipoComprobante);
    jPanelTipoComprobante.setLayout(jPanelTipoComprobanteLayout);
    jPanelTipoComprobanteLayout.setHorizontalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel15)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(empresaNombre)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel13))
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(80, 80, 80))
    );
    jPanelTipoComprobanteLayout.setVerticalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addGap(7, 7, 7)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(empresaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel13)
                .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel15))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(jLabel3)
                .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel4)
                .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(22, Short.MAX_VALUE))
    );

    jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Empresa"));

    jLabel10.setText("C.U.I.T");

    jLabel11.setText("Nombre:");

    clienteNombre1.setEditable(false);

    clienteCUIT1.setEditable(false);

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel4Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(clienteNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jLabel10)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(clienteCUIT1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel4Layout.setVerticalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel4Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel11)
                .addComponent(jLabel10)
                .addComponent(clienteNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(clienteCUIT1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(19, Short.MAX_VALUE))
    );

    jLabel14.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
    jLabel14.setText("Tipo:");

    tipoComptext.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("+"));

    btnNetoTotalV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/neto-totald.png"))); // NOI18N
    btnNetoTotalV.setBorderPainted(false);
    btnNetoTotalV.setContentAreaFilled(false);
    btnNetoTotalV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNetoTotalVActionPerformed(evt);
        }
    });

    btnNoGravOpExV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nograv-opexentad.png"))); // NOI18N
    btnNoGravOpExV.setBorderPainted(false);
    btnNoGravOpExV.setContentAreaFilled(false);
    btnNoGravOpExV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNoGravOpExVActionPerformed(evt);
        }
    });

    btnRetencPercepV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retenciones-persepcionesd.png"))); // NOI18N
    btnRetencPercepV.setBorderPainted(false);
    btnRetencPercepV.setContentAreaFilled(false);
    btnRetencPercepV.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRetencPercepVActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(btnNetoTotalV, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(btnNoGravOpExV, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(26, 26, 26)
            .addComponent(btnRetencPercepV, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnNoGravOpExV, javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNetoTotalV)))
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(btnRetencPercepV, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
    );

    jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("-"));

    btnNetoTotalC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/neto-totald.png"))); // NOI18N
    btnNetoTotalC.setBorderPainted(false);
    btnNetoTotalC.setContentAreaFilled(false);
    btnNetoTotalC.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNetoTotalCActionPerformed(evt);
        }
    });

    btnNoGravOpExC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nograv-opexentad.png"))); // NOI18N
    btnNoGravOpExC.setBorderPainted(false);
    btnNoGravOpExC.setContentAreaFilled(false);
    btnNoGravOpExC.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnNoGravOpExCActionPerformed(evt);
        }
    });

    btnRetencPercepC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retenciones-persepcionesd.png"))); // NOI18N
    btnRetencPercepC.setBorderPainted(false);
    btnRetencPercepC.setContentAreaFilled(false);
    btnRetencPercepC.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRetencPercepCActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(btnNetoTotalC, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(btnNoGravOpExC, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(26, 26, 26)
            .addComponent(btnRetencPercepC, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel5Layout.setVerticalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnNoGravOpExC)
                .addComponent(btnRetencPercepC)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNetoTotalC)))
            .addGap(0, 0, Short.MAX_VALUE))
    );

    btnCargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cargard.png"))); // NOI18N
    btnCargar.setBorderPainted(false);
    btnCargar.setContentAreaFilled(false);
    btnCargar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnCargarActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jLabel14)
                    .addGap(10, 10, 10)
                    .addComponent(jLabel12)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(tipoComptext)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel14)
                .addComponent(tipoComptext)
                .addComponent(jLabel12))
            .addGap(18, 18, 18)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(btnCargar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nroSucFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroSucFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroSuc.getText()));
        } catch (Exception e) {
        }
        nroSuc.setText(fmt.toString());
    }//GEN-LAST:event_nroSucFocusLost

    private void nroNroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroNroFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%08d", Integer.parseInt(nroNro.getText()));
        } catch (Exception e) {
        }
        nroNro.setText(fmt.toString());
    }//GEN-LAST:event_nroNroFocusLost

    private void nroLoteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroLoteFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroLote.getText()));
        } catch (Exception e) {
        }
        nroLote.setText(fmt.toString());
    }//GEN-LAST:event_nroLoteFocusLost

    private void empresaNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaNombreKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            List lista;
            if ((empresaNombre.getText().length()) == 0) {
                lista = EnterpriseController.getClientes();
            } else {
                lista = EnterpriseController.getLikeType(empresaNombre.getText(), 1);
            }
            try {
                empresa = (Enterprise) sugerenciaList("Cliente", lista.toArray());
                empresaNombre.setText(empresa.getName());
                empresaCUIT.setText(empresa.getCuit());
                empresaCUIT.setEditable(false);
            } catch (Exception e) {
                System.out.println("No se selecciono ningun cliente o proveedor.");
            }

        }
    }//GEN-LAST:event_empresaNombreKeyReleased

    private void empresaCUITKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaCUITKeyReleased
    }//GEN-LAST:event_empresaCUITKeyReleased

    private void btnNetoTotalVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNetoTotalVActionPerformed
        if (empresa != null) {
            TablaNetoConStock tablaAux = new TablaNetoConStock(this, true, netoTotalListV, empresa.getIva(), TConcepto.NETOV, TOperacion.VENTA);
            tablaAux.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNetoTotalVActionPerformed

    private void fieldFechaEmisionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaEmisionFocusLost
        try {
            fieldFechaEmision.commitEdit();
            fechaEmision = new java.sql.Date(((java.util.Date) fieldFechaEmision.getValue()).getTime());
            System.out.println(fechaEmision.toString());
            fieldFechaEmision.setBackground(Color.white);
            fieldFechaContabilizacion.setValue(fieldFechaEmision.getValue());
            fieldFechaImpFiscal.setValue(fieldFechaEmision.getValue());
        } catch (ParseException ex) {
            fieldFechaEmision.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaEmisionFocusLost

    private void fieldFechaContabilizacionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaContabilizacionFocusLost
        try {
            fieldFechaContabilizacion.commitEdit();
            fechaContabilizacion = new java.sql.Date(((java.util.Date) fieldFechaContabilizacion.getValue()).getTime());
            fieldFechaContabilizacion.setBackground(Color.white);
            fieldFechaImpFiscal.setValue(fieldFechaContabilizacion.getValue());
        } catch (ParseException ex) {
            fieldFechaContabilizacion.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaContabilizacionFocusLost

    private void fieldFechaImpFiscalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaImpFiscalFocusLost
        try {
            fieldFechaImpFiscal.commitEdit();
            fechaImpFiscal = new java.sql.Date(((java.util.Date) fieldFechaImpFiscal.getValue()).getTime());
            fieldFechaImpFiscal.setBackground(Color.white);
        } catch (ParseException ex) {
            fieldFechaImpFiscal.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaImpFiscalFocusLost

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        //setIvaGrainTicketAccount();
        if (editComprobante) {
            cargarCambios();
        } else {
            if(GrainTicketController.exists(Session.client, empresa, numeroComprobante, tipoComprobante.getId())){
                JOptionPane.showMessageDialog(null, "El numero de comprobante ya existe en la base de datos.");
            }else{
                cargarComprobante();
            }
            
        }
        
    }//GEN-LAST:event_btnCargarActionPerformed

    private void cargarComprobante() {
        numeroComprobante = nroSuc.getText() + nroNro.getText() + nroLote.getText();
        brutoC = new GrainTicketAccount(calculaTotalC(), 0, 0 , null, obtenerCuentaBruto(TOperacion.COMPRA), null, TConcepto.BRUTOC);
        brutoV = new GrainTicketAccount(calculaTotalV(), 0, 0 , null, obtenerCuentaBruto(TOperacion.VENTA), null, TConcepto.BRUTOV);
        boolean resultadoCarga = GrainTicketController.insert(Session.client, empresa, numeroComprobante, fechaEmision, fechaContabilizacion, fechaImpFiscal, tipoComprobante, netoTotalListV, noGravOpExListV, retencPercepListV, netoTotalListC, noGravOpExListC, retencPercepListC, brutoC,brutoV);
        if (resultadoCarga) {
            JOptionPane.showMessageDialog(null, "Comprobante cargado con exito");
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Error al intentar cargar comprobante");
        }
    }
    
    private void cargarCambios() {
        numeroComprobante = nroSuc.getText() + nroNro.getText() + nroLote.getText();
        editGrainTicket.setNro(numeroComprobante);
        editGrainTicket.setEmissionDate(fechaEmision);        
        editGrainTicket.setContDate(fechaContabilizacion);
        editGrainTicket.setImpFDate(fechaImpFiscal);
       
//Edito o elimino los ticket account que ya existian en el ticker
        boolean editar;
        for (GrainTicketAccount grainTicketAccount : editGrainTicket.getTicket_Accounts()) {
            editar = false;
            int i = 0;
            switch (grainTicketAccount.getConcept()) {
                case NETOV:
                    i = 0;
                    while (i < netoTotalListV.size()) {
                        if (netoTotalListV.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalListV.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalListV.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case IVAV:
                    i = 0;
                    while (i < netoTotalListV.size()) {
                        if (netoTotalListV.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalListV.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalListV.get(i).getPrice());
                            grainTicketAccount.setIvaPervent(netoTotalListV.get(i).getIvaPervent());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case NO_GRAVADOV:
                    i = 0;
                    while (i < noGravOpExListV.size()) {
                        if (noGravOpExListV.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(noGravOpExListV.get(i).getAccount());
                            grainTicketAccount.setPrice(noGravOpExListV.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case RET_PERCEPCIONESV:
                    i = 0;
                    while (i < retencPercepListV.size()) {
                        if (retencPercepListV.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(retencPercepListV.get(i).getAccount());
                            grainTicketAccount.setPrice(retencPercepListV.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case NETOC:
                    i = 0;
                    while (i < netoTotalListC.size()) {
                        if (netoTotalListC.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalListC.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalListC.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case IVAC:
                    i = 0;
                    while (i < netoTotalListC.size()) {
                        if (netoTotalListC.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(netoTotalListC.get(i).getAccount());
                            grainTicketAccount.setPrice(netoTotalListC.get(i).getPrice());
                            grainTicketAccount.setIvaPervent(netoTotalListC.get(i).getIvaPervent());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case NO_GRAVADOC:
                    i = 0;
                    while (i < noGravOpExListC.size()) {
                        if (noGravOpExListC.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(noGravOpExListC.get(i).getAccount());
                            grainTicketAccount.setPrice(noGravOpExListC.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case RET_PERCEPCIONESC:
                    i = 0;
                    while (i < retencPercepListC.size()) {
                        if (retencPercepListC.get(i).getId() == grainTicketAccount.getId()) {
                            grainTicketAccount.setAccount(retencPercepListC.get(i).getAccount());
                            grainTicketAccount.setPrice(retencPercepListC.get(i).getPrice());
                            editar = true;
                        }
                        i++;
                    }
                    break;
                case BRUTOV:
                    grainTicketAccount.setPrice(calculaTotalV());
                    editar=true;
                    break;
                case BRUTOC:
                    grainTicketAccount.setPrice(calculaTotalC());
                    editar=true;
                break;
            }            
            if (editar) {
                GrainTicketAccountController.update(grainTicketAccount);
            } else {
                GrainTicketAccountController.delete(grainTicketAccount);
            }            
        }
//agrego nuevos ticker account, en el caso de que se hallan creado
        for (GrainTicketAccount ticketAccount : netoTotalListV) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : noGravOpExListV) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : retencPercepListV) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }     
         for (GrainTicketAccount ticketAccount : netoTotalListC) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : noGravOpExListC) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }
        for (GrainTicketAccount ticketAccount : retencPercepListC) {
            if (ticketAccount.getId() == 0) {
                ticketAccount.setTicket(editGrainTicket);
                GrainTicketAccountController.insert(ticketAccount);
            }
        }       
        
        boolean resultadoEdit = GrainTicketController.update(editGrainTicket);
        if (resultadoEdit){
            JOptionPane.showMessageDialog(null, "Comprobante editado con exito");
            dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Error al intentar editar el comprobante");
        }
    }    
    

//    private void setIvaGrainTicketAccount() {
//        
//        for (int i = 0; i < netoTotalListV.size(); i++) {
//            GrainTicketAccount grainTicketAccount = netoTotalListV.get(i);
//            if (grainTicketAccount.getConcepto()==TConcepto.IVA){
//                grainTicketAccount.setConcepto(TConcepto.IVAV);
//            }
//        }
//
//        for (int i = 0; i < netoTotalListC.size(); i++) {
//            GrainTicketAccount grainTicketAccount = netoTotalListC.get(i);
//            if (grainTicketAccount.getConcepto()==TConcepto.IVA){
//                grainTicketAccount.setConcepto(TConcepto.IVAC);
//            }
//        }
//
//    }

    private void btnNetoTotalCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNetoTotalCActionPerformed
        if (empresa != null) {
            TablaNetoConStock tablaAux = new TablaNetoConStock(this, true, netoTotalListC, TOP_ALIGNMENT, TConcepto.NETOC, TOperacion.COMPRA);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNetoTotalCActionPerformed

    private void btnNoGravOpExVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNoGravOpExVActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.NO_GRAVADOV);
            tablaAux.asignarTicket(null, noGravOpExListV);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNoGravOpExVActionPerformed

    private void btnRetencPercepVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetencPercepVActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.RET_PERCEPCIONESV);
            tablaAux.asignarTicket(null, retencPercepListV);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnRetencPercepVActionPerformed

    private void btnNoGravOpExCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNoGravOpExCActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.NO_GRAVADOC);
            tablaAux.asignarTicket(null, noGravOpExListC);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnNoGravOpExCActionPerformed

    private void btnRetencPercepCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetencPercepCActionPerformed
        if (empresa != null) {
            Tabla tablaAux = new Tabla(this, true, false, TConcepto.RET_PERCEPCIONESC);
            tablaAux.asignarTicket(null, retencPercepListC);
            tablaAux.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null, error1);
        }
    }//GEN-LAST:event_btnRetencPercepCActionPerformed

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista,
                null);
        return resultado;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnNetoTotalC;
    private javax.swing.JButton btnNetoTotalV;
    private javax.swing.JButton btnNoGravOpExC;
    private javax.swing.JButton btnNoGravOpExV;
    private javax.swing.JButton btnRetencPercepC;
    private javax.swing.JButton btnRetencPercepV;
    private javax.swing.JTextField clienteCUIT;
    private javax.swing.JTextField clienteCUIT1;
    private javax.swing.JTextField clienteNombre;
    private javax.swing.JTextField clienteNombre1;
    private javax.swing.JTextField empresaCUIT;
    private javax.swing.JTextField empresaNombre;
    private javax.swing.JFormattedTextField fieldFechaContabilizacion;
    private javax.swing.JFormattedTextField fieldFechaEmision;
    private javax.swing.JFormattedTextField fieldFechaImpFiscal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanelTipoComprobante;
    private javax.swing.JTextField nroLote;
    private javax.swing.JTextField nroNro;
    private javax.swing.JTextField nroSuc;
    private javax.swing.JLabel tipoComptext;
    // End of variables declaration//GEN-END:variables
}
