/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.AccountController;
import controller.Cap;
import controller.EnterpriseAccountController;
import controller.EnterpriseController;
import controller.TTicketController;
import controller.TicketAccountController;
import controller.TicketController;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.text.MaskFormatter;
import model.Account;
import model.Enterprise;
import model.EnterpriseAccount;
import model.Session;
import model.TConcepto;
import model.TOperacion;
import model.TTicket;
import model.Ticket;
import model.TicketAccount;
import view.tabla.Tabla;
import view.tabla.TablaNeto;

/**
 *
 * @author GDF
 */
public class CargaComprobante extends javax.swing.JDialog {


    /*TICKET A EDITAR*/
    private Ticket editTicket;

    /*DATOS TICKET*/
    /**
     *
     */
    public static Enterprise Empresa; // Cliente o Proveedor    
    private TTicket type;
    private TOperacion tOp;
    private java.sql.Date fechaEmision;
    private java.sql.Date fechaContabilizacion;
    private java.sql.Date fechaImpFiscal;
    private List<Account> accounts;
    private String numeroComprobante;

    /*DATOS TICKET ACCOUNT*/
    private ArrayList<TicketAccount> netoTotalList = new ArrayList<TicketAccount>();
    private ArrayList<TicketAccount> noGravOpExeList = new ArrayList<TicketAccount>();
    private ArrayList<TicketAccount> retenPercepList = new ArrayList<TicketAccount>();

    TicketAccount bruto;

    /*OTRAS VARIABLES UTILIZADAS PARA LA CARGA/EDICION DE TICKET*/
    private String empresaEs;
    private String error1 = "Primero debe seleccionar un ";
    private Color fieldFondoError = new Color(16099753);
    private boolean editComprobante;

    /**
     * Creates new form cargaComprobante
     *
     * @param parent
     * @param modal
     * @param tipo
     */
    public CargaComprobante(java.awt.Frame parent, boolean modal, TOperacion tipo) {
        super(parent, modal);
        this.tOp = tipo;

        editComprobante = false;
        if (tOp == TOperacion.COMPRA) {
            empresaEs = "Proveedor";
        } else if (tOp == TOperacion.VENTA) {
            empresaEs = "Cliente";
        }
        error1 += empresaEs;
        setTitle("Carga comprobante " + tOp);
        addEscapeKey();
        initComponents();
        setLocationRelativeTo(null);
        clienteNombre.setText(Session.client.getName());
        clienteCUIT.setText(Session.client.getCuit());
        setVisible(true);
        new Cap("ticket");
    }

    /**
     *
     * @param parent
     * @param modal
     * @param eTicket
     */
    public CargaComprobante(java.awt.Dialog parent, boolean modal, Ticket eTicket) {
        super(parent, modal);
        editComprobante = true;
        System.out.println("ya entro: " + eTicket);
        if (eTicket == null) {
            JOptionPane.showMessageDialog(null, "Hubo un problema al cargar el ticket seleccionado");
            dispose();
        } else {
            tOp = eTicket.gettOp();
            if (tOp == TOperacion.COMPRA) {
                empresaEs = "Proveedor";
            } else if (tOp == TOperacion.VENTA) {
                empresaEs = "Cliente";
            }
            error1 += empresaEs;
            setTitle("Editar comprobante " + tOp);
            initComponents();
            editInit(eTicket);
        }
        addEscapeKey();
        setLocationRelativeTo(null);
        clienteNombre.setText(Session.client.getName());
        clienteCUIT.setText(Session.client.getCuit());
        setVisible(true);
        new Cap("ticket");
    }

    private void editInit(Ticket editTicket) {
        this.editTicket = editTicket;
        setTitle("Editar comprobante " + tOp);
        Empresa = editTicket.getEnterprise();
        empresaNombre.setText(editTicket.getEnterprise().getName());
        empresaNombre.setEditable(false); //No se puede cambiar el cliente
        empresaCUIT.setText(editTicket.getEnterprise().getCuit());
        empresaCUIT.setEditable(false); //No se puede cambiar el cliente
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        fechaEmision = editTicket.getEmissionDate();
        fechaContabilizacion = editTicket.getContDate();
        fechaImpFiscal = editTicket.getImpFDate();
        fieldFechaEmision.setText(sdf.format(editTicket.getEmissionDate()));
        fieldFechaContabilizacion.setText(sdf.format(editTicket.getContDate()));
        fieldFechaImpFiscal.setText(sdf.format(editTicket.getImpFDate()));
        nroSuc.setText(editTicket.getSucursal());
        nroNro.setText(editTicket.getNumber());
        nroLote.setText(editTicket.getLote());
        editInitTicketAccount(editTicket);
    }

    private void editInitTicketAccount(Ticket editTicket) {
        List<TicketAccount> ticket_Accounts = editTicket.getTicket_Accounts();
        for (TicketAccount ticketAccount : ticket_Accounts) {
            switch (ticketAccount.getConcept()) {
                case NETO:
                    netoTotalList.add(ticketAccount);
                    break;
                case IVA:
                    netoTotalList.add(ticketAccount);
                    break;
                case NO_GRAVADO:
                    noGravOpExeList.add(ticketAccount);
                    break;
                case RET_PERCEPCIONES:
                    retenPercepList.add(ticketAccount);
                case BRUTO:
                    bruto = ticketAccount;
            }
        }
    }

    /**
     *
     */
    public void regenerarTable() {
    }

    private void addEscapeKey() {
        // Handle escape key to close the dialog
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        Action escapeAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                String message = "Esta por abandonar la carga de Comprobantes, Confirma que desea salir";
                String title = "Salir Carga de comprobante?";
                // display the JOptionPane showConfirmDialog
                int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    dispose();
                }
            }
        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", escapeAction);
    }

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista,
                null);
        return resultado;
    }

    private void cargarCambios() {
        editTicket.setEmissionDate(fechaEmision);
        editTicket.setContDate(fechaContabilizacion);
        editTicket.setImpFDate(fechaImpFiscal);
        boolean result = TicketAccountController.updateAll(editTicket, netoTotalList, noGravOpExeList, retenPercepList);
        if(result){
            JOptionPane.showMessageDialog(null, "El Comprobante fue editado con exito");
            dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Ocurrio un problema al intentar editarlo, intente nuevamente");
            dispose();
        }

    }
    
    private void calcularBruto(){
        bruto = new TicketAccount(EnterpriseAccountController.getAccountOperation(Empresa, tOp), null, calculaTotal(), 0, TConcepto.BRUTO);
    }

    private void cargarComprobante() {
        calcularBruto();
        boolean resultadoCarga = TicketController.insert((numeroComprobante.trim()), fechaEmision, fechaContabilizacion, fechaImpFiscal, type, Empresa, tOp, netoTotalList, noGravOpExeList, retenPercepList, bruto);
        if (resultadoCarga) {
            JOptionPane.showMessageDialog(null, "El Comprobante fue cargado con exito");
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Ocurrio un error al cargar el comprobante, vefique los datos.");
        }

    }


    /**
     * private void completTableAccount() { int columTotal = 1; int columAlt =
     * 2; if (Empresa != null) { Account account = null; if (tOp ==
     * TOperacion.VENTA) { columTotal = 1; columAlt = 2; } else if (tOp ==
     * TOperacion.COMPRA) { columTotal = 2; columAlt = 1; } Account auxaccount =
     * obtenerCuentaEmpresa(); total = new TicketAccount(auxaccount, null,
     * calculaTotal(), 0, TConcepto.BRUTO); int fila = 0;
     * tableAccount.setValueAt(total.getAccount().getName(), fila, 0);
     * tableAccount.setValueAt(total.getPrice(), fila, columTotal); fila++; for
     * (TicketAccount ticketAccount : netoTotalList) {
     * tableAccount.setValueAt(ticketAccount.getAccount().getName(), fila, 0);
     * tableAccount.setValueAt(ticketAccount.getPrice(), fila, columAlt);
     * fila++; } for (TicketAccount ticketAccount : noGravOpExeList) {
     * tableAccount.setValueAt(ticketAccount.getAccount().getName(), fila, 0);
     * tableAccount.setValueAt(ticketAccount.getPrice(), fila, columAlt);
     * fila++; } for (TicketAccount ticketAccount : retenPercepList) {
     * tableAccount.setValueAt(ticketAccount.getAccount().getName(), fila, 0);
     * tableAccount.setValueAt(ticketAccount.getPrice(), fila, columAlt);
     * fila++; } } }
     */
    /**
     * Retorna el bruno de la operacion
     *
     * @return
     */
    private BigDecimal calculaTotal() {
        BigDecimal result = BigDecimal.ZERO;
        for (TicketAccount ticketAccount : netoTotalList) {
            result = result.add(ticketAccount.getPrice());
        }
        for (TicketAccount ticketAccount : noGravOpExeList) {
            result = result.add(ticketAccount.getPrice());
        }
        for (TicketAccount ticketAccount : retenPercepList) {
            result = result.add(ticketAccount.getPrice());
        }
        return result;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelComprobante = new javax.swing.JPanel();
        jPanelTipoComprobante = new javax.swing.JPanel();
        nroSuc = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nroNro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        nroLote = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        empresaNombre = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        empresaCUIT = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        fieldFechaEmision = new javax.swing.JFormattedTextField(df);
        fieldFechaContabilizacion =  new javax.swing.JFormattedTextField(df);
        fieldFechaImpFiscal =  new javax.swing.JFormattedTextField(df);
        netoTotal = new javax.swing.JButton();
        noGravOpExe = new javax.swing.JButton();
        retenPercep = new javax.swing.JButton();
        btnCargar = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        clienteNombre = new javax.swing.JTextField();
        clienteCUIT = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cargar Comprobante");
        setAutoRequestFocus(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanelComprobante.setBorder(javax.swing.BorderFactory.createTitledBorder(null,"Comprobante "+tOp.toString(), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12)));

        jPanelTipoComprobante.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        nroSuc.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') ||
                    (c == KeyEvent.VK_BACK_SPACE) ||
                    (c == KeyEvent.VK_DELETE))||(nroSuc.getText().length()>=4)) {
                getToolkit().beep();
                e.consume();
            }
        }
    });
    nroSuc.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroSucFocusLost(evt);
        }
    });

    jLabel2.setText("Sucursal:");

    jLabel3.setText("Nro:");

    nroNro.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroNro.getText().length()>=8)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroNro.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroNroFocusLost(evt);
        }
    });

    jLabel4.setText("Lote:");

    nroLote.addKeyListener(new KeyAdapter() {
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if (!((c >= '0') && (c <= '9') ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))||(nroLote.getText().length()>=4)) {
            getToolkit().beep();
            e.consume();
        }
    }
    });
    nroLote.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            nroLoteFocusLost(evt);
        }
    });

    jLabel12.setText(empresaEs+": ");

    empresaNombre.setToolTipText("Presione F2 para Buscar");
    empresaNombre.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaNombreKeyReleased(evt);
        }
    });

    jLabel13.setText("C.U.I.T");

    empresaCUIT.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            empresaCUITKeyReleased(evt);
        }
    });

    javax.swing.GroupLayout jPanelTipoComprobanteLayout = new javax.swing.GroupLayout(jPanelTipoComprobante);
    jPanelTipoComprobante.setLayout(jPanelTipoComprobanteLayout);
    jPanelTipoComprobanteLayout.setHorizontalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
                    .addComponent(jLabel12)
                    .addGap(18, 18, 18)
                    .addComponent(empresaNombre)
                    .addGap(16, 16, 16)
                    .addComponent(jLabel13)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(19, 19, 19))
    );
    jPanelTipoComprobanteLayout.setVerticalGroup(
        jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelTipoComprobanteLayout.createSequentialGroup()
            .addGap(7, 7, 7)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel12)
                .addComponent(empresaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel13)
                .addComponent(empresaCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanelTipoComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(nroSuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(jLabel3)
                .addComponent(nroNro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel4)
                .addComponent(nroLote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(22, Short.MAX_VALUE))
    );

    jLabel1.setFont(new java.awt.Font("Tw Cen MT", 0, 12)); // NOI18N
    jLabel1.setText("Tipo:");

    jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Fecha"));

    jLabel5.setText("Emision:");

    jLabel9.setText("Contabilizacion");

    jLabel8.setText("Imp. Fiscal:");

    try {
        MaskFormatter dateMask = new MaskFormatter("##/##/#####");
        dateMask.install(fieldFechaEmision);
    } catch (ParseException ex) {

    }
    fieldFechaEmision.setColumns(8);
    fieldFechaEmision.setToolTipText("");
    fieldFechaEmision.setDragEnabled(true);
    fieldFechaEmision.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            fieldFechaEmisionFocusLost(evt);
        }
    });

    try {
        MaskFormatter dateMask = new MaskFormatter("##/##/#####");
        dateMask.install(fieldFechaContabilizacion);
    } catch (ParseException ex) {

    }

    fieldFechaContabilizacion.setColumns(8);
    fieldFechaContabilizacion.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            fieldFechaContabilizacionFocusLost(evt);
        }
    });

    try {
        MaskFormatter dateMask = new MaskFormatter("##/##/#####");
        dateMask.install(fieldFechaImpFiscal);
    } catch (ParseException ex) {
    }
    fieldFechaImpFiscal.setColumns(8);
    fieldFechaImpFiscal.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            fieldFechaImpFiscalFocusLost(evt);
        }
    });

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jLabel5)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(jLabel9)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLabel8)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(45, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(fieldFechaImpFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel9)
                    .addComponent(fieldFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fieldFechaContabilizacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    netoTotal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/neto-totald.png"))); // NOI18N
    netoTotal.setBorderPainted(false);
    netoTotal.setContentAreaFilled(false);
    netoTotal.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            netoTotalActionPerformed(evt);
        }
    });

    noGravOpExe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nograv-opexentad.png"))); // NOI18N
    noGravOpExe.setBorderPainted(false);
    noGravOpExe.setContentAreaFilled(false);
    noGravOpExe.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            noGravOpExeActionPerformed(evt);
        }
    });

    retenPercep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retenciones-persepcionesd.png"))); // NOI18N
    retenPercep.setBorderPainted(false);
    retenPercep.setContentAreaFilled(false);
    retenPercep.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            retenPercepActionPerformed(evt);
        }
    });

    btnCargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cargard.png"))); // NOI18N
    btnCargar.setBorderPainted(false);
    btnCargar.setContentAreaFilled(false);
    btnCargar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    btnCargar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnCargarActionPerformed(evt);
        }
    });

    ArrayList<TTicket> valores = null;
    if(tOp==TOperacion.COMPRA){
        valores = new ArrayList(TTicketController.getCompra());
    }else if (tOp==TOperacion.VENTA){
        valores = new ArrayList(TTicketController.getVenta());
    }
    jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(valores.toArray()));
    type = valores.get(0);
    jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            jComboBox1FocusLost(evt);
        }
    });

    javax.swing.GroupLayout jPanelComprobanteLayout = new javax.swing.GroupLayout(jPanelComprobante);
    jPanelComprobante.setLayout(jPanelComprobanteLayout);
    jPanelComprobanteLayout.setHorizontalGroup(
        jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelComprobanteLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelComprobanteLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelComprobanteLayout.createSequentialGroup()
                    .addComponent(netoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(44, 44, 44)
                    .addComponent(noGravOpExe, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnCargar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(retenPercep, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addContainerGap())
    );
    jPanelComprobanteLayout.setVerticalGroup(
        jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanelComprobanteLayout.createSequentialGroup()
            .addGap(8, 8, 8)
            .addGroup(jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanelTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanelComprobanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(noGravOpExe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(retenPercep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(netoTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(5, 5, 5)
            .addComponent(btnCargar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGap(5, 5, 5))
    );

    jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Empresa"));

    jLabel6.setFont(new java.awt.Font("Tw Cen MT", 0, 12)); // NOI18N
    jLabel6.setText("C.U.I.T");

    jLabel7.setFont(new java.awt.Font("Tw Cen MT", 0, 12)); // NOI18N
    jLabel7.setText("Nombre:");

    clienteNombre.setEditable(false);

    clienteCUIT.setEditable(false);

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel7)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jLabel6)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(clienteCUIT, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
            .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel7)
                .addComponent(jLabel6)
                .addComponent(clienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(clienteCUIT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGap(19, 19, 19)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
        .addComponent(jPanelComprobante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanelComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fieldFechaEmisionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaEmisionFocusLost
        try {
            fieldFechaEmision.commitEdit();
            fechaEmision = new java.sql.Date(((java.util.Date) fieldFechaEmision.getValue()).getTime());
            fieldFechaEmision.setBackground(Color.white);
            fechaContabilizacion = new java.sql.Date(((java.util.Date) fieldFechaEmision.getValue()).getTime());
            fechaImpFiscal = new java.sql.Date(((java.util.Date) fieldFechaEmision.getValue()).getTime());
            fieldFechaContabilizacion.setValue(fieldFechaEmision.getValue());
            fieldFechaImpFiscal.setValue(fieldFechaEmision.getValue());
        } catch (ParseException ex) {
            fieldFechaEmision.setBackground(fieldFondoError);
        }

    }//GEN-LAST:event_fieldFechaEmisionFocusLost

    private void fieldFechaContabilizacionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaContabilizacionFocusLost
        try {
            fieldFechaContabilizacion.commitEdit();
            fechaContabilizacion = new java.sql.Date(((java.util.Date) fieldFechaContabilizacion.getValue()).getTime());
            fieldFechaContabilizacion.setBackground(Color.white);
            fechaImpFiscal = new java.sql.Date(((java.util.Date) fieldFechaContabilizacion.getValue()).getTime());
            fieldFechaImpFiscal.setValue(fieldFechaContabilizacion.getValue());
        } catch (ParseException ex) {
            fieldFechaContabilizacion.setBackground(fieldFondoError);
        }

    }//GEN-LAST:event_fieldFechaContabilizacionFocusLost

    private void empresaNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaNombreKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            List lista;
            if ((empresaNombre.getText().length()) == 0) {
                if (tOp == TOperacion.VENTA) {
                    lista = EnterpriseController.getClientes();
                } else {
                    lista = EnterpriseController.getProvedores();
                }
            } else {
                lista = EnterpriseController.getLikeType(empresaNombre.getText(), (tOp == TOperacion.VENTA) ? 1 : 2);
            }
            try {
                Empresa = (Enterprise) sugerenciaList("Cliente", lista.toArray());
                empresaNombre.setText(Empresa.getName());
                empresaCUIT.setText(Empresa.getCuit());
                empresaCUIT.setEditable(false);
            } catch (Exception e) {
            }

        }

    }//GEN-LAST:event_empresaNombreKeyReleased

    private void empresaCUITKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empresaCUITKeyReleased
    }//GEN-LAST:event_empresaCUITKeyReleased

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        numeroComprobante= nroSuc.getText()+nroNro.getText()+nroLote.getText();
        if (editComprobante) {
            cargarCambios();
        } else {
            if(TicketController.exists(Session.client, Empresa, numeroComprobante, tOp, type.getId())){
                JOptionPane.showMessageDialog(null, "El numero de comprobante ya existe para ese tipo de comprobante.");
            }else{
                cargarComprobante();
            }
        }
    }//GEN-LAST:event_btnCargarActionPerformed

    private void netoTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_netoTotalActionPerformed
        if (Empresa != null) {
            TablaNeto tablaNeto = new TablaNeto(this, true, netoTotalList,tOp,type);
            tablaNeto.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(jPanelComprobante, error1);
        }


    }//GEN-LAST:event_netoTotalActionPerformed

    
    private void noGravOpExeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noGravOpExeActionPerformed
        if (Empresa != null) {
            Tabla tabla = new Tabla(this, true, true, TConcepto.NO_GRAVADO);
            tabla.asignarTicket(noGravOpExeList, null);
            tabla.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(jPanelComprobante, error1);
        }
    }//GEN-LAST:event_noGravOpExeActionPerformed

    private void retenPercepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retenPercepActionPerformed
        if (Empresa != null) {
            Tabla tabla = new Tabla(this, true, true, TConcepto.RET_PERCEPCIONES);
            tabla.asignarTicket(retenPercepList, null);
            tabla.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(jPanelComprobante, error1);
        }
    }//GEN-LAST:event_retenPercepActionPerformed

    private void nroSucFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroSucFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroSuc.getText()));
        } catch (Exception e) {
        }
        nroSuc.setText(fmt.toString());
    }//GEN-LAST:event_nroSucFocusLost

    private void nroNroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroNroFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%08d", Integer.parseInt(nroNro.getText()));
        } catch (Exception e) {
        }
        nroNro.setText(fmt.toString());
    }//GEN-LAST:event_nroNroFocusLost

    private void nroLoteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nroLoteFocusLost
        Formatter fmt = new Formatter();
        try {
            fmt.format("%04d", Integer.parseInt(nroLote.getText()));
        } catch (Exception e) {
        }
        nroLote.setText(fmt.toString());
    }//GEN-LAST:event_nroLoteFocusLost

    private void jComboBox1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusLost
        // TODO add your handling code here:
        type = (TTicket) jComboBox1.getSelectedItem();
    }//GEN-LAST:event_jComboBox1FocusLost

    private void fieldFechaImpFiscalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fieldFechaImpFiscalFocusLost
        try {
            fieldFechaImpFiscal.commitEdit();
            fechaImpFiscal = new java.sql.Date(((java.util.Date) fieldFechaImpFiscal.getValue()).getTime());
            fieldFechaImpFiscal.setBackground(Color.white);
        } catch (ParseException ex) {
            fieldFechaImpFiscal.setBackground(fieldFondoError);
        }
    }//GEN-LAST:event_fieldFechaImpFiscalFocusLost

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargar;
    private javax.swing.JTextField clienteCUIT;
    private javax.swing.JTextField clienteNombre;
    private javax.swing.JTextField empresaCUIT;
    private javax.swing.JTextField empresaNombre;
    private javax.swing.JFormattedTextField fieldFechaContabilizacion;
    private javax.swing.JFormattedTextField fieldFechaEmision;
    private javax.swing.JFormattedTextField fieldFechaImpFiscal;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelComprobante;
    private javax.swing.JPanel jPanelTipoComprobante;
    private javax.swing.JButton netoTotal;
    private javax.swing.JButton noGravOpExe;
    private javax.swing.JTextField nroLote;
    private javax.swing.JTextField nroNro;
    private javax.swing.JTextField nroSuc;
    private javax.swing.JButton retenPercep;
    // End of variables declaration//GEN-END:variables
}
