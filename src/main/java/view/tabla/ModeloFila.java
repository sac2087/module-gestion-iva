/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import model.Account;

/**
 *
 * @author lprone
 */
public class ModeloFila {

    int id;
    Account cuentaOperacion; // colum 0
    BigDecimal precio;// colum 1

    /**
     *
     */
    public ModeloFila() {
        precio = BigDecimal.ZERO;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Account getCuentaOperacion() {
        return cuentaOperacion;
    }

    /**
     *
     * @return
     */
    public BigDecimal getPrecio() {
        return precio;
    }

    /**
     *
     * @param cuentaOperacion
     */
    public void setCuentaOperacion(Account cuentaOperacion) {
        this.cuentaOperacion = cuentaOperacion;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }
}
