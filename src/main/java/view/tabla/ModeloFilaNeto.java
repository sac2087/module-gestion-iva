/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import model.Account;
import model.GrainType;

/**
 *
 * @author GDF
 */
public class ModeloFilaNeto {

    int id;
    int idIva;
    Account cuentaOperacion; // colum 0
    BigDecimal precio;// colum 1
    Float porcIva; // colum 2
    Account cuentaIva;  // colum 3
    BigDecimal iva; // colum 4
    BigDecimal total; // colum 5

    /**
     *
     */
    public ModeloFilaNeto() {
        porcIva = 0f;
        iva = BigDecimal.ZERO;
        precio = new BigDecimal(0).setScale(2);
        total = new BigDecimal(0).setScale(2);
    }

    /**
     *
     * @return
     */
    public Account getCuentaOperacion() {
        return cuentaOperacion;
    }

    /**
     *
     * @return
     */
    public int getIdIva() {
        return idIva;
    }

    /**
     *
     * @param idIva
     */
    public void setIdIva(int idIva) {
        this.idIva = idIva;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Account getCuentaIva() {
        return cuentaIva;
    }

    /**
     *
     * @return
     */
    public Float getPorcIva() {
        return porcIva;
    }

    /**
     *
     * @return
     */
    public BigDecimal getIva() {
        return iva;
    }

    /**
     *
     * @return
     */
    public BigDecimal getPrecio() {
        return precio;
    }

    /**
     *
     * @return
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     *
     * @param cuentaIva
     */
    public void setCuentaIva(Account cuentaIva) {
        this.cuentaIva = cuentaIva;
    }

    /**
     *
     * @param cuentaOperacion
     */
    public void setCuentaOperacion(Account cuentaOperacion) {
        this.cuentaOperacion = cuentaOperacion;
    }

    /**
     *
     * @param iva
     */
    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    /**
     *
     * @param porcIva
     */
    public void setPorcIva(Float porcIva) {
        this.porcIva = porcIva;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    /**
     *
     * @param total
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
