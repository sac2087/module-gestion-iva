/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import java.math.BigInteger;
import model.Account;
import model.GrainType;

/**
 *
 * @author GDF
 */
public class ModeloFilaNetoConStock {
    int idOp;
    int idIva;
    Account cuentaOperacion; // colum 0
    GrainType tipoGrano; // colum 1
    Integer cantidad; // colum 2
    BigDecimal precio;// colum 3
    Float porcIva; // colum 4
    Account cuentaIva;  // colum 5
    BigDecimal subTotal; // colum 6
    BigDecimal iva; // colum 7
    BigDecimal total; // colum 8

    /**
     *
     */
    public ModeloFilaNetoConStock() {
        idOp=0;
        idIva=0;
        cantidad = 0;
        porcIva = 0f;
        iva = BigDecimal.ZERO;
        precio = BigDecimal.ZERO;
        subTotal = BigDecimal.ZERO;
        total = BigDecimal.ZERO;
        cuentaOperacion = null;
    }

    /**
     *
     * @return
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     *
     * @param idOp
     */
    public void setIdOp(int idOp) {
        this.idOp = idOp;
    }


    /**
     *
     * @param idIva
     */
    public void setIdIva(int idIva) {
        this.idIva = idIva;
    }

    /**
     *
     * @return
     */
    public int getIdOp() {
        return idOp;
    }

    /**
     *
     * @return
     */
    public int getIdIva() {
        return idIva;
    }

    
    
    /**
     *
     * @return
     */
    public Account getCuentaIva() {
        return cuentaIva;
    }

    /**
     *
     * @return
     */
    public Account getCuentaOperacion() {
        return cuentaOperacion;
    }

    /**
     *
     * @param cuentaIva
     */
    public void setCuentaIva(Account cuentaIva) {
        this.cuentaIva = cuentaIva;
    }

    /**
     *
     * @param cuentaOperacion
     */
    public void setCuentaOperacion(Account cuentaOperacion) {
        this.cuentaOperacion = cuentaOperacion;
    }

    /**
     *
     * @return
     */
    public Integer getCantidad() {
        return cantidad;
    }

    /**
     *
     * @return
     */
    public BigDecimal getIva() {
        return iva;
    }

    /**
     *
     * @return
     */
    public Float getPorcIva() {
        return porcIva;
    }

    /**
     *
     * @return
     */
    public BigDecimal getPrecio() {
        return precio;
    }

    /**
     *
     * @return
     */
    public BigDecimal getSubTotal() {
        return subTotal;
    }

    /**
     *
     * @return
     */
    public GrainType getTipoGrano() {
        return tipoGrano;
    }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @param iva
     */
    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    /**
     *
     * @param porcIva
     */
    public void setPorcIva(float porcIva) {
        this.porcIva = porcIva;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    /**
     *
     * @param subTotal
     */
    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    /**
     *
     * @param tipoGrano
     */
    public void setTipoGrano(GrainType tipoGrano) {
        this.tipoGrano = tipoGrano;
    }

    /**
     *
     * @param total
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
