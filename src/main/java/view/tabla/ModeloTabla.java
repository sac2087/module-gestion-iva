/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import java.util.LinkedList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import model.Account;

/**
 *
 * @author GDF
 */
public class ModeloTabla implements TableModel {

    private LinkedList<ModeloFila> tabla;
    private LinkedList suscriptores = new LinkedList();

    /**
     *
     */
    public ModeloTabla() {
        tabla = new LinkedList<ModeloFila>();
    }

    public int getRowCount() {
        return tabla.size();
    }

    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Cuenta";
            case 1:
                return "Importe";
            default:
                return null;
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Account.class;
            case 1:
                return BigDecimal.class;
            default:
                return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return true;
            case 1:
                return true;
            default:
                return false;
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        ModeloFila aux = tabla.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return aux.getCuentaOperacion();
            case 1:
                return aux.precio;
            default:
                return true;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ModeloFila aux = tabla.get(rowIndex);
        BigDecimal auxIva;
        switch (columnIndex) {
            case 0:
                //Cambia cuenta de operacion
                aux.setCuentaOperacion((Account) aValue);
                break;
            case 1:
                //Cambia el importte
                aux.setPrecio((BigDecimal) aValue);
                break;
        }
        TableModelEvent evento = new TableModelEvent(this, rowIndex - 1, rowIndex, columnIndex);
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     */
    public void nuevaFila() {
        // A�ade la persona al modelo 
        ModeloFila auxModeloFila = new ModeloFila();
        tabla.add(auxModeloFila);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param mfn
     */
    public void nuevaFila(ModeloFila mfn) {
        tabla.add(mfn);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param fila
     */
    public void borraFila(int fila) {
        // Se borra la fila 
        tabla.remove(fila);
        // Y se avisa a los suscriptores, creando un TableModelEvent... 
        TableModelEvent evento = new TableModelEvent(this, fila, fila,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        // ... y pas�ndoselo a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param nroFila
     * @return
     */
    public ModeloFila getFila(int nroFila) {
        if (nroFila < getRowCount()) {
            return tabla.get(nroFila);
        } else {
            return null;
        }
    }

    public void addTableModelListener(TableModelListener l) {
        suscriptores.add(l);
    }

    public void removeTableModelListener(TableModelListener l) {
        suscriptores.remove(l);
    }
}
