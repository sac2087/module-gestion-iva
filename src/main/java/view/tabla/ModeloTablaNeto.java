/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import model.Account;
import model.GrainType;

/**
 *
 * @author GDF
 */
public class ModeloTablaNeto implements TableModel {

    private LinkedList<ModeloFilaNeto> tabla;
    private LinkedList suscriptores = new LinkedList();
    //Datos
    ModeloFilaNeto modeloFilaDefecto = new ModeloFilaNeto();
    boolean ivaReverse;

    /**
     *
     * @param porcIvaPorDefecto
     * @param cuentaIVA
     * @param ivaReverse  
     */
    public ModeloTablaNeto(float porcIvaPorDefecto, Account cuentaIVA, boolean ivaReverse) {
        tabla = new LinkedList<ModeloFilaNeto>();
        modeloFilaDefecto.setPorcIva(porcIvaPorDefecto);
        modeloFilaDefecto.setCuentaIva(cuentaIVA);
        this.ivaReverse = ivaReverse;
        System.out.println(ivaReverse);
    }

    public int getRowCount() {
        return tabla.size();
    }

    public int getColumnCount() {
        return 6;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Cuenta";
            case 1:
                return "Importe";
            case 2:
                return "%IVA";
            case 3:
                return "Cuenta IVA";
            case 4:
                return "IVA";
            case 5:
                return "Total";
            default:
                return null;
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Account.class;
            case 1:
                return BigDecimal.class;
            case 2:
                return Float.class;
            case 3:
                return Account.class;
            case 4:
                return BigDecimal.class;
            case 5:
                return BigDecimal.class;
            default:
                return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return true;
            case 3:
                return true;
            case 4:
                return false;
            case 5:
                return false;
            default:
                return false;
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        ModeloFilaNeto aux = tabla.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return aux.getCuentaOperacion();
            case 1:
                return aux.precio;
            case 2:
                return aux.getPorcIva();
            case 3:
                return aux.getCuentaIva();
            case 4:
                return aux.iva;
            case 5:
                return aux.getTotal();
            default:
                return true;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ModeloFilaNeto aux = tabla.get(rowIndex);
        BigDecimal auxIva;
        if (!ivaReverse) {
            switch (columnIndex) {
                case 0:
                    //Cambia cuenta de operacion
                    aux.setCuentaOperacion((Account) aValue);
                    break;
                case 1:
                    //Cambia el importte
                    aux.setPrecio((BigDecimal) aValue);
                    //Calcula totales
                    auxIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100));
                    aux.setIva(aux.getPrecio().multiply(auxIva));
                    aux.setTotal(aux.getPrecio().add(aux.getIva()));
                    break;
                case 2:
                    //Cambia el % iva
                    aux.setPorcIva((Float) aValue);
                    //Calcula totales
                    auxIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100));
                    aux.setIva(aux.getPrecio().multiply(auxIva));
                    aux.setTotal(aux.getPrecio().add(aux.getIva()));
                    break;
                case 3:
                    //Cambia la cuenta iva
                    aux.setCuentaIva((Account) aValue);
                    break;
                case 4:
                    aux.setIva((BigDecimal) aValue);
                    break;
                case 5:
                    //Total, no incluye iva
                    aux.setTotal((BigDecimal) aValue);
                    break;
            }
        } else {
            switch (columnIndex) {
                case 0:
                    //Cambia cuenta de operacion
                    aux.setCuentaOperacion((Account) aValue);
                    break;
                case 1:
                    //Cambia el importte
//                    BigDecimal acc = ((BigDecimal) aValue).divide(BigDecimal.valueOf(1.21));
  //                  System.out.println(acc);
                   
                    BigDecimal auxCIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100)).add(BigDecimal.ONE);
                    aux.setPrecio(  (((BigDecimal) aValue).divide(auxCIva, RoundingMode.CEILING)) );
                    //Calcula totales
                    aux.setIva(((BigDecimal) aValue).subtract(aux.getPrecio()));
                    aux.setTotal(((BigDecimal) aValue));
                    break;
                case 2:
                    //Cambia el % iva
                    aux.setPorcIva((Float) aValue);
                    //Calcula totales
                    auxIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100));
                    aux.setIva(aux.getPrecio().multiply(auxIva));
                    aux.setTotal(aux.getPrecio().add(aux.getIva()));
                    break;
                case 3:
                    //Cambia la cuenta iva
                    aux.setCuentaIva((Account) aValue);
                    break;
                case 4:
                    aux.setIva((BigDecimal) aValue);
                    break;
                case 5:
                    //Total, no incluye iva
                    aux.setTotal((BigDecimal) aValue);
                    break;
            }
        }

        TableModelEvent evento = new TableModelEvent(this, rowIndex - 1, rowIndex, columnIndex);
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     */
    public void calcularTotales() {
        int count = 0;
        for (ModeloFilaNeto modeloFilaNeto : tabla) {
            BigDecimal value = modeloFilaNeto.getPrecio();
            setValueAt(value, count, 1);
        }
    }

    /**
     *
     */
    public void nuevaFila() {
        // A�ade la persona al modelo 
        ModeloFilaNeto auxModeloFila = new ModeloFilaNeto();
        auxModeloFila.setPorcIva(modeloFilaDefecto.getPorcIva());
        auxModeloFila.setCuentaIva(modeloFilaDefecto.getCuentaIva());
        tabla.add(auxModeloFila);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param mfn
     */
    public void nuevaFila(ModeloFilaNeto mfn) {
        tabla.add(mfn);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param fila
     */
    public void borraFila(int fila) {
        // Se borra la fila 
        tabla.remove(fila);
        // Y se avisa a los suscriptores, creando un TableModelEvent... 
        TableModelEvent evento = new TableModelEvent(this, fila, fila,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        // ... y pas�ndoselo a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param nroFila
     * @return
     */
    public ModeloFilaNeto getFila(int nroFila) {
        if (nroFila < getRowCount()) {
            return tabla.get(nroFila);
        } else {
            return null;
        }
    }

    public void addTableModelListener(TableModelListener l) {
        suscriptores.add(l);
    }

    public void removeTableModelListener(TableModelListener l) {
        suscriptores.remove(l);
    }
}
