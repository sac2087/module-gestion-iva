/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import java.math.BigDecimal;
import java.util.LinkedList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import model.Account;
import model.GrainType;

/**
 *
 * @author GDF
 */
public class ModeloTablaNetoConStock implements TableModel {

    private LinkedList<ModeloFilaNetoConStock> tabla;
    private LinkedList suscriptores = new LinkedList();
    //Datos
    ModeloFilaNetoConStock modeloFilaDefecto = new ModeloFilaNetoConStock();

    /**
     *
     * @param porcIvaPorDefecto
     * @param cuentaIVA
     */
    public ModeloTablaNetoConStock(float porcIvaPorDefecto, Account cuentaIVA) {
        tabla = new LinkedList<ModeloFilaNetoConStock>();
        modeloFilaDefecto.setPorcIva(porcIvaPorDefecto);
        modeloFilaDefecto.setCuentaIva(cuentaIVA);
    }

    public int getRowCount() {
        return tabla.size();
    }

    public int getColumnCount() {
        return 9;
    }

    /**
     *
     * @param nroFila
     * @return
     */
    public ModeloFilaNetoConStock getFila(int nroFila) {
        if (nroFila < getRowCount()) {
            return tabla.get(nroFila);
        } else {
            return null;
        }
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Cuenta";
            case 1:
                return "Tipo de Grano";
            case 2:
                return "Cantidad";
            case 3:
                return "Precio/Kg";
            case 4:
                return "% IVA";
            case 5:
                return "Cuenta IVA";
            case 6:
                return "SubTotal";
            case 7:
                return "IVA";
            case 8:
                return "Total";
            case 9:
                return "Opciones";
            default:
                return null;
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Account.class;
            case 1:
                return GrainType.class;
            case 2:
                return Integer.class;
            case 3:
                return BigDecimal.class;
            case 4:
                return Float.class;
            case 5:
                return Account.class;
            case 6:
                return BigDecimal.class;
            case 7:
                return BigDecimal.class;
            case 8:
                return BigDecimal.class;
            default:
                return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        ModeloFilaNetoConStock aux = tabla.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return aux.getCuentaOperacion();
            case 1:
                return aux.getTipoGrano();
            case 2:
                return aux.getCantidad();
            case 3:
                return aux.getPrecio();
            case 4:
                return aux.getPorcIva();
            case 5:
                return aux.getCuentaIva();
            case 6:
                return aux.getSubTotal();
            case 7:
                return aux.getIva();
            case 8:
                return aux.getTotal();
            default:
                return null;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ModeloFilaNetoConStock aux = tabla.get(rowIndex);
        BigDecimal auxIva;
        switch (columnIndex) {
            case 0:
                //Cambia cuenta de operacion
                aux.setCuentaOperacion((Account) aValue);
                break;
            case 1:
                //Cambia el tipo de grano
                aux.setTipoGrano((GrainType) aValue);
                break;
            case 2:
                //Cambia la cantidad
                aux.setCantidad((Integer) aValue);
                //Calcula totales
                aux.setSubTotal(aux.precio.multiply(BigDecimal.valueOf((Integer) aValue)));
                auxIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100));
                aux.setIva(aux.getSubTotal().multiply(auxIva));
                aux.setTotal((aux.getSubTotal().multiply(auxIva)).add(aux.getSubTotal()));
                break;
            case 3:
                //Cambia el precio
                aux.setPrecio((BigDecimal) aValue);
                //Calcula totales
                aux.setSubTotal(((BigDecimal) aValue).multiply(BigDecimal.valueOf(aux.getCantidad())));
                auxIva = new BigDecimal(aux.getPorcIva().toString()).divide(new BigDecimal(100));
                aux.setIva(aux.getSubTotal().multiply(auxIva));
                aux.setTotal((aux.getSubTotal().multiply(auxIva)).add(aux.getSubTotal()));
                break;
            case 4:
                //Cambia el % de iva
                aux.setPorcIva(((Float) aValue).floatValue());
                //Calcula totales
                auxIva = new BigDecimal(((Float) aValue).toString()).divide(new BigDecimal(100));
                aux.setIva(aux.getSubTotal().multiply(auxIva));
                aux.setTotal((aux.getSubTotal().multiply(auxIva)).add(aux.getSubTotal()));
                break;
            case 5:
                //Cuenta de iva
                aux.setCuentaIva((Account) aValue);
                break;
            case 6:
                //SubTotal, no incluye iva
                aux.setSubTotal((BigDecimal) aValue);
                break;
            case 7:
                //Importe IVA
                aux.setIva((BigDecimal) aValue);
                break;
            case 8:
                //Total
                aux.setTotal((BigDecimal) aValue);
                break;
        }
        TableModelEvent evento = new TableModelEvent(this, rowIndex - 1, rowIndex, columnIndex);
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    
    /**
     *
     */
    public void calcularTotales() {
        int count=0;
        for (ModeloFilaNetoConStock modeloFilaNeto : tabla) {
            BigDecimal value = modeloFilaNeto.getPrecio();
            setValueAt(value, count, 3);       
        }
    }  
    
    /**
     *
     */
    public void nuevaFila() {
        // A�ade la persona al modelo 
        ModeloFilaNetoConStock auxModeloFila = new ModeloFilaNetoConStock();
        auxModeloFila.setPorcIva(modeloFilaDefecto.getPorcIva());
        auxModeloFila.setCuentaIva(modeloFilaDefecto.getCuentaIva());
        tabla.add(auxModeloFila);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param mfn
     */
    public void nuevaFila(ModeloFilaNetoConStock mfn) {
        tabla.add(mfn);
        // Avisa a los suscriptores creando un TableModelEvent... 
        TableModelEvent evento;
        evento = new TableModelEvent(
                this, this.getRowCount() - 1, this.getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        // ... y avisando a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    /**
     *
     * @param fila
     */
    public void borraFila(int fila) {
        // Se borra la fila 
        tabla.remove(fila);
        // Y se avisa a los suscriptores, creando un TableModelEvent... 
        TableModelEvent evento = new TableModelEvent(this, fila, fila,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        // ... y pas�ndoselo a los suscriptores
        int i;
        for (i = 0; i < suscriptores.size(); i++) {
            ((TableModelListener) suscriptores.get(i)).tableChanged(evento);
        }
    }

    public void addTableModelListener(TableModelListener l) {
        suscriptores.add(l);
    }

    public void removeTableModelListener(TableModelListener l) {
        suscriptores.remove(l);
    }
}
