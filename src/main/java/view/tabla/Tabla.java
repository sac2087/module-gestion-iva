/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import controller.AccountController;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import model.Account;
import model.GrainTicketAccount;
import model.GrainType;
import model.Session;
import model.TConcepto;
import model.TicketAccount;
import view.BuscarCuenta;

/**
 *
 * @author GDF
 */
public class Tabla extends javax.swing.JDialog {

    ModeloTabla modeloTabla;
    ArrayList<TicketAccount> ticketAccounts;
    ArrayList<GrainTicketAccount> grainTicketAccounts;
    TConcepto concepto;
    boolean isTicketAccount;

    /**
     *
     * @param parent
     * @param modal
     * @param isTicketAccount
     * @param concepto
     */
    public Tabla(java.awt.Dialog parent, boolean modal, boolean isTicketAccount, TConcepto concepto) {
        super(parent, modal);
        modeloTabla = new ModeloTabla();
        this.concepto = concepto;
        initComponents();
        addEscapeKey();
        this.isTicketAccount = isTicketAccount;
        setLocationRelativeTo(null);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                close();
            }
        });
    }

    private void addEscapeKey() {
        // Handle escape key to close the dialog
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        Action escapeAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                close();
            }
        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", escapeAction);
    }

    private void close() {
        if (isTicketAccount) {
            TableToTicketAccount();
        } else {
            TableToGrainTicketAccount();
        }
        dispose();
    }

    private void TableToTicketAccount() {
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            ModeloFila auxFila = modeloTabla.getFila(i);
            if (auxFila.getCuentaOperacion() != null) {
                TicketAccount auxTA = new TicketAccount(auxFila.getCuentaOperacion(), null, auxFila.getPrecio(), 0f, concepto);
                auxTA.setId(auxFila.getId());
                ticketAccounts.add(auxTA);
            }
        }
    }

    private void TableToGrainTicketAccount() {
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            ModeloFila auxFila = modeloTabla.getFila(i);
            if (auxFila.getCuentaOperacion() != null) {
                GrainTicketAccount auxTA = new GrainTicketAccount(auxFila.precio, 0f, 0, null, auxFila.getCuentaOperacion(), null, concepto);

                grainTicketAccounts.add(auxTA);
            }

        }
    }

    /**
     *
     */
    public void buscarCuenta() {
        BuscarCuenta ventanaBuscar = new BuscarCuenta(this, true);
        Account selected;
        try {
            selected = Session.accountSelected;
            modeloTabla.setValueAt(selected, jTable1.getSelectedRow(), jTable1.getSelectedColumn());
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista, null);
        return resultado;
    }

    /**
     * Se asigna el ticket correspondiente dependiendo para que comprobante se
     * lo utilize.
     *
     * @param ticketAccounts
     * @param grainTicketAccounts
     */
    public void asignarTicket(ArrayList<TicketAccount> ticketAccounts, ArrayList<GrainTicketAccount> grainTicketAccounts) {
        this.grainTicketAccounts = grainTicketAccounts;
        this.ticketAccounts = ticketAccounts;
        if (isTicketAccount) {
            if (this.ticketAccounts.size() > 0) {
                TicketAccountToTable();
            } else {
                modeloTabla.nuevaFila();
            }
        } else {
            if (this.grainTicketAccounts.size() > 0) {
                GrainTicketAccountToTable();
            } else {
                modeloTabla.nuevaFila();
            }
        }
        jTable1.setRowSelectionInterval(0, 0);
    }

    /**
     *
     */
    public void TicketAccountToTable() {
        for (int i = 0; i < ticketAccounts.size(); i++) {
            TicketAccount ticketAccountOperacion = ticketAccounts.get(i);
            ModeloFila auxFila = new ModeloFila();
            auxFila.setCuentaOperacion(ticketAccountOperacion.getAccount());
            auxFila.setPrecio(ticketAccountOperacion.getPrice());
            auxFila.setId(ticketAccountOperacion.getId());
            modeloTabla.nuevaFila(auxFila);
        }
        ticketAccounts.clear();
    }

    /**
     *
     */
    public void GrainTicketAccountToTable() {
        for (int i = 0; i < grainTicketAccounts.size(); i++) {
            GrainTicketAccount grainTicketAccountO = grainTicketAccounts.get(i);
            ModeloFila auxFila = new ModeloFila();
            auxFila.setCuentaOperacion(grainTicketAccountO.getAccount());
            auxFila.setPrecio(grainTicketAccountO.getPrice());
            modeloTabla.nuevaFila(auxFila);
        }
        grainTicketAccounts.clear();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(modeloTabla);
        /*Cambia foco de manera horizontal*/
        jTable1.getInputMap(jTable1.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "selectNextColumnCell");

        /*Comportamiento F2*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "ProjSave");
        jTable1.getActionMap().put("ProjSave", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int columna = jTable1.getSelectedColumn();
                if(columna==0){
                    buscarCuenta();
                }
                if(columna==3){
                    buscarCuenta();
                }
            }
        });
        /*Comportamiento +*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        /*Comportamiento -*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("F2 (Buscar)          + (Agregar Fila)          -  (Borrar Fila)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(172, 172, 172))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(5, 5, 5))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
