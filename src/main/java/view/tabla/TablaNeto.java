/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import controller.AccountController;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import model.Account;
import model.Session;
import model.TConcepto;
import model.TOperacion;
import model.TTicket;
import model.TicketAccount;
import view.BuscarCuenta;
import view.CargaComprobante;

/**
 *
 * @author GDF
 */
public class TablaNeto extends javax.swing.JDialog {

    ModeloTablaNeto modeloTabla;
    ArrayList<TicketAccount> ticketAccounts;
    TOperacion operacion;
    TTicket tcomprobante;

    /**
     * Creates new form TablaNeto
     * @param parent
     * @param modal
     * @param ticketAccounts
     * @param operacion
     * @param tcomprobante  
     */
    public TablaNeto(java.awt.Dialog parent, boolean modal, ArrayList<TicketAccount> ticketAccounts, TOperacion operacion, TTicket tcomprobante) {
        super(parent, modal);
        this.ticketAccounts = ticketAccounts;
        addEscapeKey();
        Account accountIva = null;
        if (operacion==TOperacion.VENTA){
            accountIva = AccountController.findForName(Session.client.getAccountPlan().getId(), "I.V.A DEBITO FISCAL");
        }else if (operacion==TOperacion.COMPRA){
            accountIva = AccountController.findForName(Session.client.getAccountPlan().getId(), "IVA CREDITO FISCAL");
        }
        modeloTabla = new ModeloTablaNeto(CargaComprobante.Empresa.getIva(),accountIva, ((tcomprobante.getId()==6)&&(operacion==TOperacion.VENTA)));
        if (this.ticketAccounts.size() > 0) {
            TicketAccountToTable();
        } else {
            modeloTabla.nuevaFila();
        }
        initComponents();
        setLocationRelativeTo(null);
        jTable1.setRowSelectionInterval(0, 0);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                close();
            }
        });
    }

    /**
     *
     */
    public void close() {
        TableToTicketAccount();
        dispose();
    }

    private void addEscapeKey() {
        // Handle escape key to close the dialog
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        Action escapeAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                close();
            }
        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", escapeAction);
    }

    /**
     *
     */
    public void TicketAccountToTable() {
        for (int i = 0; i < ticketAccounts.size(); i++) {
            TicketAccount ticketAccountOperacion = ticketAccounts.get(i);
            TicketAccount ticketAccountIVA = ticketAccounts.get(i + 1);
            ModeloFilaNeto auxFila = new ModeloFilaNeto();
            auxFila.setId(ticketAccountOperacion.getId());
            auxFila.setIdIva(ticketAccountIVA.getId());
            auxFila.setCuentaOperacion(ticketAccountOperacion.getAccount());
            auxFila.setCuentaIva(ticketAccountIVA.getAccount());
            auxFila.setPrecio(ticketAccountOperacion.getPrice());
            auxFila.setIva(ticketAccountIVA.getPrice());
            auxFila.setPorcIva(ticketAccountIVA.getIvaPercent());
            System.out.println("CUENTA IVA> " + auxFila.getCuentaIva() + "\n CUENTA OPERACION>" + auxFila.cuentaOperacion + "\n PRECIO" + auxFila.getPrecio() + "\n IMPORTE IVA " + auxFila.getIva() + "\n %IVA " + auxFila.getPorcIva() + "\n TOTAL " + auxFila.getTotal());
            modeloTabla.nuevaFila(auxFila);
            i++;
        }
        modeloTabla.calcularTotales();
        ticketAccounts.clear();
    }

    /**
     *
     */
    public void TableToTicketAccount() {
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            ModeloFilaNeto auxFila = modeloTabla.getFila(i);
            System.out.println(auxFila.cuentaOperacion);
            TicketAccount auxTA = new TicketAccount(auxFila.getCuentaOperacion(), null, auxFila.getPrecio(), 0f, TConcepto.NETO);
            if (auxFila.getId() != 0) {
                auxTA.setId(auxFila.getId());
            }
            ticketAccounts.add(auxTA);
            TicketAccount auxTAIVA = new TicketAccount(auxFila.getCuentaIva(), null, auxFila.getIva(), auxFila.getPorcIva(), TConcepto.IVA);
            if (auxFila.getIdIva() != 0) {
                auxTAIVA.setId(auxFila.getIdIva());
            }
            ticketAccounts.add(auxTAIVA);

        }
    }

    /**
     *
     */
    public void buscarCuenta() {
        BuscarCuenta ventanaBuscar = new BuscarCuenta(this, true);
        Account selected;
        try {
            selected = Session.accountSelected;
            modeloTabla.setValueAt(selected, jTable1.getSelectedRow(), jTable1.getSelectedColumn());
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista, null);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(modeloTabla);
        /*Cambia foco de manera horizontal*/
        jTable1.getInputMap(jTable1.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "selectNextColumnCell");

        /*Comportamiento F2*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "ProjSave");
        jTable1.getActionMap().put("ProjSave", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int columna = jTable1.getSelectedColumn();
                if(columna==0){
                    buscarCuenta();
                }
                if(columna==3){
                    buscarCuenta();
                }
            }
        });
        /*Comportamiento +*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        /*Comportamiento -*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("F2 (Buscar)          + (Agregar Fila)          -  (Borrar Fila)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(197, 197, 197)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
