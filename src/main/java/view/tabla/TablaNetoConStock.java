/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.tabla;

import controller.AccountController;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import model.Account;
import model.GrainTicketAccount;
import model.GrainType;
import model.Session;
import model.TConcepto;
import model.TOperacion;
import view.BuscarCuenta;

/**
 *
 * @author GDF
 */
public class TablaNetoConStock extends javax.swing.JDialog {

    ModeloTablaNetoConStock modeloTabla;
    ArrayList<GrainTicketAccount> grainTicketAccount;
    TConcepto concepto;
    TOperacion operacion;

    /**
     *
     * @param parent
     * @param modal
     * @param grainTicketAccount
     * @param iva
     * @param concepto
     * @param operacion  
     */
    public TablaNetoConStock(java.awt.Dialog parent, boolean modal, ArrayList<GrainTicketAccount> grainTicketAccount, float iva, TConcepto concepto, TOperacion operacion) {
        super(parent, modal);
        this.grainTicketAccount = grainTicketAccount;
        this.operacion = operacion;
        this.concepto = concepto;
                Account accountIva = null;
        if (operacion==TOperacion.VENTA){
            accountIva = AccountController.findForName(Session.client.getAccountPlan().getId(), "I.V.A DEBITO FISCAL");
        }else if (operacion==TOperacion.COMPRA){
            accountIva = AccountController.findForName(Session.client.getAccountPlan().getId(), "IVA CREDITO FISCAL");
        }
        modeloTabla = new ModeloTablaNetoConStock(iva, accountIva);
        if (this.grainTicketAccount.size() > 0) {
            GrainTicketAccountToTable();
        } else {
            modeloTabla.nuevaFila();
        }
        addEscapeKey();
        initComponents();
        setLocationRelativeTo(null);
        jTable1.setRowSelectionInterval(0, 0);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                close();
            }
        });
    }

    private void addEscapeKey() {
        // Handle escape key to close the dialog
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        Action escapeAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                close();
            }
        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", escapeAction);
    }

    private void CargarDatosEnTabla(ArrayList<GrainTicketAccount> grainTicketAccount) {
        this.grainTicketAccount = grainTicketAccount;
    }

    /**
     *
     */
    public void close() {
        TableToGrainTicketAccount();
        dispose();
    }

    /**
     *
     */
    public void GrainTicketAccountToTable() {
        for (int i = 0; i < grainTicketAccount.size(); i++) {
            GrainTicketAccount grainTicketAccountOp = grainTicketAccount.get(i);
            GrainTicketAccount grainTicketAccountIVA = grainTicketAccount.get(i + 1);
            ModeloFilaNetoConStock auxFila = new ModeloFilaNetoConStock();
            auxFila.setCuentaOperacion(grainTicketAccountOp.getAccount());
            auxFila.setIdOp(grainTicketAccountOp.getId());
            auxFila.setIdIva(grainTicketAccountIVA.getId());
            auxFila.setTipoGrano(grainTicketAccountOp.getGrainType());
            auxFila.setCantidad(grainTicketAccountOp.getKg());
            auxFila.setCuentaIva(grainTicketAccountIVA.getAccount());
            auxFila.setPrecio(grainTicketAccountOp.getPrice());
            auxFila.setIva(grainTicketAccountIVA.getPrice());
            auxFila.setPorcIva(grainTicketAccountIVA.getIvaPervent());
            modeloTabla.nuevaFila(auxFila);
            i++;
        }
        modeloTabla.calcularTotales();
        grainTicketAccount.clear();
    }

    /**
     *
     */
    public void TableToGrainTicketAccount() {
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            ModeloFilaNetoConStock auxFila = modeloTabla.getFila(i);
            GrainTicketAccount auxGTA = new GrainTicketAccount(auxFila.getPrecio(), 0f, auxFila.getCantidad(), auxFila.getTipoGrano(), auxFila.getCuentaOperacion(), null, concepto);
            auxGTA.setId(auxFila.getIdOp());
            grainTicketAccount.add(auxGTA);
            GrainTicketAccount auxGTAIVA = null;
            if (operacion==TOperacion.COMPRA){
               auxGTAIVA = new GrainTicketAccount(auxFila.getIva(), auxFila.getPorcIva(), 0, null, auxFila.getCuentaIva(), null, TConcepto.IVAC); 
            }else if(operacion==TOperacion.VENTA){
                auxGTAIVA = new GrainTicketAccount(auxFila.getIva(), auxFila.getPorcIva(), 0, null, auxFila.getCuentaIva(), null, TConcepto.IVAV);
            }
            
            auxGTAIVA.setId(auxFila.getIdIva());
            grainTicketAccount.add(auxGTAIVA);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        jPopupMenu1.setToolTipText("opciones");
        jPopupMenu1.setComponentPopupMenu(jPopupMenu1);

        jMenuItem1.setText("Agregar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setText("Quitar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(modeloTabla);
        /*Cambia foco de manera horizontal*/
        jTable1.getInputMap(jTable1.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "selectNextColumnCell");

        /*Comportamiento F2*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "ProjSave");
        jTable1.getActionMap().put("ProjSave", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int columna = jTable1.getSelectedColumn();
                if(columna==0){
                    buscarCuenta();
                }
                if(columna==1){
                    buscarTipoGrano();
                }
                if(columna==5){
                    buscarCuenta();
                }
            }
        });
        /*Comportamiento +*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "AgregarFila");
        jTable1.getActionMap().put("AgregarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                modeloTabla.nuevaFila();
            }
        });
        /*Comportamiento -*/
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jTable1.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "EliminarFila");
        jTable1.getActionMap().put("EliminarFila2", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int filaSelec= jTable1.getSelectedRow();
                modeloTabla.borraFila(filaSelec);
            }
        });
        jTable1.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("F2 (Buscar)          + (Agregar Fila)          -  (Borrar Fila)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 789, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(268, 268, 268))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(12, 12, 12))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        modeloTabla.nuevaFila();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        if (!(jTable1.getSelectedRow() == -1)) {
            modeloTabla.borraFila(jTable1.getSelectedRow());
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    /**
     *
     */
    public void buscarCuenta() {
        BuscarCuenta ventanaBuscar = new BuscarCuenta(this, true);
        Account selected;
        try {
            selected = Session.accountSelected;
            modeloTabla.setValueAt(selected, jTable1.getSelectedRow(), jTable1.getSelectedColumn());
        } catch (Exception e) {
        }
    }

    /**
     *
     */
    public void buscarTipoGrano() {
        GrainType selected;
        try {
            selected = (GrainType) sugerenciaList("Tipo Grano", GrainType.values());
            modeloTabla.setValueAt(selected, jTable1.getSelectedRow(), jTable1.getSelectedColumn());
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param name
     * @param lista
     * @return
     */
    public Object sugerenciaList(String name, Object[] lista) {
        Object resultado;
        resultado = (Object) JOptionPane.showInputDialog(null, "Seleccione un " + name, name,
                JOptionPane.QUESTION_MESSAGE, null, lista, null);
        return resultado;
    }
    /*
     public static ModeloTablaNetoConStock adaptarDatos(ArrayList<GrainTicketAccount> grainTicketAccounts, float iva){
     ModeloTablaNetoConStock datosAdaptados = new ModeloTablaNetoConStock(iva, AccountController.findForName(Session.client.getAccountPlan().getId(), "IVA CREDITO FISCAL"));
     for (int i = 0; i < grainTicketAccounts.size(); i++) {
     GrainTicketAccount grainTicketAccount = grainTicketAccounts.get(i);
     if(grainTicketAccount.getConcepto().equals("NETO")){
     datosAdaptados.nuevaFila();
     datosAdaptados.setValueAt(grainTicketAccount.getAccount(), 0, 0);
               
     }
            
            
            
     }
     return datosAdaptados;
     }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
