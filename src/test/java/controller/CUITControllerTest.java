/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class CUITControllerTest {

    /**
     * Test of verify method, of class CUITController.
     */
    @Test
    public void testVerifyOk() {
        boolean expResult = true;
        boolean result = CUITController.verify("20330543249");
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testVerifyFail() {
        boolean expResult = false;

        boolean result = false;
        result = result || CUITController.verify("20330543240");
        result = result || CUITController.verify("20330543241");
        result = result || CUITController.verify("20330543242");
        result = result || CUITController.verify("20330543243");
        result = result || CUITController.verify("20330543244");
        result = result || CUITController.verify("20330543245");
        result = result || CUITController.verify("20330543246");
        result = result || CUITController.verify("20330543247");
        result = result || CUITController.verify("20330543248");
        result = result || CUITController.verify("33054324");
        assertEquals(expResult, result);
    }
}