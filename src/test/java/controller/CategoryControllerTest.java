/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Category;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class CategoryControllerTest {

    private final String categoryOk = "JUNIT", categoryFail = "JUNIT2";

    /**
     *
     */
    public CategoryControllerTest() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
        CategoryController.delete(categoryOk);
        CategoryController.delete(categoryFail);
    }

    /**
     * Test of insert method, of class CategoryController.
     */
    @Test
    public void testInsertOk() {
        boolean expResult = true;
        boolean result = CategoryController.insert(categoryOk);
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class CategoryController.
     */
    @Test
    public void testInsertFail() {
        boolean insert = CategoryController.insert(categoryOk);
        assertTrue(insert);
        boolean expResult = false;
        boolean result = CategoryController.insert(categoryOk);
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class CategoryController.
     */
    @Test
    public void testDelete() {
        boolean insert = CategoryController.insert(categoryOk);
        boolean delete = CategoryController.delete(categoryOk);
        assertEquals(insert, delete);
    }

    /**
     * Test of editName method, of class CategoryController.
     */
    @Test
    public void testEditName() {
        boolean expResult = true;
        CategoryController.insert(categoryOk);
        boolean result = CategoryController.editName(categoryOk, categoryFail);
        assertEquals(expResult, result);
    }

    /**
     * Test of findForName method, of class CategoryController.
     */
    @Test
    public void testFindForName() {
        CategoryController.insert(categoryOk);
        Category result = CategoryController.findForName(categoryOk);
        assertEquals(categoryOk, result.getName());
    }
}