/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.FiscCondition;
import model.IdType;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class ClientControllerTest {

    private final String clientOk = "JUNIT";
    private final IdType tipoDoc = null;
    private final String cuit = "";
    private final String zipcode = "";
    private final String address = "";
    private final String phone = "";
    private final float iva = 0.0F;
    private final FiscCondition condFiscal = null;
    private final String email = "";

    /**
     *
     */
    public ClientControllerTest() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
        try {
            ClientController.delete(ClientController.findForName(clientOk));
        } catch (Exception e) {
        }

    }

    /**
     * Test of insert method, of class ClientController.
     */
    @Test
    public void testInsertOk() {
        boolean expResult = true;
        boolean result = ClientController.insert(clientOk, tipoDoc, cuit, zipcode, address, phone, iva, condFiscal, email);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testInsertFail() {
        boolean expResult = false;
        ClientController.insert(clientOk, tipoDoc, cuit, zipcode, address, phone, iva, condFiscal, email);
        boolean result = ClientController.insert(clientOk, tipoDoc, cuit, zipcode, address, phone, iva, condFiscal, email);
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class ClientController.
     */
    @Test
    public void testDelete() {
        ClientController.insert(clientOk, tipoDoc, cuit, zipcode, address, phone, iva, condFiscal, email);
        boolean delete = ClientController.delete(ClientController.findForName(clientOk));
        assertTrue(delete);
    }

    /**
     * Test of getAll method, of class ClientController.
     */
    @Test
    public void testGetAll() {
        List result = ClientController.getAll();
        assert (result.size() >= 0);
    }
}