/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Enterprise;
import model.EnterpriseAccount;
import model.FiscCondition;
import model.IdType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class EnterpriseControllerTest {

    private final String enterpriseOk = "JUNIT";
    private final IdType tipoDoc = null;
    private final String cuit = "";
    private final String address = "";
    private final String phone = "";
    private final float iva = 0.0F;
    private final FiscCondition condFiscal = null;
    private final String email = "";
    private final String zipCode = "";
    private final List<EnterpriseAccount> enterpriseAccount = null;

    /**
     *
     */
    public EnterpriseControllerTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        EnterpriseController.insert(enterpriseOk, cuit, zipCode, address, phone, condFiscal, iva, email, tipoDoc, enterpriseAccount);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        EnterpriseController.delete(EnterpriseController.findForCuit(cuit));
    }

    /**
     * Test of insert method, of class EnterpriseController.
     */
    @Test
    public void testInsert() {
        boolean result = EnterpriseController.insert(enterpriseOk, cuit, zipCode, address, phone, condFiscal, iva, email, tipoDoc, enterpriseAccount);
        assertFalse(result);
    }

    /**
     * Test of delete method, of class EnterpriseController.
     */
    @Test
    public void testDelete() {
        //EnterpriseController.delete(EnterpriseController.findForCuit(cuit).getId());
        assertTrue(true);
    }

    /**
     * Test of editEnterprise method, of class EnterpriseController.
     */
    @Test
    public void testEditEnterprise() {
        boolean expResult = true;
        boolean result = EnterpriseController.editEnterprise(EnterpriseController.findForCuit(cuit).getId(), enterpriseOk, cuit, address, phone, condFiscal, email);
        assertEquals(expResult, result);
    }

    /**
     * Test of editIVA method, of class EnterpriseController.
     */
    @Test
    public void testEditIVA() {
        boolean expResult = true;
        boolean result = EnterpriseController.editIVA(EnterpriseController.findForCuit(cuit).getId(), iva);
        assertEquals(expResult, result);
    }

    /**
     * Test of getLike method, of class EnterpriseController.
     */
    @Test
    public void testGetLike() {
        List result = EnterpriseController.getLike(enterpriseOk.substring(3));
        assertNotNull(result);
    }

    /**
     * Test of getAll method, of class EnterpriseController.
     */
    @Test
    public void testGetAll() {
        List result = EnterpriseController.getAll();
        assertNotNull(result);
    }

    /**
     * Test of findForName method, of class EnterpriseController.
     */
    @Test
    public void testFindForName() {
        Enterprise result = EnterpriseController.findForName(enterpriseOk);
        assertNotNull(result);
    }

    /**
     * Test of findForCuit method, of class EnterpriseController.
     */
    @Test
    public void testFindForCuit() {
        Enterprise result = EnterpriseController.findForCuit(cuit);
        assertNotNull(result);
    }
}