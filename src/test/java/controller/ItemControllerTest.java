/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Item;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class ItemControllerTest {

    private final String subCategory = "JUNITSUBCATEGORY", itemOk = "JUNIT", itemFail = "JUNIT2";

    /**
     *
     */
    public ItemControllerTest() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
        ItemController.delete(itemOk);
        ItemController.delete(itemFail);
    }

    /**
     * Test of insert method, of class ItemController.
     */
    @Test
    public void testInsertOk() {
        boolean expResult = true;
        boolean result = ItemController.insert(itemOk, subCategory);
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class ItemController.
     */
    @Test
    public void testInsertFail() {
        boolean insert = ItemController.insert(itemOk, subCategory);
        assertTrue(insert);
        boolean expResult = false;
        boolean result = ItemController.insert(itemOk, subCategory);
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class ItemController.
     */
    @Test
    public void testDelete() {
        boolean insert = ItemController.insert(itemOk, subCategory);
        boolean delete = ItemController.delete(itemOk);
        assertEquals(insert, delete);
    }

    /**
     * Test of editName method, of class ItemController.
     */
    @Test
    public void testEditName() {
        boolean expResult = true;
        ItemController.insert(itemOk, subCategory);
        boolean result = ItemController.editName(itemOk, itemFail);
        assertEquals(expResult, result);
    }

    /**
     * Test of findForName method, of class ItemController.
     */
    @Test
    public void testFindForName() {
        ItemController.insert(itemOk, subCategory);
        Item result = ItemController.findForName(itemOk);
        assertEquals(itemOk, result.getName());
    }
}