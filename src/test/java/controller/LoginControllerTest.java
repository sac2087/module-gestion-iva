/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author lprone
 */
public class LoginControllerTest {

    private final String userAdmin = "junitAdmin", user = "junitUser", userFail = "junit2";
    private final String pass = "junit";
    private final String roleAdmin = null, roleUser = "user";

    /**
     *
     */
    @Before
    public void insert() {
        UserController.insert(userAdmin, pass, roleAdmin);
        UserController.insert(user, pass, roleUser);
    }

    /**
     *
     */
    @After
    public void deleteTempUser() {
        UserController.delete(userAdmin);
        UserController.delete(user);
    }

    /**
     * Test of validate method, of class LoginController.
     */
    @Test
    public void validateOk() {
        if (UserController.getAllUsers().isEmpty()) {
            assertTrue(true);
        } else {
            boolean expResult = true;
            boolean result = LoginController.validate(userAdmin, pass);
            result = result || LoginController.validate(user, pass);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of validate method, of class LoginController.
     */
    @Test
    public void validateError() {
        boolean expResult = false;
        boolean result = LoginController.validate(userFail, pass);
        assertEquals(expResult, result);
    }
}