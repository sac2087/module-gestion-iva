/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.SubCategory;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class SubCategoryControllerTest {

    private final String category = "JUNITCATEGORY", subCategoryOk = "JUNIT", subCategoryFail = "JUNIT2";

    /**
     *
     */
    public SubCategoryControllerTest() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
        SubCategoryController.delete(subCategoryOk);
        SubCategoryController.delete(subCategoryFail);
    }

    /**
     * Test of insert method, of class SubCategoryController.
     */
    @Test
    public void testInsertOk() {
        boolean expResult = true;
        boolean result = SubCategoryController.insert(subCategoryOk, category);
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class SubCategoryController.
     */
    @Test
    public void testInsertFail() {
        boolean insert = SubCategoryController.insert(subCategoryOk, category);
        boolean expResult = false;
        boolean result = SubCategoryController.insert(subCategoryOk, category);
        assertEquals(expResult, result);
    }

    /**
     * Test of delete method, of class SubCategoryController.
     */
    @Test
    public void testDelete() {
        boolean insert = SubCategoryController.insert(subCategoryOk, category);
        boolean delete = SubCategoryController.delete(subCategoryOk);
        assertEquals(insert, delete);
    }

    /**
     * Test of editName method, of class SubCategoryController.
     */
    @Test
    public void testEditName() {
        boolean expResult = true;
        SubCategoryController.insert(subCategoryOk, category);
        boolean result = SubCategoryController.editName(subCategoryOk, subCategoryFail);
        assertEquals(expResult, result);
    }

    /**
     * Test of findForName method, of class SubCategoryController.
     */
    @Test
    public void testFindForName() {
        SubCategoryController.insert(subCategoryOk, category);
        SubCategory result = SubCategoryController.findForName(subCategoryOk);
        assertEquals(subCategoryOk, result.getName());
    }
}