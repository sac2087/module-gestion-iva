/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lprone
 */
public class cryptTest {

    /**
     *
     */
    public cryptTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class crypt.
     * @throws Exception 
     */
    @Test
    public void testEncrypt() throws Exception {
        String plainText = "root";
        crypt instance = new crypt();
        String expResult = "8K+SAymidwGGNdpa0+f7jg==";
        String result = instance.encrypt(plainText);
        assertEquals(expResult, result);
    }

    /**
     * Test of decrypt method, of class crypt.
     * @throws Exception 
     */
    @Test
    public void testDecrypt() throws Exception {
        String codedText = "8K+SAymidwGGNdpa0+f7jg==";
        crypt instance = new crypt();
        String expResult = "root";
        String result = instance.decrypt(codedText);
        assertEquals(expResult, result);
    }
}